-- ----------------------------
-- Table structure for tb_customer_msg
-- ----------------------------
DROP TABLE IF EXISTS `tb_customer_msg`;
CREATE TABLE `tb_customer_msg`  (
  `id` varchar(32)  NOT NULL COMMENT '会话id',
  `msg_id` varchar(9)  DEFAULT NULL,
  `name` varchar(50)  DEFAULT NULL,
  `city` varchar(50)  DEFAULT NULL COMMENT '城市',
  `openid` varchar(32)  NOT NULL COMMENT '用户id',
  `first_cid` varchar(32)  DEFAULT NULL COMMENT '初次接入客服',
  `current_cid` varchar(32)  DEFAULT NULL COMMENT '当前接入客服',
  `complete_cid` varchar(32) DEFAULT NULL COMMENT '会话完成客服id',
  `appid` varchar(32) DEFAULT NULL COMMENT '公众号id',
  `data` longtext COMMENT '消息记录',
  `tenant_code` varchar(32) NOT NULL DEFAULT '000000',
  `create_org` varchar(32) DEFAULT NULL,
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `create_user` varchar(50) NOT NULL,
  `update_user` varchar(50) NOT NULL,
  `update_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '1、待接入  2、 已接入 3 完成 4 未完成',
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0,
  `score` varchar(30) DEFAULT NULL COMMENT '打分',
  `type` varchar(32) DEFAULT NULL COMMENT '咨询事项 字典',
  `address` varchar(50) DEFAULT NULL COMMENT '地址',
  `is_special` tinyint(1) DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '会话记录表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tb_chat_file
-- ----------------------------
DROP TABLE IF EXISTS `tb_chat_file`;
CREATE TABLE `tb_chat_file` (
  `id` varchar(50) NOT NULL,
  `file_real_name` varchar(255) DEFAULT NULL COMMENT '文件原名称',
  `file_path` varchar(255) DEFAULT NULL COMMENT '文件全路径',
  `create_user` varchar(255) DEFAULT NULL,
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_user` varchar(255) DEFAULT NULL,
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `create_org` varchar(255) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT NULL,
  `tenant_code` varchar(10) NOT NULL DEFAULT '000000',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='文件表';

-- ----------------------------
-- Table structure for tb_chat_matter
-- ----------------------------
DROP TABLE IF EXISTS `tb_chat_matter`;
CREATE TABLE `tb_chat_matter` (
  `id` varchar(32) NOT NULL,
  `name` varchar(50) DEFAULT NULL COMMENT '事项名称',
  `parent_id` varchar(32) DEFAULT NULL,
  `create_user` varchar(50) DEFAULT NULL,
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_user` varchar(50) DEFAULT NULL,
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(1) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT NULL,
  `tenant_code` varchar(20) NOT NULL DEFAULT '000000',
  `create_org` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='咨询事项表';

-- ----------------------------
-- Table structure for tb_company
-- ----------------------------
DROP TABLE IF EXISTS `tb_company`;
CREATE TABLE `tb_company` (
  `id` varchar(32) NOT NULL,
  `area` varchar(100) DEFAULT NULL COMMENT '地区',
  `company` varchar(100) DEFAULT NULL COMMENT '单位',
  `cid` varchar(50) DEFAULT NULL COMMENT '客服号',
  `person` varchar(50) DEFAULT NULL COMMENT '负责人',
  `phone` varchar(15) DEFAULT NULL COMMENT '电话',
  `remark` varchar(50) DEFAULT NULL,
  `create_user` varchar(50) DEFAULT NULL,
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_user` varchar(50) DEFAULT NULL,
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `create_org` varchar(32) DEFAULT NULL,
  `tenant_code` varchar(10) NOT NULL DEFAULT '000000',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='局办';

-- ----------------------------
-- Table structure for tb_customer_settings
-- ----------------------------
DROP TABLE IF EXISTS `tb_customer_settings`;
CREATE TABLE `tb_customer_settings` (
  `id` varchar(32) NOT NULL,
  `cid` varchar(50) NOT NULL COMMENT '客服账号',
  `auto_access` tinyint(1) NOT NULL DEFAULT '0' COMMENT '自动接入 0 关闭 1开启',
  `auto_reply` tinyint(1) NOT NULL DEFAULT '0' COMMENT '接入自动回复 0 关闭 1开启',
  `reply_content` varchar(2500) NOT NULL DEFAULT '您好！有什么可以帮您？' COMMENT '自动回复内容',
  `allow_transfer` tinyint(1) NOT NULL DEFAULT '0' COMMENT '限制转接功能 0 关闭 1开启',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `create_user` varchar(50) NOT NULL DEFAULT 'root',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_user` varchar(50) NOT NULL DEFAULT 'root',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `create_org` varchar(32) DEFAULT NULL,
  `auto_access_reply` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='客服设置表';

-- ----------------------------
-- Table structure for tb_demo_record
-- ----------------------------
DROP TABLE IF EXISTS `tb_demo_record`;
CREATE TABLE `tb_demo_record` (
  `id` varchar(32) NOT NULL,
  `group_id` varchar(32) DEFAULT NULL,
  `user_id` varchar(32) DEFAULT NULL,
  `content` varchar(2500) DEFAULT NULL,
  `create_user` varchar(32) DEFAULT NULL,
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_user` varchar(32) DEFAULT NULL,
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `create_org` varchar(32) DEFAULT NULL,
  `color` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='聊天室';

-- ----------------------------
-- Table structure for tb_fixed_reply
-- ----------------------------
DROP TABLE IF EXISTS `tb_fixed_reply`;
CREATE TABLE `tb_fixed_reply` (
  `id` varchar(32) NOT NULL,
  `appid` varchar(32) DEFAULT NULL COMMENT '公众号id',
  `ask` varchar(500) DEFAULT NULL COMMENT '用户问题',
  `start_time` datetime DEFAULT NULL COMMENT '开始时间',
  `end_time` datetime DEFAULT NULL COMMENT '结束时间',
  `reply_bodys` varchar(1500) DEFAULT NULL COMMENT '回复内容json数组格式',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `create_user` varchar(255) NOT NULL DEFAULT 'root',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_user` varchar(255) NOT NULL DEFAULT 'root',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否启用1是 0否',
  `create_org` varchar(32) DEFAULT NULL,
  `tenant_code` varchar(10) NOT NULL DEFAULT '000000',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='固定回复表';

-- ----------------------------
-- Table structure for tb_special_user
-- ----------------------------
DROP TABLE IF EXISTS `tb_special_user`;
CREATE TABLE `tb_special_user` (
  `id` varchar(32) NOT NULL,
  `name` varchar(50) DEFAULT NULL COMMENT '姓名',
  `phone` varchar(15) DEFAULT NULL COMMENT '手机号',
  `idcard` varchar(22) DEFAULT NULL COMMENT '身份证号',
  `remark` varchar(1000) DEFAULT NULL COMMENT '备注',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `create_user` varchar(32) NOT NULL DEFAULT 'root',
  `update_user` varchar(32) NOT NULL DEFAULT 'root',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `create_org` varchar(32) DEFAULT NULL,
  `company` varchar(255) DEFAULT NULL COMMENT '单位i',
  `openid` varchar(50) DEFAULT NULL COMMENT '微信openid',
  `appid` varchar(50) DEFAULT NULL COMMENT '公众号id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='白名单表';

-- ----------------------------
-- Table structure for tb_standard_reply
-- ----------------------------
DROP TABLE IF EXISTS `tb_standard_reply`;
CREATE TABLE `tb_standard_reply` (
  `id` varchar(32) NOT NULL,
  `cid` varchar(32) DEFAULT NULL COMMENT '客服账号',
  `content` varchar(2000) NOT NULL COMMENT '回复内容',
  `create_user` varchar(50) NOT NULL,
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_user` varchar(50) NOT NULL,
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1 个人快捷回复 2 公共标准回复',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `create_org` varchar(32) DEFAULT NULL,
  `tenant_code` varchar(32) NOT NULL DEFAULT '000000',
  `type` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='客服快捷回复表';

-- ----------------------------
-- Table structure for tb_wx_app
-- ----------------------------
DROP TABLE IF EXISTS `tb_wx_app`;
CREATE TABLE `tb_wx_app` (
  `id` varchar(32) NOT NULL,
  `mp_name` varchar(50) NOT NULL DEFAULT '' COMMENT '平台名称',
  `app_id` varchar(30) NOT NULL DEFAULT '' COMMENT 'appID',
  `app_secret` varchar(100) NOT NULL DEFAULT '' COMMENT 'appSecret',
  `token` varchar(100) DEFAULT NULL COMMENT '盐值',
  `aes_key` varchar(50) DEFAULT '' COMMENT '微信公众号的EncodingAESKey',
  `create_user` varchar(128) NOT NULL DEFAULT '' COMMENT '创建人',
  `update_user` varchar(128) NOT NULL DEFAULT 'admin' COMMENT '更新人',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `create_org` varchar(32) DEFAULT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '0',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `tenant_code` varchar(32) NOT NULL DEFAULT '000000',
  `wx_original_id` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='微信公众号配置表';

-- ----------------------------
-- Table structure for tb_wx_menu_log
-- ----------------------------
DROP TABLE IF EXISTS `tb_wx_menu_log`;
CREATE TABLE `tb_wx_menu_log` (
  `id` varchar(32) NOT NULL,
  `appid` varchar(32) NOT NULL COMMENT '公众号id',
  `menu` longtext NOT NULL COMMENT '菜单字符串',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `create_user` varchar(32) NOT NULL DEFAULT 'root',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_user` varchar(32) NOT NULL DEFAULT 'root',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `create_org` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='菜单修该记录表';

-- ----------------------------
-- Table structure for tb_wx_userinfo
-- ----------------------------
DROP TABLE IF EXISTS `tb_wx_userinfo`;
CREATE TABLE `tb_wx_userinfo` (
  `id` varchar(32) NOT NULL,
  `openid` varchar(50) DEFAULT NULL,
  `subscribe` tinyint(1) DEFAULT NULL COMMENT '用户是否订阅该公众号标识，值为0时，代表此用户没有关注该公众号，拉取不到其余信息。',
  `nickname` varchar(100) DEFAULT NULL COMMENT '用户的昵称',
  `sex` tinyint(1) DEFAULT NULL COMMENT '用户的性别，值为1时是男性，值为2时是女性，值为0时是未知',
  `city` varchar(50) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `province` varchar(50) DEFAULT NULL,
  `headimgurl` varchar(255) DEFAULT NULL,
  `subscribe_time` int(11) DEFAULT NULL COMMENT '关注时间',
  `unionid` varchar(50) DEFAULT NULL COMMENT 'uid',
  `remark` varchar(500) DEFAULT NULL,
  `groupid` varchar(10) DEFAULT NULL COMMENT '组id',
  `tagid_list` varchar(255) DEFAULT NULL COMMENT '标签',
  `subscribe_scene` varchar(50) DEFAULT NULL,
  `qr_scene` varchar(50) DEFAULT NULL,
  `qr_scene_str` varchar(255) DEFAULT NULL,
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `create_user` varchar(50) NOT NULL DEFAULT 'sys',
  `create_org` varchar(50) DEFAULT NULL,
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_user` varchar(50) NOT NULL DEFAULT 'sys',
  `tenant_code` varchar(10) NOT NULL DEFAULT '000000',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `app_id` varchar(30) DEFAULT NULL,
  `location` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `openidAndAppid` (`openid`,`app_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='公众号用户信息表';
