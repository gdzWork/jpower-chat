<img src="docs/image/logo.png" width="50%" syt height="50%" />

# JPower-Chat：最全的公众号客服管理平台

[![Gitter](https://img.shields.io/badge/Release-V2.1.4-green.svg)](https://gitee.com/gdzWork/JPower)   [![License](https://img.shields.io/badge/Author-mr.g-orange.svg)](https://gitee.com/gdzWork/JPower)
[![Gitter](https://img.shields.io/badge/Copyright%20-@Jpower-%23ff3f59.svg)](https://gitee.com/gdzWork/JPower) [![Gitter](https://img.shields.io/badge/Copyright-JPower--Chat-brightgreen)](https://gitee.com/gdzWork/jpower-chat)


### QQ群 : <img src="docs/image/qrcode.jpg" width="15%" syt height="5%" /> 
 > tip: 加群后凭Star截图可免费获取代码生成器一份
-------

#### 重要说明：此项目依赖于 [JPower](https://gitee.com/gdzWork/JPower) ，切记需要先编译 [JPower](https://gitee.com/gdzWork/JPower) 再编译此项目......

## JPower 简介
`JPower`是由一款政府商业项目升级优化而来。

`JPower` 基于`SpringCloud(2020.0.4)`  + `SpringBoot(2.5.8)` 的微服务快速开发平台.
具备网关统一鉴权、Xss防跨站攻击、分布式事务等多个模块，支持多业务系统并行开发，
支持多服务并行开发，可以作为后端服务的开发脚手架。代码简洁，注释齐全，架构清晰，非常适合学习和作为基础框架使用。
`JPower` 的目标蓝图是能够打造一款集成各种比较好用的工具于一体的开发框架，例如可在页面配置各种报表，集成echarts可实现快速生成页面，各种场景下的数据传输等等各类工具。
目前只是开发了基础的架构，后续会逐渐开发各种各样的工具到框架中。

核心技术采用Spring Cloud Alibaba、SpringBoot、Mybatis、Seata、Sentinel、SkyWalking等主要框架和中间件。
希望能努力打造一套集 `基础框架` —>`分布式微服务架构` —>  `工具集成` —> `系统监测` 的解决方案。`本项目旨在实现基础能力，不涉及具体业务。`

采用JWT做Token认证，可拓展集成Redis等细颗粒度控制方案。

注册中心、配置中心选型Nacos，为工程瘦身的同时加强各模块之间的联动。

集成Sentinel从流量控制、熔断降级等多个维度保护服务的稳定性。

基于MybatisPlus-generator实现了代码生成器。

实现微信公众号管理

## 分支介绍
1. master 分支为最新的稳定版本，每次提交都会升级一个版本号
2. dev 分支为作者的开发分支，作者开发的最新代码会实时提交上来，喜欢尝鲜的可以切换为dev。 但可能有报错、漏提等，对项目不是很熟的朋友千万别尝试。
3. tag 每个固定的版本都会打一个tag方便后续切换任意版本，版本号跟随JPower。

## 技术文档
* [JPower-Chat 配置说明](https://www.kancloud.cn/guodingzhi/jpower/2669786)

## 业务功能介绍：
1. 公众号管理：可管理公众平台下所有公众号
2. 微信客服聊天：支持多个坐席同时接入客服

## 项目亮点功能介绍:
1. **微信公众号管理：**

    全面的公众号管理，可在线设置公众号菜单、标签等等，一切操作都在页面完成，即时生效。
1. **微信公众号客服聊天：**

    提供单独得聊天服务，可即时回复公众号内容，实现即时聊天，支持多个坐席同时在线。
1. **支持时间段固定回复：**

    支持数据库配置固定时间段回复，并且支持配置文件开关配置。
1. **支持时间段固定回复：**

    支持数据库配置固定时间段回复内容，并且支持配置文件开关配置。
1. **指定接入百度图片识别：**

    支持接入百度图片识别，通过配置文件配置百度参数，可实时识别图片内容并返回。
1. **支持接入百度Unit：**

    支持接入百度Unit机器人问答，设置好Unit后可自动通过机器人回答。
1. **支持百度搜索回复：**

    可通过配置文件打开百度搜索回答，公众号可自动回复百度搜索结果。
1. **支持自定义第三方回复：**

    可通过配置文件配置自定义三方地址，自动把地址返回内容返回到公众号。
1. **支持钉钉报警：**

    支持微信公众号模板发送失败钉钉报警。
## 工程结构：
~~~
JPower
├── chat-api -- feign接口和实体类
├── chat-biz -- 业务层
├── chat-common -- 公共模块层
├── chat-reference -- 外部api封装层
├── chat-server -- Netty服务层以及Netty启动消息分发
├── chat-web -- 接口层
└── jpower-wx-mp -- 公众号消息路由层 
~~~
    
## 技术栈：
* 所涉及的相关的技术有
    *  通讯：netty
    *  缓存：Redis
    *  数据库： MySQL 5.7
    *  持久层框架： Mybatis-plus
    *  API网关：Gateway
    *  服务注册与发现: Nacos
    *  服务消费：OpenFeign
    *  负载均衡：spring load balancing
    *  配置中心：Nacos
    *  服务熔断：Sentinel
    *  项目构建：Maven 3.3
    *  分布式事务：seata
    *  分布式系统的流量防卫兵：Sentinel
    *  监控：spring-boot-admin
    *  链路调用跟踪&APM监控：skywalking
    *  Nginx
* 部署方面
    *  服务器：CentOS
    *  Docker 18
    *  Nginx

# 如果觉得本项目对您有任何一点帮助，请点右上角 "Star" 支持一下， 并向您的基友、同事们宣传一下吧，谢谢！

# 发现bug请提交 [issues](https://gitee.com/gdzWork/jpower-chat/issues)

# 参与贡献
1. Fork 本仓库
2. 新建 feat_xxx 分支
3. 提交代码
4. 新建 Pull Request

# 请作者喝杯咖啡把

[![支付](docs/image/erweima.jpg)]()

# 感谢 JetBrains 提供的免费开源 License：

[![JetBrains](docs/image/jetbrains.png)](https://www.jetbrains.com/?from=lamp-cloud)

# 友情链接 & 特别鸣谢
* 微服务快速开发平台：[https://gitee.com/gdzWork/JPower](https://gitee.com/gdzWork/JPower)
* jpower-ui：[https://gitee.com/gdzWork/jpower-ui](https://gitee.com/gdzWork/jpower-ui)
* JWchat：[https://gitee.com/CodeGI/chat](https://gitee.com/CodeGI/chat)
