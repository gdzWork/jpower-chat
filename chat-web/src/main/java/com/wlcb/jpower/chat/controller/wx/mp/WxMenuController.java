package com.wlcb.jpower.chat.controller.wx.mp;

import com.wlcb.jpower.chat.service.wx.mp.WxMenuService;
import com.wlcb.jpower.module.base.enums.JpowerError;
import com.wlcb.jpower.module.base.exception.JpowerAssert;
import com.wlcb.jpower.module.base.vo.ResponseData;
import com.wlcb.jpower.module.common.controller.BaseController;
import com.wlcb.jpower.module.common.utils.ReturnJsonUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import me.chanjar.weixin.common.bean.menu.WxMenu;
import me.chanjar.weixin.mp.bean.menu.WxMpMenu;
import org.springframework.web.bind.annotation.*;

/**
 * @author: liwenfei
 * @version: v1.0
 * @time: 2021/3/5 9:56
 * @desc: 微信菜单
 */
@Api(tags = "公众号菜单")
@RestController
@RequestMapping("/menu")
@RequiredArgsConstructor
public class WxMenuController extends BaseController {

    private final WxMenuService wxMpMenuService;

    @ApiOperation("创建公众号菜单")
    @PostMapping("create")
    public ResponseData create(@RequestBody WxMenu wxMenu,
                               @ApiParam(value = "公众号ID",required = true) @RequestParam(required = false) String appId,
                               @ApiParam(value = "创建失败是否发送钉钉通知",defaultValue = "false") @RequestParam(defaultValue = "false") Boolean exSend) {
        JpowerAssert.notEmpty(appId, JpowerError.Arg,"公众号ID不可为空");
        return ReturnJsonUtil.status(wxMpMenuService.createMenu(wxMenu, appId, exSend));
    }

    @ApiOperation("查询公众号菜单")
    @GetMapping("get")
    public ResponseData<WxMpMenu> get(@ApiParam(value = "公众号ID",required = true) @RequestParam(required = false) String appId) {
        JpowerAssert.notEmpty(appId, JpowerError.Arg,"公众号ID不可为空");
        WxMpMenu menu = wxMpMenuService.findMenu(appId);
        return ReturnJsonUtil.ok("查询成功", menu);
    }

    @ApiOperation("删除公众号菜单")
    @DeleteMapping("delete")
    public ResponseData delete(@ApiParam(value = "公众号ID",required = true) @RequestParam(required = false) String appId) {
        JpowerAssert.notEmpty(appId, JpowerError.Arg,"公众号ID不可为空");
        return ReturnJsonUtil.status(wxMpMenuService.deleteMenu(appId));
    }
}
