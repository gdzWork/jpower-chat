package com.wlcb.jpower.chat.controller.wx.mp;

import com.wlcb.jpower.constant.RedisEunm;
import com.wlcb.jpower.module.base.enums.JpowerError;
import com.wlcb.jpower.module.base.exception.JpowerAssert;
import com.wlcb.jpower.module.base.vo.ResponseData;
import com.wlcb.jpower.module.common.controller.BaseController;
import com.wlcb.jpower.module.common.utils.Fc;
import com.wlcb.jpower.module.common.utils.ReturnJsonUtil;
import com.wlcb.jpower.utils.RedisHelpUtils;
import com.wlcb.jpower.utils.WxMpInstance;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.SneakyThrows;
import me.chanjar.weixin.common.bean.subscribemsg.CategoryData;
import me.chanjar.weixin.common.bean.subscribemsg.PubTemplateKeyword;
import me.chanjar.weixin.common.bean.subscribemsg.PubTemplateTitleListResult;
import me.chanjar.weixin.common.bean.subscribemsg.TemplateInfo;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.subscribe.WxMpSubscribeMessage;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author: liwenfei
 * @version: v1.0
 * @time: 2021/4/6 14:27
 * @desc: 微信订阅通知相关接口实现
 */
@Api(tags = "微信订阅通知")
@RestController
@RequestMapping("/submsg")
public class SubscribeMsgController extends BaseController {
    /**
     * 获取公众号所属类目，可用于查询类目下的公共模板
     * @return
     */
    @ApiOperation(value = "获取公众号所属类目",notes = "可用于查询类目下的公共模板")
    @GetMapping("getCategory")
    @SneakyThrows(WxErrorException.class)
    public ResponseData<List<CategoryData>> getCategory(@ApiParam("公众号ID") String appId) {
        JpowerAssert.notEmpty(appId, JpowerError.Arg,"公众号ID不可为空");
        WxMpService wxMp = WxMpInstance.getWxMp().switchoverTo(appId);
        List<CategoryData> category = wxMp.getSubscribeMsgService().getCategory();
        return ReturnJsonUtil.ok("操作成功", category);
    }

    /**
     * 获取类目下的公共模板，可从中选用模板使用
     *
     * @param appId
     * @param ids   类目 id，多个用逗号隔开
     * @param start 用于分页，表示从 start 开始，从 0 开始计数
     * @param limit 用于分页，表示拉取 limit 条记录，最大为 30
     * @return
     */
    @ApiOperation(value = "获取类目下的公共模板",notes = "可从中选用模板使用")
    @GetMapping("getPubTemplateTitleList")
    @SneakyThrows(WxErrorException.class)
    public ResponseData<PubTemplateTitleListResult> getPubTemplateTitleList(@ApiParam("公众号ID") String appId,@ApiParam("类目id，多个用逗号隔开") String ids,@ApiParam("用于分页，表示从 start 开始，从 0 开始计数") Integer start,@ApiParam("用于分页，表示拉取 limit 条记录，最大为 30") Integer limit) {
        JpowerAssert.notEmpty(appId, JpowerError.Arg,"公众号ID不可为空");
        JpowerAssert.notEmpty(ids, JpowerError.Arg,"类目ID不可为空");
        JpowerAssert.notNull(start, JpowerError.Arg,"start不可为空");
        JpowerAssert.notNull(limit, JpowerError.Arg,"limit不可为空");
        WxMpService wxMp = WxMpInstance.getWxMp().switchoverTo(appId);
        PubTemplateTitleListResult list = wxMp.getSubscribeMsgService().getPubTemplateTitleList(Fc.toStrArray(",", ids), start, limit);
        return ReturnJsonUtil.ok("操作成功", list);
    }

    /**
     * 从公共模板库中选用模板，到私有模板库中
     *
     * @param appId
     * @param tid       模板标题 id，可通过getPubTemplateTitleList接口获取，也可登录公众号后台查看获取
     * @param sort      开发者自行组合好的模板关键词列表，关键词顺序可以自由搭配（例如 [3,5,4] 或 [4,5,3]），最多支持5个，最少2个关键词组合
     * @param sceneDesc 服务场景描述，15个字以内
     * @return
     */
    @ApiOperation(value = "从公共模板库中选用模板,到私有模板库中")
    @PostMapping("addTemplate")
    @SneakyThrows(WxErrorException.class)
    public ResponseData<String> addTemplate(@ApiParam("公众号ID") String appId,@ApiParam("模板标题id") String tid,@ApiParam("模板关键词列表 如 3,5,4，最多支持5个，最少2个关键词组合") String sort,@ApiParam("服务场景描述") String sceneDesc) {
        JpowerAssert.notEmpty(appId, JpowerError.Arg,"公众号ID不可为空");
        JpowerAssert.notEmpty(tid, JpowerError.Arg,"模板标题id不可为空");
        JpowerAssert.notEmpty(sort, JpowerError.Arg,"模板关键词列表不可为空");
        JpowerAssert.notEmpty(sceneDesc, JpowerError.Arg,"服务场景描述不可为空");
        WxMpService wxMp = WxMpInstance.getWxMp().switchoverTo(appId);
        return ReturnJsonUtil.ok("操作成功", wxMp.getSubscribeMsgService().addTemplate(tid, Fc.toIntList(sort), sceneDesc));
    }

    /**
     * 删除私有模板库中的模板
     *
     * @param appId
     * @param priTmplId 要删除的模板id
     * @return
     */
    @ApiOperation(value = "删除私有模板库中的模板")
    @DeleteMapping("deleteTemplate")
    @SneakyThrows(WxErrorException.class)
    public ResponseData deleteTemplate(@ApiParam("公众号ID") String appId,@ApiParam("要删除的模板id") String priTmplId) {
        JpowerAssert.notEmpty(appId, JpowerError.Arg,"公众号ID不可为空");
        JpowerAssert.notEmpty(priTmplId, JpowerError.Arg,"模板标题id不可为空");
        WxMpService wxMp = WxMpInstance.getWxMp().switchoverTo(appId);
        return ReturnJsonUtil.status(wxMp.getSubscribeMsgService().delTemplate(priTmplId));
    }

    /**
     * 获取公共模板下的关键词列表
     *
     * @param appId
     * @param tid   模板标题 id，可通过接口获取
     * @return
     */
    @ApiOperation(value = "获取公共模板下的关键词列表")
    @GetMapping("getPubTemplateKeyWordsById")
    @SneakyThrows(WxErrorException.class)
    public ResponseData<List<PubTemplateKeyword>> getPubTemplateKeyWordsById(@ApiParam("公众号ID") String appId,@ApiParam("模板标题id") String tid) {
        JpowerAssert.notEmpty(appId, JpowerError.Arg,"公众号ID不可为空");
        JpowerAssert.notEmpty(tid, JpowerError.Arg,"模板标题id不可为空");

        WxMpService wxMp = WxMpInstance.getWxMp().switchoverTo(appId);
        List<PubTemplateKeyword> pubTemplateKeyWordsById = wxMp.getSubscribeMsgService().getPubTemplateKeyWordsById(tid);
        return ReturnJsonUtil.ok("操作成功", pubTemplateKeyWordsById);
    }

    /**
     * 获取私有的模板列表
     *
     * @param appId
     * @return
     */
    @ApiOperation(value = "获取私有的模板列表")
    @GetMapping("getTemplateList")
    @SneakyThrows(WxErrorException.class)
    public ResponseData<List<TemplateInfo>> getTemplateList(@ApiParam("公众号ID") String appId) {
        JpowerAssert.notEmpty(appId, JpowerError.Arg,"公众号ID不可为空");
        WxMpService wxMp = WxMpInstance.getWxMp().switchoverTo(appId);
        List<TemplateInfo> templateList = wxMp.getSubscribeMsgService().getTemplateList();
        return ReturnJsonUtil.ok("操作成功", templateList);
    }

    /**
     * 发送订阅通知
     *
     * @param appId
     * @param message 消息体
     * @return
     */
    @ApiOperation("发送订阅通知(模板消息)")
    @PostMapping("send")
    @SneakyThrows(WxErrorException.class)
    public ResponseData send(@ApiParam("公众号ID") @RequestParam(value = "appId",required = false) String appId, @RequestBody WxMpSubscribeMessage message) {
        JpowerAssert.notEmpty(appId, JpowerError.Arg,"公众号ID不可为空");
        WxMpService wxMp = WxMpInstance.getWxMp().switchoverTo(appId);
        wxMp.getSubscribeMsgService().send(message);
        RedisHelpUtils.getRedisUtil().remove(RedisEunm.SUB.getKeyPrefix() + appId + ":" + message.getTemplateId() + ":" + message.getToUser());
        return ReturnJsonUtil.ok("操作成功");
    }
}
