package com.wlcb.jpower.chat.controller.customer.reply;

import com.github.pagehelper.PageInfo;
import com.wlcb.jpower.chat.entity.reply.TbStandardReply;
import com.wlcb.jpower.chat.service.reply.StandardReplyService;
import com.wlcb.jpower.module.base.enums.JpowerError;
import com.wlcb.jpower.module.base.exception.JpowerAssert;
import com.wlcb.jpower.module.base.vo.ResponseData;
import com.wlcb.jpower.module.common.controller.BaseController;
import com.wlcb.jpower.module.common.page.PaginationContext;
import com.wlcb.jpower.module.common.utils.ReturnJsonUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 坐席用的
 */
@Api(tags = "快捷回复设置")
@RestController
@RequestMapping("reply")
@AllArgsConstructor
public class StandardReplyController extends BaseController {

    private StandardReplyService standardReplyService;

    /**
     * 保存
     *
     * @param reply
     * @return
     */
    @ApiOperation("保存")
    @PostMapping("add")
    public ResponseData add(TbStandardReply reply) {
        JpowerAssert.notEmpty(reply.getContent(), JpowerError.Arg,"回复内容不能为空");
        return ReturnJsonUtil.status(standardReplyService.save(reply));
    }

    /**
     * 删除
     *
     * @param id
     * @return
     */
    @ApiOperation("删除")
    @DeleteMapping("remove")
    public ResponseData delete(@ApiParam("主键") String id) {
        JpowerAssert.notEmpty(id, JpowerError.Arg,"主键不能为空");
        return ReturnJsonUtil.status(standardReplyService.removeById(id));
    }

    /**
     * 修改
     *
     * @param reply
     * @return
     */
    @ApiOperation("修改")
    @PutMapping("update")
    public ResponseData update(TbStandardReply reply) {
        JpowerAssert.notEmpty(reply.getId(), JpowerError.Arg,"主键不能为空");
        JpowerAssert.notEmpty(reply.getContent(), JpowerError.Arg,"回复内容不能为空");
        return ReturnJsonUtil.status(standardReplyService.updateById(reply));
    }

    /**
     * 查询
     *
     * @param status 1，个人标准回复 2，公共标准回复
     * @return
     */
    @ApiOperation("查询")
    @GetMapping("get")
    public ResponseData<PageInfo<TbStandardReply>> get(@ApiParam("1，个人标准回复 2，公共标准回复") Integer status,@ApiParam("客服账号") String cid) {
        PaginationContext.startPage();
        List<TbStandardReply> list = standardReplyService.findList(status, cid);
        return ReturnJsonUtil.ok("操作成功", new PageInfo<>(list));
    }
}
