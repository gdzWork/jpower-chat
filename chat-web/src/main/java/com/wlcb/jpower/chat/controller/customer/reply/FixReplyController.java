package com.wlcb.jpower.chat.controller.customer.reply;

import com.github.pagehelper.PageInfo;
import com.wlcb.jpower.chat.entity.reply.TbDateReply;
import com.wlcb.jpower.chat.service.reply.DateReplyService;
import com.wlcb.jpower.module.base.enums.JpowerError;
import com.wlcb.jpower.module.base.exception.JpowerAssert;
import com.wlcb.jpower.module.base.vo.ResponseData;
import com.wlcb.jpower.module.common.controller.BaseController;
import com.wlcb.jpower.module.common.page.PaginationContext;
import com.wlcb.jpower.module.common.utils.ReturnJsonUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @description: 回复设置
 * @author: liwenfei
 * @time: 2021/2/23 11:36
 */
@Api(tags = "固定回复设置")
@RestController
@RequestMapping("/reply/fix")
@AllArgsConstructor
public class FixReplyController extends BaseController {

    private DateReplyService dateReplyService;

    /**
     * 查询
     * @param reply
     * @return
     */
    @ApiOperation("查询")
    @GetMapping("list")
    public ResponseData<PageInfo<TbDateReply>> list(TbDateReply reply) {
        PaginationContext.startPage();
        List<TbDateReply> pageList = dateReplyService.findPageList(reply);
        return ReturnJsonUtil.ok("操作成功", new PageInfo<>(pageList));
    }

    @ApiOperation("新增")
    @PostMapping("add")
    public ResponseData add(TbDateReply reply) {
        JpowerAssert.notEmpty(reply.getAppid(), JpowerError.Arg,"公众号ID不可为空");
        return ReturnJsonUtil.status(dateReplyService.save(reply));
    }

    @ApiOperation("删除")
    @DeleteMapping("delete")
    public ResponseData delete(@ApiParam("主键") String id) {
        JpowerAssert.notEmpty(id,JpowerError.Arg,"主键不可为空");
        return ReturnJsonUtil.status(dateReplyService.removeById(id));
    }

    @ApiOperation("修改状态")
    @PutMapping("update")
    public ResponseData update(@ApiParam("主键") String id,@ApiParam("状态") Integer status) {
        JpowerAssert.notEmpty(id,JpowerError.Arg,"主键不可为空");
        JpowerAssert.notNull(status,JpowerError.Arg,"状态不可为空");
        TbDateReply reply = new TbDateReply();
        reply.setId(id);
        reply.setStatus(status);
        reply.setIsDeleted(false);
        return ReturnJsonUtil.status(dateReplyService.updateById(reply));
    }
}
