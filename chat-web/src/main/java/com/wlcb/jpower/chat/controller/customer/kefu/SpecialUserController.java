package com.wlcb.jpower.chat.controller.customer.kefu;

import com.github.pagehelper.PageInfo;
import com.wlcb.jpower.chat.entity.customer.TbSpecialUser;
import com.wlcb.jpower.chat.service.customer.SpecialUserService;
import com.wlcb.jpower.module.base.enums.JpowerError;
import com.wlcb.jpower.module.base.exception.JpowerAssert;
import com.wlcb.jpower.module.base.vo.ResponseData;
import com.wlcb.jpower.module.common.controller.BaseController;
import com.wlcb.jpower.module.common.page.PaginationContext;
import com.wlcb.jpower.module.common.utils.ReturnJsonUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author: liwenfei
 * @version: v1.0
 * @time: 2021/4/13 9:42
 * @desc:
 */
@Api(tags = "特殊人员管理")
@RestController
@RequestMapping("/user/special")
@AllArgsConstructor
public class SpecialUserController extends BaseController {

    private SpecialUserService specialUserService;

    @ApiOperation("列表")
    @GetMapping("list")
    public ResponseData<PageInfo<TbSpecialUser>> findList(TbSpecialUser specialUser) {
        PaginationContext.startPage();
        List<TbSpecialUser> list = specialUserService.findList(specialUser);
        return ReturnJsonUtil.ok("操作成功",new PageInfo<>(list));
    }

    @ApiOperation("查找一个")
    @GetMapping("queryOne")
    public ResponseData<TbSpecialUser> queryOne(@ApiParam("姓名") String name,@ApiParam("身份证") String idcard,@ApiParam("电话") String phone) {
        return ReturnJsonUtil.ok("操作成功", specialUserService.match(name, idcard, phone));
    }

    @ApiOperation("新增")
    @PostMapping("add")
    public ResponseData add(TbSpecialUser specialUser) {
        JpowerAssert.notEmpty(specialUser.getName(), JpowerError.Arg,"name不可为空");
        JpowerAssert.notEmpty(specialUser.getCompany(), JpowerError.Arg,"company不可为空");
        return ReturnJsonUtil.status(specialUserService.save(specialUser));
    }

    @ApiOperation("删除")
    @PostMapping("delete")
    public ResponseData delete(@ApiParam("主键") String id) {
        JpowerAssert.notEmpty(id,JpowerError.Arg,"主键不可为空");
        return ReturnJsonUtil.status(specialUserService.removeById(id));
    }

    @ApiOperation("更新")
    @PostMapping("update")
    public ResponseData update(TbSpecialUser specialUser) {
        JpowerAssert.notEmpty(specialUser.getId(),JpowerError.Arg,"主键不可为空");
        return ReturnJsonUtil.status(specialUserService.updateById(specialUser));
    }
}
