package com.wlcb.jpower.chat.controller.customer.kefu;

import com.wlcb.jpower.chat.service.customer.ChatService;
import com.wlcb.jpower.module.base.enums.JpowerError;
import com.wlcb.jpower.module.base.exception.JpowerAssert;
import com.wlcb.jpower.module.base.vo.ResponseData;
import com.wlcb.jpower.module.common.controller.BaseController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @description: 客服动作接口 接入 转接 完成 转离线
 * @author: liwenfei
 * @time: 2020/12/17 9:13
 */
@Api(tags = "客服动作")
@RestController
@AllArgsConstructor
@RequestMapping("acction")
public class CustomerActionController extends BaseController {

    private ChatService chatService;

    /**
     * 接入用户
     *
     * @param cid   客服loginId
     * @param uid   用户loginId
     * @param appId 公众号appId
     * @return
     */
    @ApiOperation("接入用户")
    @PostMapping("access")
    public ResponseData access(@ApiParam(value = "客服loginId",required = true) @RequestParam(required = false) String cid,
                               @ApiParam(value = "用户openid",required = true) @RequestParam(required = false) String uid,
                               @ApiParam(value = "公众号appId",required = true) @RequestParam(required = false) String appId) {
        JpowerAssert.notEmpty(cid, JpowerError.Arg,"客服loginId不可为空");
        JpowerAssert.notEmpty(uid, JpowerError.Arg,"用户openid不可为空");
        JpowerAssert.notEmpty(appId, JpowerError.Arg,"公众号appId不可为空");
        return chatService.connectUser(cid, uid, appId);
    }

    /**
     * 转接用户
     *
     * @param cid   转接客服
     * @param tcid  转接目标客服
     * @param uid   被转接用户
     * @param appId 公众号appId
     * @return
     */
    @ApiOperation("转接用户")
    @PostMapping("transfer")
    public ResponseData transfer(@ApiParam(value = "转接客服loginId",required = true) @RequestParam(required = false) String cid,
                                 @ApiParam(value = "转接目标客服loginId",required = true) @RequestParam(required = false) String tcid,
                                 @ApiParam(value = "被转接用户openid",required = true) @RequestParam(required = false) String uid,
                                 @ApiParam(value = "公众号appId",required = true) @RequestParam(required = false) String appId) {
        JpowerAssert.notEmpty(cid, JpowerError.Arg,"转接客服loginId不可为空");
        JpowerAssert.notEmpty(tcid, JpowerError.Arg,"转接目标客服loginId不可为空");
        JpowerAssert.notEmpty(uid, JpowerError.Arg,"被转接用户openid不可为空");
        JpowerAssert.notEmpty(appId, JpowerError.Arg,"公众号appId不可为空");
        return chatService.transferUser(cid, tcid, uid, appId);
    }

    /**
     * 完成会话
     *
     * @param cid   客服
     * @param uid   用户
     * @param appId 公众号appId
     * @param type  咨询事项类型
     * @return
     */
    @ApiOperation("完成会话")
    @PostMapping("finish")
    public ResponseData finish(@ApiParam(value = "客服loginId",required = true) @RequestParam(required = false) String cid,
                               @ApiParam(value = "用户openid",required = true) @RequestParam(required = false) String uid,
                               @ApiParam(value = "公众号appId",required = true) @RequestParam(required = false) String appId,
                               @ApiParam(value = "咨询事项类型",required = true) @RequestParam(required = false) String type) {
        JpowerAssert.notEmpty(cid, JpowerError.Arg,"客服loginId不可为空");
        JpowerAssert.notEmpty(uid, JpowerError.Arg,"用户openid不可为空");
        JpowerAssert.notEmpty(appId, JpowerError.Arg,"公众号appId不可为空");
        JpowerAssert.notEmpty(type, JpowerError.Arg,"咨询事项类型");
        return chatService.finishUser(cid, uid, appId, type);
    }

    /**
     * 转离线
     *
     * @param cid 客服账号
     * @param tcid 转接目标账号
     * @param uid  用户openid
     * @param openId 客服openid
     * @param appId 公众号appId
     * @return
     */
    @ApiOperation("转离线")
    @PostMapping("/transfer/offLine")
    public ResponseData transferOffLine(@ApiParam(value = "客服loginId",required = true) @RequestParam(required = false) String cid,
                                        @ApiParam(value = "转接目标客服loginId",required = true) @RequestParam(required = false) String tcid,
                                        @ApiParam(value = "被转接用户openid",required = true) @RequestParam(required = false) String uid,
                                        @ApiParam(value = "客服openid",required = true) @RequestParam(required = false) String openId,
                                        @ApiParam(value = "公众号appId",required = true) @RequestParam(required = false) String appId) {
        JpowerAssert.notEmpty(cid, JpowerError.Arg,"转接客服loginId不可为空");
        JpowerAssert.notEmpty(tcid, JpowerError.Arg,"转接目标客服loginId不可为空");
        JpowerAssert.notEmpty(uid, JpowerError.Arg,"被转接用户openid不可为空");
        JpowerAssert.notEmpty(appId, JpowerError.Arg,"公众号appId不可为空");
        JpowerAssert.notEmpty(openId, JpowerError.Arg,"客服openid不可为空");
        return chatService.offLine(cid, tcid, uid, openId, appId);
    }
}
