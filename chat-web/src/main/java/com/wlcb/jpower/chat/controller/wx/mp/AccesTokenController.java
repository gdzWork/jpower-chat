package com.wlcb.jpower.chat.controller.wx.mp;

import com.wlcb.jpower.module.base.enums.JpowerError;
import com.wlcb.jpower.module.base.exception.JpowerAssert;
import com.wlcb.jpower.module.base.vo.ResponseData;
import com.wlcb.jpower.module.common.controller.BaseController;
import com.wlcb.jpower.module.common.utils.ReturnJsonUtil;
import com.wlcb.jpower.utils.WxMpInstance;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.SneakyThrows;
import me.chanjar.weixin.common.error.WxErrorException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author: liwenfei
 * @version: v1.0
 * @time: 2021/2/25 14:19
 * @desc: 获取accesstoken
 */
@Api(tags = "微信token管理")
@RestController
@RequestMapping("accessToken")
public class AccesTokenController extends BaseController {

    /**
     * 获取accessToken
     *
     * @param appId 公众号标识
     * @param flag  是否强制获取
     * @return
     */
    @ApiOperation("获取accessToken")
    @GetMapping("/get")
    @SneakyThrows(WxErrorException.class)
    public ResponseData<String> get(@ApiParam(value = "公众号ID",required = true) @RequestParam(value = "appId",required = false) String appId,
                                    @ApiParam(value = "是否强制刷新",defaultValue = "false") @RequestParam(defaultValue = "false",required = false) Boolean flag) {
        JpowerAssert.notEmpty(appId, JpowerError.Arg,"公众号ID不可为空");
        String accessToken = WxMpInstance.getWxMp().switchoverTo(appId).getAccessToken(flag);
        return ReturnJsonUtil.ok("操作成功", accessToken);
    }
}
