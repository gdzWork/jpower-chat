package com.wlcb.jpower.chat.controller.wx.mp;

import com.wlcb.jpower.constant.ChatConstant;
import com.wlcb.jpower.constant.RedisEunm;
import com.wlcb.jpower.module.common.controller.BaseController;
import com.wlcb.jpower.module.common.redis.RedisUtil;
import com.wlcb.jpower.module.common.utils.Fc;
import com.wlcb.jpower.utils.RedisHelpUtils;
import com.wlcb.jpower.utils.WxMpInstance;
import me.chanjar.weixin.mp.api.WxMpMessageRouter;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.message.WxMpXmlMessage;
import me.chanjar.weixin.mp.bean.message.WxMpXmlOutMessage;
import org.apache.commons.lang3.EnumUtils;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.HashMap;
import java.util.Map;

/**
 * 接收wx回调消息
 */
@ApiIgnore
@RestController
@RequestMapping("/receive/wx")
public class ReceiveMessageControler extends BaseController {

    /**
     * 公众号接入验证
     *
     * @param appId
     * @param signature
     * @param timestamp
     * @param nonce
     * @param echostr
     * @return
     */
    @GetMapping(produces = "text/plain;charset=utf-8")
    public String authGet(@RequestParam String appId,
                          @RequestParam(name = "signature", required = false) String signature,
                          @RequestParam(name = "timestamp", required = false) String timestamp,
                          @RequestParam(name = "nonce", required = false) String nonce,
                          @RequestParam(name = "echostr", required = false) String echostr) {

        logger.warn("\n接收到来自微信服务器的认证消息：[{},{}, {}, {}, {}]", appId, signature,
                timestamp, nonce, echostr);
        WxMpService wxMpService = WxMpInstance.getWxMp();
        if (Fc.isAnyBlank(signature, timestamp, nonce, echostr)) {
            throw new IllegalArgumentException("请求参数非法，请核实!");
        }
        if (!wxMpService.switchover(appId)) {
            throw new IllegalArgumentException(String.format("未找到对应appid=[%s]的配置，请核实！", appId));
        }
        if (wxMpService.checkSignature(timestamp, nonce, signature)) {
            return echostr;
        }
        return "请求非法";
    }

    /**
     * 公众号回调
     *
     * @param appId
     * @param requestBody
     * @param signature
     * @param timestamp
     * @param nonce
     * @param openid
     * @param encType
     * @param msgSignature
     * @return
     */
    @PostMapping(produces = "application/xml; charset=UTF-8")
    public String post(@RequestParam String appId,
                       @RequestBody String requestBody,
                       @RequestParam("signature") String signature,
                       @RequestParam("timestamp") String timestamp,
                       @RequestParam("nonce") String nonce,
                       @RequestParam("openid") String openid,
                       @RequestParam(name = "encrypt_type", required = false) String encType,
                       @RequestParam(name = "msg_signature", required = false) String msgSignature) {

        logger.debug("\n接收微信请求：[openid=[{}], [signature=[{}], encType=[{}], msgSignature=[{}],"
                        + " timestamp=[{}], nonce=[{}], requestBody=[\n{}\n] ",
                openid, signature, encType, msgSignature, timestamp, nonce, requestBody);

        WxMpService wxMpService = WxMpInstance.getWxMp();
        if (!wxMpService.switchover(appId)) {
            throw new IllegalArgumentException(String.format("未找到对应appid=[%s]的配置，请核实！", appId));
        }

        if (!wxMpService.checkSignature(timestamp, nonce, signature)) {
            throw new IllegalArgumentException("非法请求，可能属于伪造的请求！");
        }

        String out = null;
        if (encType == null) {
            // 明文传输的消息
            WxMpXmlMessage inMessage = WxMpXmlMessage.fromXml(requestBody);
            if (!msgDup(inMessage)) {
                return "";
            }
            WxMpXmlOutMessage outMessage = this.route(appId, inMessage);
            if (outMessage == null) {
                return "";
            } else {
                out = outMessage.toXml();
            }
        } else if ("aes".equalsIgnoreCase(encType)) {
            // TODO 密文传输消息 有问题
            WxMpXmlMessage wxMpXmlMessage = WxMpXmlMessage.fromEncryptedXml(requestBody, wxMpService.getWxMpConfigStorage(), timestamp, nonce, msgSignature);
            WxMpXmlOutMessage outMessage = this.route(appId, wxMpXmlMessage);
            if (outMessage == null) {
                return "";
            } else {
                out = outMessage.toXml();
            }
        }
        logger.debug("\n组装回复信息：{}", out);
        return out;
    }

    private WxMpXmlOutMessage route(String appId, WxMpXmlMessage message) {
        try {
            WxMpMessageRouter messageRouter = WxMpInstance.getMessageRouter();
            Map<String, Object> map = new HashMap<>();
            map.put("appId", appId);
            return messageRouter.route(appId, message, map);
        } catch (Exception e) {
            logger.error("路由消息时出现异常！", e);
        }

        return null;
    }

    private boolean msgDup(WxMpXmlMessage inMessage) {
        RedisUtil redisUtil = RedisHelpUtils.getRedisUtil();
        if (EnumUtils.isValidEnumIgnoreCase(ChatConstant.MessageTypeEnum.class, inMessage.getMsgType())) {
            Object obj = redisUtil.get(RedisEunm.MSG_DUP.getKeyPrefix() + inMessage.getMsgId());
            if (Fc.notNull(obj)) {
                return false;
            } else {
                redisUtil.set(RedisEunm.MSG_DUP.getKeyPrefix() + inMessage.getMsgId(), "", RedisEunm.MSG_DUP.getTime(), RedisEunm.MSG_DUP.timeUnit);
                return true;
            }
        } else {
            Object obj = redisUtil.get(RedisEunm.MSG_DUP.getKeyPrefix() + inMessage.getFromUser() + inMessage.getCreateTime() + inMessage.getEvent());
            if (Fc.notNull(obj)) {
                return false;
            } else {
                redisUtil.set(RedisEunm.MSG_DUP.getKeyPrefix() + inMessage.getFromUser() + inMessage.getCreateTime() + inMessage.getEvent(), "", RedisEunm.MSG_DUP.getTime(), RedisEunm.MSG_DUP.timeUnit);
                return true;
            }
        }
    }
}
