package com.wlcb.jpower.chat.controller.wx.mp;

import com.wlcb.jpower.chat.reply.ReplyEntity;
import com.wlcb.jpower.module.base.vo.ResponseData;
import com.wlcb.jpower.module.common.controller.BaseController;
import com.wlcb.jpower.module.common.utils.ReturnJsonUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.SneakyThrows;
import me.chanjar.weixin.common.error.WxErrorException;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author: fei
 * @version: v1.0
 * @time: 2021/5/8 14:26
 * @desc: 客服接口封装
 */
@Api(tags = "客服")
@RestController
@RequestMapping("kefu")
public class KefuController extends BaseController {

    /**
     * 发送消息 文本 图片  菜单  模板
     *
     * @param replyBodys
     * @param openid
     * @param appId
     * @return
     */
    @ApiOperation("发送消息")
    @PostMapping("send")
    @SneakyThrows(WxErrorException.class)
    public ResponseData send(@RequestBody(required = true) List<ReplyEntity.ReplyBody> replyBodys,
                             @ApiParam(value = "用户openid",required = true) @RequestParam(value = "openid", required = true) String openid,
                             @ApiParam(value = "公众号ID",required = true) @RequestParam(value = "appId", required = true) String appId) {
        ReplyEntity replyEntity = new ReplyEntity();
        replyEntity.setToUser(openid);
        replyEntity.setAppId(appId);
        replyEntity.setReplyBodys(replyBodys);
        replyEntity.sendReply();
        return ReturnJsonUtil.ok("操作成功");
    }
}
