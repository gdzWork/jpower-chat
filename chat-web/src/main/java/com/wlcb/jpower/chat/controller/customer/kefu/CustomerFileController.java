package com.wlcb.jpower.chat.controller.customer.kefu;

import com.wlcb.jpower.chat.entity.customer.TblCustomerMsg;
import com.wlcb.jpower.chat.service.customer.ChatService;
import com.wlcb.jpower.chat.service.customer.CustomerMsgService;
import com.wlcb.jpower.chat.service.file.ChatFileService;
import com.wlcb.jpower.im.cache.ImChatRecordCache;
import com.wlcb.jpower.module.base.enums.JpowerError;
import com.wlcb.jpower.module.base.exception.JpowerAssert;
import com.wlcb.jpower.module.base.vo.ResponseData;
import com.wlcb.jpower.module.common.controller.BaseController;
import com.wlcb.jpower.module.common.utils.ReturnJsonUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * 客服文件接口
 * @author mr.gmac
 */
@Api(tags = "客服文件")
@RestController
@AllArgsConstructor
@RequestMapping("file")
public class CustomerFileController extends BaseController {

    private CustomerMsgService customerMsgService;
    private ChatService chatService;
    private ChatFileService fileService;

    @ApiOperation("客服发送文件")
    @PostMapping(value = "/temp/addMedia")
    @SneakyThrows(IOException.class)
    public ResponseData addMedia(@ApiParam(value = "文件",required = true) @RequestParam(required = false) MultipartFile media,
                                 @ApiParam(value = "公众号appId",required = true) @RequestParam(required = false) String appId,
                                 @ApiParam(value = "接受用户ID(openId)",required = true) @RequestParam(required = false) String receiveId,
                                 @ApiParam(value = "发送用户ID(客服loginId)",required = true) @RequestParam(required = false) String sendId) {

        JpowerAssert.notNull(media, JpowerError.Arg,"文件不可为空");
        JpowerAssert.notEmpty(appId,JpowerError.Arg,"appId不可为空");
        JpowerAssert.notEmpty(receiveId,JpowerError.Arg,"receiveId不可为空");
        JpowerAssert.notEmpty(sendId,JpowerError.Arg,"sendId不可为空");

        ImChatRecordCache imChatRecordCache = ImChatRecordCache.getInstance(receiveId);
        JpowerAssert.notNull(imChatRecordCache, JpowerError.Business,"该用户会话不存在");

        TblCustomerMsg msg = customerMsgService.getById(imChatRecordCache.getDialogId());
        if (!sendId.equals(msg.getCurrentCid())) {
            return ReturnJsonUtil.fail("您还未接入该用户");
        }
        return chatService.sendFile(media, appId, receiveId, sendId);
    }

    @ApiOperation("新增文件")
    @PostMapping("/add")
    public ResponseData addMedia(MultipartFile file) {
        return fileService.fileSave(file);
    }

    @ApiOperation("下载文件")
    @GetMapping("show")
    @SneakyThrows(IOException.class)
    public void show(@ApiParam(value = "文件ID",required = true) @RequestParam(required = false) String id) {
        JpowerAssert.notEmpty(id,JpowerError.Arg,"id不可为空");
        chatService.showFile(id);
    }
}
