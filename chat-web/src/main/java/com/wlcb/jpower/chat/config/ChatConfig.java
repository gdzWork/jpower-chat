package com.wlcb.jpower.chat.config;

import com.wlcb.jpower.module.common.utils.Fc;
import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.ThreadPoolExecutor;

/**
 * @description:
 * @author: liwenfei
 */


@Configuration
@EnableAsync
public class ChatConfig {

    @Value("${spring.redis.host}")
    private String ip;
    @Value("${spring.redis.port}")
    private Integer port;
    @Value("${spring.redis.password:}")
    private String password;
    @Value("${spring.redis.database}")
    private int database;

    @Bean
    public RedissonClient getRedisson() {
        Config config = new Config();
        config.setThreads(8);
        config.setNettyThreads(16);
        config.useSingleServer()
                .setDnsMonitoringInterval(30000)
                .setAddress("redis://" + ip + ":" + port)
                .setTimeout(5000)
                .setConnectionPoolSize(32)
                .setConnectionMinimumIdleSize(12)
                .setPassword(Fc.isBlank(password)?null:password)
                .setDatabase(database);
        return Redisson.create(config);
    }

    @Bean
    public ThreadPoolTaskExecutor threadPool() {
        ThreadPoolTaskExecutor simpleThreadPool = new ThreadPoolTaskExecutor();
        simpleThreadPool.setCorePoolSize(5);
        simpleThreadPool.setMaxPoolSize(50);
        simpleThreadPool.setQueueCapacity(200);
        simpleThreadPool.setThreadNamePrefix("jChat-handler-");
        simpleThreadPool.setRejectedExecutionHandler(new ThreadPoolExecutor.DiscardPolicy());
        simpleThreadPool.initialize();
        return simpleThreadPool;
    }
}

