package com.wlcb.jpower.chat.controller.customer.company;

import com.github.pagehelper.PageInfo;
import com.wlcb.jpower.chat.entity.company.TbCompany;
import com.wlcb.jpower.chat.service.company.CompanyService;
import com.wlcb.jpower.module.base.enums.JpowerError;
import com.wlcb.jpower.module.base.exception.JpowerAssert;
import com.wlcb.jpower.module.base.vo.ResponseData;
import com.wlcb.jpower.module.common.controller.BaseController;
import com.wlcb.jpower.module.common.page.PaginationContext;
import com.wlcb.jpower.module.common.utils.FileUtil;
import com.wlcb.jpower.module.common.utils.ReturnJsonUtil;
import com.wlcb.jpower.module.common.utils.constants.ConstantsReturn;
import com.wlcb.jpower.module.common.utils.constants.ImportExportConstants;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.List;

/**
 * @description 通讯录单位api
 * @author liwenfei
 * @time 2021/1/8 13:36
 */
@Api(tags = "通讯录单位")
@RestController
@AllArgsConstructor
@RequestMapping("company")
public class CompanyController extends BaseController {

    private CompanyService companyService;

    /**
     * 新增一个通讯录
     *
     * @param company
     * @return
     */
    @ApiOperation(value = "新增", notes = "主键不用传")
    @PostMapping("add")
    public ResponseData add(TbCompany company) {
        JpowerAssert.notEmpty(company.getArea(), JpowerError.Arg,"area不可为空");
        JpowerAssert.notEmpty(company.getCompany(), JpowerError.Arg,"company不可为空");
        JpowerAssert.notEmpty(company.getCid(), JpowerError.Arg,"cid不可为空");
        JpowerAssert.notEmpty(company.getPerson(), JpowerError.Arg,"person不可为空");
        JpowerAssert.notEmpty(company.getPhone(), JpowerError.Arg,"phone不可为空");
        JpowerAssert.notEmpty(company.getRemark(), JpowerError.Arg,"remark不可为空");

        return ReturnJsonUtil.status(companyService.save(company));
    }

    /**
     * 删除
     *
     * @param id
     * @return
     */
    @ApiOperation(value = "删除")
    @DeleteMapping("delete")
    public ResponseData delete(@ApiParam("主键") @RequestParam String id) {
        JpowerAssert.notEmpty(id,JpowerError.Arg,"ID为空");
        return ReturnJsonUtil.status(companyService.removeById(id));
    }

    /**
     * 更新
     *
     * @param company
     * @return
     */
    @ApiOperation(value = "更新")
    @PutMapping("update")
    public ResponseData update(TbCompany company) {
        JpowerAssert.notEmpty(company.getId(),JpowerError.Arg,"ID为空");
        return ReturnJsonUtil.status(companyService.updateById(company));
    }

    /**
     * 查询
     *
     * @param company
     * @return
     */
    @ApiOperation(value = "列表")
    @GetMapping("list")
    public ResponseData<PageInfo<TbCompany>> list(TbCompany company) {
        PaginationContext.startPage();
        List<TbCompany> list = companyService.findList(company);
        return ReturnJsonUtil.ok("操作成功", new PageInfo<>(list));
    }

    @ApiOperation(value = "导入")
    @PostMapping("importFile")
    @SneakyThrows
    public ResponseData importFile(MultipartFile file) {
        if (file == null || file.isEmpty() || file.getSize() <= 0) {
            return ReturnJsonUtil.print(ConstantsReturn.RECODE_NULL, "上传文件不可为空", false);
        }
        File saveFile = FileUtil.saveFile(file, "xlsx,xls", ImportExportConstants.IMPORT_PATH);
        return companyService.importExcel(saveFile);
    }
}
