package com.wlcb.jpower.chat.controller.customer.kefu;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageInfo;
import com.wlcb.jpower.chat.entity.customer.TblCustomerMsg;
import com.wlcb.jpower.chat.service.customer.CustomerMsgService;
import com.wlcb.jpower.constant.DBConstant;
import com.wlcb.jpower.module.base.enums.JpowerError;
import com.wlcb.jpower.module.base.exception.JpowerAssert;
import com.wlcb.jpower.module.base.vo.ResponseData;
import com.wlcb.jpower.module.common.controller.BaseController;
import com.wlcb.jpower.module.common.page.PaginationContext;
import com.wlcb.jpower.module.common.utils.DateUtil;
import com.wlcb.jpower.module.common.utils.ReturnJsonUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * 客服会话记录
 */
@Api(tags = "客服会话记录")
@RequestMapping("chatLog")
@RestController
public class CustomerLogController extends BaseController {

    @Resource
    private CustomerMsgService customerMsgService;

    /**
     * 查询历史聊天记录
     *
     * @param chatLog
     * @param startTime
     * @param endTime
     * @return
     */
    @ApiOperation("查询历史聊天记录")
    @GetMapping("list")
    public ResponseData<PageInfo<TblCustomerMsg>> list(TblCustomerMsg chatLog,
                                                       @ApiParam(value = "开始时间",required = true) @RequestParam(required = false,defaultValue = "1970-01-01") Date startTime,
                                                       @ApiParam(value = "结束时间",required = true) @RequestParam(required = false,defaultValue = "2099-12-31") Date endTime) {

        JpowerAssert.notNull(startTime, JpowerError.Arg, "开始时间不可为空");
        JpowerAssert.notNull(endTime, JpowerError.Arg, "结束时间不可为空");

        PaginationContext.startPage();
        List<TblCustomerMsg> list = customerMsgService.find(chatLog, startTime, endTime);
        return ReturnJsonUtil.ok("操作成功", new PageInfo<>(list));
    }

    /**
     * 获取客服当天或历史接待完成量
     *
     * @param account
     * @param islishi 是否查询历史信息 0 否  1是
     * @return
     */
    @ApiOperation("获取客服当天或历史接待完成量")
    @GetMapping("/customer/finish/count")
    public ResponseData<Long> customerFinishCount(@ApiParam(value = "客服账号",required = true) String account,
                                                  @ApiParam(value = "是否历史 0:当天，1:历史",required = true) Integer islishi) {

        JpowerAssert.notEmpty(account,JpowerError.Arg,"客服账号不可为空");
        JpowerAssert.notNull(islishi,JpowerError.Arg,"是否历史不可为空");

        long count = 0;
        if (islishi == 0) {
            count = customerMsgService.count(new QueryWrapper<TblCustomerMsg>().lambda()
                    .eq(TblCustomerMsg::getCompleteCid, account)
                    .eq(TblCustomerMsg::getStatus, DBConstant.CustomerMsgEnum.FINISH.getType())
                    .apply("date(update_time) =" + "'" + DateUtil.today() + "'"));
        } else if (islishi == 1) {
            count = customerMsgService.count(new QueryWrapper<TblCustomerMsg>().lambda()
                    .eq(TblCustomerMsg::getCompleteCid, account)
                    .eq(TblCustomerMsg::getStatus, DBConstant.CustomerMsgEnum.FINISH.getType()));
        }
        return ReturnJsonUtil.ok("操作成功", count);
    }

    /**
     * 获取当天或历史咨询量
     *
     * @param islishi 是否查询历史信息 0 否  1是
     * @return
     */
    @ApiOperation("获取当天或历史咨询量")
    @GetMapping("/ask/count")
    public ResponseData<Long> askCount(@ApiParam(value = "是否历史 0:当天，1:历史",required = true) Integer islishi) {
        JpowerAssert.notNull(islishi,JpowerError.Arg,"是否历史不可为空");

        long count = 0;
        if (islishi == 0) {
            count = customerMsgService.count(new QueryWrapper<TblCustomerMsg>().lambda()
                    .apply("date(update_time) =" + "'" + DateUtil.today() + "'"));
        } else if (islishi == 1) {
            count = customerMsgService.count(new QueryWrapper<TblCustomerMsg>().lambda());

        }
        return ReturnJsonUtil.ok("操作成功", count);
    }

    /**
     * 查询接待客服
     *
     * @param startTime
     * @param endTime
     * @return
     */
    @ApiOperation("查询接待客服")
    @GetMapping("getRecept")
    public ResponseData<List<String>> getRecept(@ApiParam("开始时间") Date startTime,@ApiParam("结束时间") Date endTime) {
        List<String> recept = customerMsgService.findRecept(startTime, endTime);
        return ReturnJsonUtil.ok("", recept);
    }

    /**
     * 查询客服完成记录
     *
     * @param cid       客服
     * @param startTime 开始日期
     * @param endTime   结束日期
     * @return
     */
    @ApiOperation("查询客服完成记录")
    @GetMapping("listbyCid")
    public ResponseData<List<TblCustomerMsg>> listbyCid(@ApiParam(value = "客服账号",required = true) String cid,
                                                        @ApiParam(value = "开始时间",required = true) Date startTime,
                                                        @ApiParam(value = "结束时间",required = true) Date endTime) {

        JpowerAssert.notEmpty(cid,JpowerError.Arg,"客服账号不可为空");
        JpowerAssert.notNull(startTime,JpowerError.Arg,"开始时间不可为空");
        JpowerAssert.notNull(endTime,JpowerError.Arg,"结束时间不可为空");
        List<TblCustomerMsg> list = customerMsgService.findList(cid, startTime, endTime);
        return ReturnJsonUtil.ok("操作成功", list);
    }
}
