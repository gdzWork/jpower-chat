package com.wlcb.jpower.chat.controller.customer.kefu;


import com.wlcb.jpower.chat.entity.wxuser.WxUserInfo;
import com.wlcb.jpower.chat.service.customer.CustomerMsgService;
import com.wlcb.jpower.constant.ChatConfigConstant;
import com.wlcb.jpower.dbs.entity.TbCoreUser;
import com.wlcb.jpower.feign.UserClient;
import com.wlcb.jpower.im.cache.ImChatRecordCache;
import com.wlcb.jpower.im.cache.Session;
import com.wlcb.jpower.im.cache.SessionCache;
import com.wlcb.jpower.im.model.DataPacket;
import com.wlcb.jpower.module.base.enums.JpowerError;
import com.wlcb.jpower.module.base.exception.JpowerAssert;
import com.wlcb.jpower.module.base.vo.ResponseData;
import com.wlcb.jpower.module.common.controller.BaseController;
import com.wlcb.jpower.module.common.utils.BeanUtil;
import com.wlcb.jpower.module.common.utils.Fc;
import com.wlcb.jpower.module.common.utils.ReturnJsonUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 客服状态接口
 */
@Api(tags = "客服状态")
@RestController
@RequestMapping("chat_status")
@AllArgsConstructor
public class CustomerStatusController extends BaseController {

    private CustomerMsgService customerMsgService;
    private UserClient userClient;

    /**
     * 获取在线用户
     *
     * @return
     */
    @ApiOperation("获取在线用户")
    @GetMapping("/connect")
    public ResponseData connect() {
        Map<String, Session> sessions = SessionCache.getSessions();
        Collection<Session> values = sessions.values();
        return ReturnJsonUtil.ok("操作成功", values);
    }

    /**
     * 返回客服账号接入的用户信息
     *
     * @param account 客服账号
     * @return
     */
    @ApiOperation("返回客服账号接入的用户信息")
    @GetMapping("/getAccess")
    public ResponseData<List<WxUserInfo>> getAccess(@ApiParam("客服账号") String account) {
        JpowerAssert.notEmpty(account, JpowerError.Arg,"客服账号不可为空");
        List<WxUserInfo> wxUserInfos = customerMsgService.findByCurrentCid(account);
        return ReturnJsonUtil.ok("操作成功", wxUserInfos);
    }

    /**
     * 查看聊天记录
     *
     * @param openid 用户id
     * @return
     */
    @ApiOperation("查看聊天记录")
    @GetMapping("/getRecord")
    public ResponseData<List<DataPacket>> getRecord(@ApiParam("用户openid") String openid) {
        JpowerAssert.notEmpty(openid,JpowerError.Arg,"用户openid不可为空");
        ImChatRecordCache imChatRecordCache = ImChatRecordCache.getInstance(openid);
        if (Fc.isNull(imChatRecordCache)) {
            return ReturnJsonUtil.fail("未找到该用户的会话");
        }
        return ReturnJsonUtil.ok("操作成功", imChatRecordCache.getRecords());
    }

    /**
     * 清除所有用户信息
     *
     * @return
     */
    @ApiOperation("清除所有用户信息")
    @PostMapping("/connect/remove")
    public ResponseData remove() {
        SessionCache.removeAll();
        return ReturnJsonUtil.ok("操作成功");
    }

    /**
     * 移除用户
     *
     * @param account
     * @return
     */
    @ApiOperation("移除用户")
    @PostMapping("/closeUser")
    public ResponseData closeUser(@ApiParam("客服账号") String account) {
        JpowerAssert.notEmpty(account,JpowerError.Arg,"客服账号不可为空");
        SessionCache.remove(account);
        return ReturnJsonUtil.ok("操作成功");
    }

    /**
     * 查询用户是否在线
     *
     * @param loginId
     * @return
     */
    @ApiOperation("查询用户是否在线")
    @PostMapping("/isUserLogin")
    public ResponseData isUserLogin(@ApiParam("客服账号") String loginId) {
        JpowerAssert.notEmpty(loginId,JpowerError.Arg,"客服账号不可为空");
        boolean flag = SessionCache.isSession(loginId);
        if (!flag) {
            return ReturnJsonUtil.fail("离线");
        }
        return ReturnJsonUtil.ok("在线");
    }


    /**
     * 获取离线客服
     *
     * @return
     */
    @ApiOperation("获取离线客服")
    @GetMapping("/disConnect")
    public ResponseData<List<Session>> disConnect() {
        Map<String, Session> sessions = SessionCache.getSessions();
        List<TbCoreUser> data = userClient.listByUserType(ChatConfigConstant.UserType.CUSTOMER).getData();
        if (data.size() == 0) {
            return ReturnJsonUtil.ok("获取成功", Collections.EMPTY_LIST);
        }
        List<Session> ses = new ArrayList<>();
        for (TbCoreUser coreUser : data) {
            Session copySes = BeanUtil.copyProperties(coreUser, Session.class);
            ses.add(copySes);
        }

        Collection<Session> values = sessions.values();
        List<Session> collect = ses.stream().filter(item -> !values.stream().map(e -> e.getLoginId()).collect(Collectors.toList())
                .contains(item.getLoginId())).collect(Collectors.toList());

        return ReturnJsonUtil.ok("操作成功", collect);
    }

    @ApiOperation("获取客服数量")
    @GetMapping("/count")
    public ResponseData<Integer> count() {
        List<TbCoreUser> data = userClient.listByUserType(ChatConfigConstant.UserType.CUSTOMER).getData();
        return ReturnJsonUtil.ok("操作成功", data.size());
    }
}
