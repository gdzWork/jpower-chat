package com.wlcb.jpower.chat.controller.customer.kefu;

import com.wlcb.jpower.im.cache.SessionCache;
import com.wlcb.jpower.im.model.DataPacket;
import com.wlcb.jpower.module.common.controller.BaseController;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

/**
 * @description: 集群实现接口
 * @author: liwenfei
 * @time: 2021/1/4 13:30
 */
@Api(tags = "缓存清理，提供给负载调用")
@ApiIgnore
@RestController
@RequestMapping("/open")
public class ServerOpenController extends BaseController {

    @RequestMapping("/remove")
    public void remve(String loginId) {
        SessionCache.remove(loginId);
    }

    @RequestMapping("/send")
    public void send(String receiveId, @RequestBody DataPacket dataPacket) {
        SessionCache.write(receiveId, dataPacket);
    }
}
