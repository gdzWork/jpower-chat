package com.wlcb.jpower.chat.controller.demo;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageInfo;
import com.wlcb.jpower.chat.entity.demo.TbDemoRecord;
import com.wlcb.jpower.chat.service.demo.DemoRecordService;
import com.wlcb.jpower.module.base.vo.ResponseData;
import com.wlcb.jpower.module.common.controller.BaseController;
import com.wlcb.jpower.module.common.page.PaginationContext;
import com.wlcb.jpower.module.common.utils.ReturnJsonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

/**
 * @date: 2021/5/18 13:54
 * @author: fei
 * @desc: 聊天demo
 */
@ApiIgnore
@RestController()
@RequestMapping("/demo/record")
public class DemoRecordController extends BaseController {

    @Autowired
    private DemoRecordService recordService;


    @PostMapping("/add")
    public ResponseData add(TbDemoRecord record) {
        if (!recordService.save(record)) {
            return ReturnJsonUtil.fail("添加识别");
        }
        return ReturnJsonUtil.ok("添加成功", record);
    }

    @GetMapping("get")
    public ResponseData get(String groupId) {
        PaginationContext.startPage();
        List<TbDemoRecord> list = recordService.findList(groupId);
        return ReturnJsonUtil.ok("操作成功", JSON.toJSON(new PageInfo<>(list)));
    }

}
