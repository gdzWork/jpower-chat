package com.wlcb.jpower.chat.controller.customer.wx.customer;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.wlcb.jpower.chat.entity.customer.TblCustomerMsg;
import com.wlcb.jpower.chat.entity.matter.TbChatMatter;
import com.wlcb.jpower.chat.entity.wxuser.WxUserInfo;
import com.wlcb.jpower.chat.service.customer.ChatService;
import com.wlcb.jpower.chat.service.customer.CustomerMsgService;
import com.wlcb.jpower.chat.service.matter.ChatMatterService;
import com.wlcb.jpower.im.cache.ImChatRecordCache;
import com.wlcb.jpower.im.cache.SessionCache;
import com.wlcb.jpower.im.model.DataPacket;
import com.wlcb.jpower.module.base.enums.JpowerError;
import com.wlcb.jpower.module.base.exception.JpowerAssert;
import com.wlcb.jpower.module.base.vo.ResponseData;
import com.wlcb.jpower.module.common.controller.BaseController;
import com.wlcb.jpower.module.common.utils.Fc;
import com.wlcb.jpower.module.common.utils.ReturnJsonUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

/**
 * @description: 给微信端客服使用的接口
 * @author: liwenfei
 * @time: 2021/1/8 9:30
 */
@Api(tags = "给微信端客服使用的接口")
@RestController
@RequestMapping("/wx/customer")
@AllArgsConstructor
public class WxCustomerController extends BaseController {

    private CustomerMsgService customerMsgService;
    private ChatService chatService;
    private ChatMatterService chatMatterService;

    /**
     * 返回客服账号接入的用户信息
     *
     * @param account 客服账号
     * @return
     */
    @ApiOperation("返回客服账号接入的用户信息")
    @GetMapping("/chat_status/getAccess")
    public ResponseData<List<WxUserInfo>> getAccess(@ApiParam("客服账号") String account) {
        JpowerAssert.notEmpty(account,JpowerError.Arg,"客服账号不可为空");
        List<WxUserInfo> wxUserInfos = customerMsgService.findByCurrentCid(account);
        return ReturnJsonUtil.ok("操作成功", wxUserInfos);
    }

    /**
     * 查看聊天记录
     *
     * @param openid 用户id
     * @return
     */
    @ApiOperation("查看聊天记录")
    @GetMapping("/chat_status/getRecord")
    public ResponseData<List<DataPacket>> getRecord(@ApiParam("用户openid") String openid) {

        JpowerAssert.notEmpty(openid,JpowerError.Arg,"用户openid不可为空");

        ImChatRecordCache imChatRecordCache = ImChatRecordCache.getInstance(openid);
        if (Fc.isNull(imChatRecordCache)) {
            return ReturnJsonUtil.fail("未找到该用户的会话");
        }
        return ReturnJsonUtil.ok("操作成功", imChatRecordCache.getRecords());
    }

    /**
     * 完成会话
     *
     * @param cid   客服
     * @param uid   用户
     * @param appId 公众号id
     * @param type  咨询事项类型
     * @return
     */
    @ApiOperation("完成会话")
    @PostMapping("/acction/finish")
    public ResponseData finish(@ApiParam("客服账号") String cid,@ApiParam("用户openid") String uid,@ApiParam("公众号id") String appId,@ApiParam("咨询事项类型") String type) {
        JpowerAssert.notEmpty(cid,JpowerError.Arg,"客服账号不可为空");
        JpowerAssert.notEmpty(uid,JpowerError.Arg,"用户openid不可为空");
        JpowerAssert.notEmpty(appId,JpowerError.Arg,"公众号id不可为空");
        JpowerAssert.notEmpty(type,JpowerError.Arg,"咨询事项类型不可为空");
        return chatService.finishUser(cid, uid, appId, type);
    }

    /**
     * 移除用户
     *
     * @param account
     * @return
     */
    @ApiOperation("移除用户")
    @PostMapping("/chat_status/closeUser")
    public ResponseData closeUser(@ApiParam("客服账号") String account) {
        SessionCache.remove(account);
        return ReturnJsonUtil.ok("操作成功");
    }

    /**
     * 查询事项
     *
     * @param parentId
     * @return
     */
    @ApiOperation("查询事项")
    @GetMapping("/matter/get")
    public ResponseData<List<TbChatMatter>> get(@ApiParam("事项父级ID") String parentId) {
        JpowerAssert.notEmpty(parentId,JpowerError.Arg,"事项父级ID不可为空");

        List<TbChatMatter> list = chatMatterService.list(new QueryWrapper<TbChatMatter>().lambda()
                .eq(TbChatMatter::getParentId, parentId).eq(TbChatMatter::getIsDeleted, 0));
        return ReturnJsonUtil.ok("操作成功", list);
    }

    /**
     * 发送文件接口
     *
     * @param media
     * @param appId
     * @param receiveId
     * @param sendId
     * @return
     */
    @ApiOperation("客服发送文件")
    @PostMapping(value = "/file/temp/addMedia")
    @SneakyThrows(IOException.class)
    public ResponseData addMedia(@ApiParam("文件") MultipartFile media,@ApiParam("公众号ID") String appId,@ApiParam("接受用户openid") String receiveId,@ApiParam("发送客服账号") String sendId) {
        JpowerAssert.notNull(media, JpowerError.Arg,"文件不可为空");
        JpowerAssert.notEmpty(appId,JpowerError.Arg,"appId不可为空");
        JpowerAssert.notEmpty(receiveId,JpowerError.Arg,"receiveId不可为空");
        JpowerAssert.notEmpty(sendId,JpowerError.Arg,"sendId不可为空");

        ImChatRecordCache imChatRecordCache = ImChatRecordCache.getInstance(receiveId);
        JpowerAssert.notNull(imChatRecordCache, JpowerError.Business,"该用户会话不存在");

        TblCustomerMsg msg = customerMsgService.getById(imChatRecordCache.getDialogId());
        if (!sendId.equals(msg.getCurrentCid())) {
            return ReturnJsonUtil.fail("您还未接入该用户");
        }
        return chatService.sendFile(media, appId, receiveId, sendId);
    }

    /**
     * 获取文件接口
     *
     * @param id
     */
    @ApiOperation("获取文件")
    @GetMapping("/file/show")
    @SneakyThrows(IOException.class)
    public void show(@ApiParam("文件ID") String id) {
        chatService.showFile(id);
    }
}
