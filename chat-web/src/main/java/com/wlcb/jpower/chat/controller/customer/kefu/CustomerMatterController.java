package com.wlcb.jpower.chat.controller.customer.kefu;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.wlcb.jpower.chat.entity.matter.TbChatMatter;
import com.wlcb.jpower.chat.service.matter.ChatMatterService;
import com.wlcb.jpower.module.base.enums.JpowerError;
import com.wlcb.jpower.module.base.exception.JpowerAssert;
import com.wlcb.jpower.module.base.vo.ResponseData;
import com.wlcb.jpower.module.common.controller.BaseController;
import com.wlcb.jpower.module.common.utils.ReturnJsonUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @description
 * @author liwenfei
 * @time 2020/12/28 14:24
 */
@Api(tags = "客服咨询事项")
@RestController
@RequestMapping("matter")
public class CustomerMatterController extends BaseController {

    @Resource
    private ChatMatterService chatMatterService;

    /**
     * 新增咨询事项
     *
     * @param chatMatter
     * @return
     */
    @ApiOperation("新增咨询事项")
    @PostMapping("add")
    public ResponseData add(TbChatMatter chatMatter) {
        JpowerAssert.notEmpty(chatMatter.getName(), JpowerError.Arg,"名称不可为空");
        return ReturnJsonUtil.status(chatMatterService.save(chatMatter));
    }

    /**
     * 删除咨询事项
     *
     * @param id
     * @return
     */
    @ApiOperation("删除咨询事项")
    @DeleteMapping("delete")
    public ResponseData delete(@ApiParam("事项ID") String id) {

        JpowerAssert.notEmpty(id,JpowerError.Arg,"事项ID不可为空");

        boolean save = chatMatterService.update(new UpdateWrapper<TbChatMatter>().lambda()
                .set(TbChatMatter::getIsDeleted, 1)
                .eq(TbChatMatter::getId, id)
                .eq(TbChatMatter::getIsDeleted, 0));
        return ReturnJsonUtil.status(save);
    }

    /**
     * 查询事项
     *
     * @param parentId
     * @return
     */
    @ApiOperation("查询事项")
    @GetMapping("/get")
    public ResponseData<List<TbChatMatter>> get(@ApiParam("父级事项ID") String parentId) {
        JpowerAssert.notEmpty(parentId,JpowerError.Arg,"父级事项ID不可为空");

        List<TbChatMatter> list = chatMatterService.list(new QueryWrapper<TbChatMatter>().lambda()
                .eq(TbChatMatter::getParentId, parentId));
        return ReturnJsonUtil.ok("操作成功", list);
    }

}
