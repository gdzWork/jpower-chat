package com.wlcb.jpower.chat.controller.customer.kefu;

import com.wlcb.jpower.chat.entity.customer.TbCustomerSettings;
import com.wlcb.jpower.chat.service.customer.CustomerSettingService;
import com.wlcb.jpower.constant.DBConstant;
import com.wlcb.jpower.module.base.enums.JpowerError;
import com.wlcb.jpower.module.base.exception.JpowerAssert;
import com.wlcb.jpower.module.base.vo.ResponseData;
import com.wlcb.jpower.module.common.controller.BaseController;
import com.wlcb.jpower.module.common.utils.Fc;
import com.wlcb.jpower.module.common.utils.ReturnJsonUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @description: 客服设置接口
 * @author: liwenfei
 * @time: 2021/1/26 14:35
 */
@Api(tags = "客服设置")
@RestController
@RequestMapping("customer")
@AllArgsConstructor
public class CustomerSettingController extends BaseController {

    private CustomerSettingService settingService;

    /**
     * 获取设置信息
     *
     * @param cid 客服loginid
     * @return
     */
    @ApiOperation("获取设置信息")
    @GetMapping("getByCid")
    public ResponseData<TbCustomerSettings> getByCid(@ApiParam("客服账号") String cid) {
        JpowerAssert.notEmpty(cid, JpowerError.Arg,"客服账号不可为空");
        TbCustomerSettings settings = settingService.findByCid(cid);
        return ReturnJsonUtil.ok("操作成功", settings);
    }

    /**
     * @param id              修改id
     * @param autoReply       开启自动回复
     * @param autoAccess      是否开启自动接入
     * @param autoAccessReply 是否开启接入自动回复
     * @param allowTransfer   是否开启限制转接功能
     * @param content         自动回复内容
     * @return
     */
    @ApiOperation("修改设置信息")
    @PutMapping("update")
    public ResponseData update(@ApiParam("设置ID") String id,@ApiParam("开启自动回复") Boolean autoReply,@ApiParam("是否开启自动接入") Boolean autoAccess,@ApiParam("是否开启接入自动回复") Boolean autoAccessReply,@ApiParam("是否开启限制转接功能") Boolean allowTransfer,@ApiParam("自动回复内容") String content) {

        JpowerAssert.notEmpty(id,JpowerError.Arg,"设置ID不可为空");

        TbCustomerSettings settings = new TbCustomerSettings();
        settings.setId(id);
        if (!Fc.isNull(autoAccess)) {
            settings.setAutoAccess(autoAccess ? DBConstant.EnableEnum.OPEN.getType() : DBConstant.EnableEnum.CLOSR.getType());
        }
        if (Fc.notNull(autoReply)) {
            settings.setAutoReply(autoReply ? DBConstant.EnableEnum.OPEN.getType() : DBConstant.EnableEnum.CLOSR.getType());
        }
        if (Fc.notNull(autoAccessReply)) {
            JpowerAssert.notEmpty(content,JpowerError.Arg,"回复内容不能为空");

            settings.setAutoAccessReply(autoAccessReply ? DBConstant.EnableEnum.OPEN.getType() : DBConstant.EnableEnum.CLOSR.getType());
            settings.setReplyContent(content);
        }

        if (Fc.notNull(allowTransfer)) {
            settings.setAllowTransfer(allowTransfer ? DBConstant.EnableEnum.OPEN.getType() : DBConstant.EnableEnum.CLOSR.getType());
        }

        return ReturnJsonUtil.status(settingService.updateById(settings));
    }
}
