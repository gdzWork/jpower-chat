package com.wlcb.jpower.chat.task;

import cn.hutool.core.date.DatePattern;
import com.wlcb.jpower.chat.entity.wxapp.TblWxApp;
import com.wlcb.jpower.chat.service.wx.mp.WxAppService;
import com.wlcb.jpower.constant.DBConstant;
import com.wlcb.jpower.constant.RedisEunm;
import com.wlcb.jpower.module.common.utils.DateUtil;
import com.wlcb.jpower.module.common.utils.Fc;
import com.wlcb.jpower.utils.CreateJedisOps;
import com.wlcb.jpower.utils.WxMpInstance;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.config.impl.WxMpDefaultConfigImpl;
import me.chanjar.weixin.mp.config.impl.WxMpRedisConfigImpl;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @description: 更新wxmpconfig
 * @author: liwenfei
 * @time: 2021/2/23 15:40
 */
@Component
public class WxMpTask {
    @Autowired
    private WxAppService wxAppService;
    @Autowired
    private Environment env;

    @Scheduled(cron = "1 0 0 * * ?")
    public void updateWxMpConfig() {
        String host = env.getProperty("spring.redis.host");
        String port = env.getProperty("spring.redis.port");
        String password = env.getProperty("spring.redis.password");
        String database = env.getProperty("spring.redis.database");
        String fileParentPath = env.getProperty("jpowerchat.file-path");
        List<TblWxApp> wxApps = wxAppService.findByStaus(DBConstant.EnableEnum.OPEN.getType());
        WxMpService wxMpService = WxMpInstance.getWxMp();
        if (Fc.isNotEmpty(wxApps)){
            CreateJedisOps.Ops ops = CreateJedisOps.Ops.builder()
                    .config(new GenericObjectPoolConfig())
                    .host(host)
                    .port(Integer.valueOf(port))
                    .timeout(3000)
                    .password(password)
                    .database(Integer.valueOf(database))
                    .ssl(false).build();
            wxMpService.setMultiConfigStorages(wxApps.stream().map(x -> {
                WxMpDefaultConfigImpl configStorage = new WxMpRedisConfigImpl(CreateJedisOps.getInstance(ops), RedisEunm.WX_MP.getKeyPrefix());
                configStorage.setSecret(x.getAppSecret());
                configStorage.setAesKey(x.getAesKey());
                configStorage.setAppId(x.getAppId());
                configStorage.setToken(x.getToken());
                File parentPath = new File(fileParentPath + x.getAppId() + File.separator + DateUtil.format(DateUtil.date(), DatePattern.PURE_DATE_FORMATTER));
                if (!parentPath.exists()) {
                    parentPath.mkdirs();
                }
                configStorage.setTmpDirFile(parentPath);
                return configStorage;
            }).collect(Collectors.toMap(WxMpDefaultConfigImpl::getAppId, a -> a, (o, n) -> o)));
        }
    }
}
