package com.wlcb.jpower;

import com.thebeastshop.forest.springboot.annotation.ForestScan;
import com.wlcb.jpower.annotation.EnableJpowerFeignClients;
import com.wlcb.jpower.constant.ChatConfigConstant;
import com.wlcb.jpower.module.common.deploy.JpowerApplication;
import com.wlcb.jpower.module.common.deploy.service.annotation.JpowerCloudApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @author mr.gmac
 */
@EnableScheduling
@JpowerCloudApplication
@EnableJpowerFeignClients
@ForestScan("com.wlcb.jpower.chat.api")
public class ChatWebApplication {

    public static void main(String[] args) {
        JpowerApplication.run(ChatConfigConstant.ChatAppConstant.CHAT, ChatWebApplication.class, args);
    }
}
