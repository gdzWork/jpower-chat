package com.wlcb.jpower.chat.pojo.baidu;

import lombok.Data;

/**
 * @author: liwenfei
 * @version: v1.0
 * @time: 2021/3/9 16:07
 * @desc:
 */
@Data
public class AccesstokenResponse {
    private String refreshToken;
    private Long expiresIn;
    private String sessionKey;
    private String accessToken;
    private String scope;
    private String sessionSecret;
}
