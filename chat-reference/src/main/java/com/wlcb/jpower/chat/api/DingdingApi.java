package com.wlcb.jpower.chat.api;

import com.dtflys.forest.annotation.Body;
import com.dtflys.forest.annotation.DataVariable;
import com.dtflys.forest.annotation.PostRequest;
import com.wlcb.jpower.chat.pojo.dingding.DRequest;

/**
 * @author: liwenfei
 * @version: v1.0
 * @time: 2021/3/2 14:59
 * @desc: 钉钉接口
 */
public interface DingdingApi {

    @PostRequest(
            url = "https://oapi.dingtalk.com/robot/send?access_token=${token}&sign=${sign}&timestamp=${timestamp}",
            contentType = "application/json"
    )
    String msgSend(@DataVariable("token") String token,@DataVariable("timestamp") Long timestamp, @DataVariable("sign") String sign, @Body DRequest request);
}
