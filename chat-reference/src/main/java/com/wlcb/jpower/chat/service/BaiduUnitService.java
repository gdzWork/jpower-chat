package com.wlcb.jpower.chat.service;

public interface BaiduUnitService {

    String accesstoken();

    String say(String query,String userId);
}
