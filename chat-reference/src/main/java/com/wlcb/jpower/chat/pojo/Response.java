package com.wlcb.jpower.chat.pojo;

import com.alibaba.fastjson.JSON;
import lombok.Getter;
import lombok.Setter;

/**
 * @author: liwenfei
 * @version: v1.0
 * @time: 2021/4/9 13:34
 * @desc:
 */
@Getter
@Setter
public class Response<T> {
    private Integer code;
    private String msg;
    private String message;
    private T data;
    private Boolean status;

    @Override
    public String toString() {
        return JSON.toJSONString(Response.this);
    }
}


