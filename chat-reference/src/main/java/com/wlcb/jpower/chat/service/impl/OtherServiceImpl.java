package com.wlcb.jpower.chat.service.impl;

import com.alibaba.fastjson.JSON;
import com.wlcb.jpower.chat.api.OtherApi;
import com.wlcb.jpower.chat.service.OtherService;
import com.wlcb.jpower.module.common.utils.ExceptionUtil;
import com.wlcb.jpower.module.common.utils.ReturnJsonUtil;
import lombok.extern.slf4j.Slf4j;
import me.chanjar.weixin.mp.bean.message.WxMpXmlMessage;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author mr.g
 * @time 2021/3/12 9:41
 */
@Slf4j
@Service
public class OtherServiceImpl implements OtherService {

    @Resource
    private OtherApi otherApi;

    @Override
    public String request(String url, String appId, WxMpXmlMessage message) {
        log.info("route message to other ");
        try {
            return otherApi.route(url, appId, message);
        } catch (Exception e) {
            log.error("请求{}失败:{}",url, ExceptionUtil.getStackTraceAsString(e));
            return JSON.toJSONString(ReturnJsonUtil.fail("请求失败"));
        }
    }
}
