package com.wlcb.jpower.chat.api;

import com.dtflys.forest.annotation.DataVariable;
import com.dtflys.forest.annotation.Request;
import com.wlcb.jpower.chat.pojo.Response;
import com.wlcb.jpower.chat.pojo.baidu.SearchResponse;

import java.util.List;

/**
 * @author: liwenfei
 * @version: v1.0
 * @time: 2021/4/20 17:26
 * @desc:
 */
public interface SearchApi {


    @Request(
            url = "https://avuejs.com/bd?content=${content}",
            type = "get"
    )
    Response<List<SearchResponse>> query(@DataVariable("content") String content);
}
