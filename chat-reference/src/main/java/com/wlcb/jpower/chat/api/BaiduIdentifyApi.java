package com.wlcb.jpower.chat.api;

import com.dtflys.forest.annotation.DataVariable;
import com.dtflys.forest.annotation.Request;

import java.util.Map;

public interface BaiduIdentifyApi {

    @Request(
            url = "https://aip.baidubce.com/oauth/2.0/token?grant_type=client_credentials&client_id=${clientId}&client_secret=${clientSecret}",
            dataType = "json"
    )
    Map<String, String> accesstoken(@DataVariable("clientId") String clientId, @DataVariable("clientSecret") String clientSecret);


    @Request(
            url = "https://aip.baidubce.com/rest/2.0/image-classify/v1/plant?access_token=${1}",
            type = "post",
            data = "url=${0}&baike_num=3",
            contentType = "application/x-www-form-urlencoded"
    )
    String image(String url,String accesstoken);
}
