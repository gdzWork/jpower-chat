package com.wlcb.jpower.chat.api;

import com.dtflys.forest.annotation.Body;
import com.dtflys.forest.annotation.DataVariable;
import com.dtflys.forest.annotation.Request;
import com.wlcb.jpower.chat.pojo.baidu.UnitChatRequest;

public interface BaiduUnitApi {

    @Request(
            url = "https://aip.baidubce.com/rpc/2.0/unit/service/chat?access_token=${accesstoken}",
            type = "post",
            contentType = "application/json"
    )
    String query(@DataVariable("accesstoken") String accesstoken, @Body UnitChatRequest request);
}
