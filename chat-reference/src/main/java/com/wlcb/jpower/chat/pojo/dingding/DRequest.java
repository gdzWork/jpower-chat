package com.wlcb.jpower.chat.pojo.dingding;

import com.alibaba.fastjson.JSON;
import lombok.Data;

import java.util.List;

/**
 * @author: liwenfei
 * @version: v1.0
 * @time: 2021/3/1 16:07
 * @desc:
 */
@Data
public class DRequest {
    private String msgtype = "markdown";
    private MarkDown markdown;
    private At at;

    public DRequest(String msgtype, MarkDown markdown, At at) {
        this.at = at;
        this.markdown = markdown;
        this.msgtype = msgtype;
    }

    public DRequest(String apiName, String appId, String mpName, String ex) {
        MarkDown markDown = new MarkDown();
        markDown.setText(toText(apiName, appId, mpName, ex));
        this.markdown = markDown;
        this.at = new At();
    }

    public DRequest(String apiName, String appId, String mpName, String ex, List<String> phones) {
        MarkDown markDown = new MarkDown();
        markDown.setText(toText(apiName, appId, mpName, ex));
        At at = new At();
        at.setPhones(phones);
        this.markdown = markDown;
        this.at = at;
    }

    public DRequest(String apiName, String from, String ex) {
        MarkDown markDown = new MarkDown();
        markDown.setText(toText(apiName, from, ex));
        this.markdown = markDown;
        this.at = new At();
    }

    public DRequest(String apiName, String from, String ex, List<String> phones) {
        MarkDown markDown = new MarkDown();
        markDown.setText(toText(apiName, from, ex));
        this.markdown = markDown;
        At at = new At();
        at.setPhones(phones);
        this.at = at;
    }

    @Override
    public String toString() {
        return JSON.toJSONString(DRequest.this);
    }


    @Data
    private static class MarkDown {
        private String title = "异常通知";
        private String text;

        @Override
        public String toString() {
            return JSON.toJSONString(MarkDown.this);
        }
    }

    @Data
    private static class At {
        private List<String> phones;
        private Boolean isAtAll = true;

        @Override
        public String toString() {
            return JSON.toJSONString(At.this);
        }
    }

    private String toText(String apiName, String appId, String mpName, String ex) {
        return "## 调用" + apiName + "接口异常 \n\n> ------ \n\n> - **公众号标识：**<font color='#00BFFF'>" + appId + "</font> \n\n> - **公众号名称：**<font color='#00BFFF'>" + mpName + "</font> \n\n> - **异常信息：**<font color='#FF3030'>" + ex + "</font>";
    }

    private String toText(String apiName, String from, String ex) {
        return "## 调用" + apiName + "接口异常 \n\n> ------ \n\n> - **服务名称：**<font color='#00BFFF'>" + from + "</font> \n\n> - **异常信息：**<font color='#FF3030'>" + ex + "</font>";
    }
}
