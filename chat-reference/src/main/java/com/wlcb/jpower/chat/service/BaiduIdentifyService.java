package com.wlcb.jpower.chat.service;

public interface BaiduIdentifyService {

    String accesstoken();

    String imageIdentify(String url);
}
