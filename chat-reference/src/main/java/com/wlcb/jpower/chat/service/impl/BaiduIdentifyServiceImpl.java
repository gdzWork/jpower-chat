package com.wlcb.jpower.chat.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.wlcb.jpower.chat.api.BaiduIdentifyApi;
import com.wlcb.jpower.chat.pojo.baidu.ImageIdentifyResponse;
import com.wlcb.jpower.chat.service.BaiduIdentifyService;
import com.wlcb.jpower.constant.RedisEunm;
import com.wlcb.jpower.module.common.redis.RedisUtil;
import com.wlcb.jpower.module.common.utils.Fc;
import com.wlcb.jpower.properties.JpowerChatBaiduIdentifyProperties;
import com.wlcb.jpower.utils.RedisHelpUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * @author: liwenfei
 * @version: v1.0
 * @time: 2021/3/9 16:09
 * @desc: 百度接口实现
 */
@Slf4j
@Service
public class BaiduIdentifyServiceImpl implements BaiduIdentifyService {

    @Resource
    private BaiduIdentifyApi baiduIdentifyApi;
    @Autowired
    private JpowerChatBaiduIdentifyProperties properties;

    @Override
    public String accesstoken() {
        log.warn("baidu accesstoken");
        RedisUtil redisUtil = RedisHelpUtils.getRedisUtil();
        Object obj = redisUtil.get(RedisEunm.BAIDU_ACCESSTOKEN.getKeyPrefix() + properties.getAppKey());
        if (!Fc.isNull(obj)) {
            return obj.toString();
        }
        Map<String, String> result = baiduIdentifyApi.accesstoken(properties.getAppKey(), properties.getSecretKey());
        String accessToken = result.get("access_token");
        if (Fc.isBlank(accessToken)) {
            log.error("baidu image Identify accesstoken fail");
            return null;
        }
        redisUtil.set(RedisEunm.BAIDU_ACCESSTOKEN.getKeyPrefix() + properties.getAppKey(), accessToken,
                Long.valueOf(result.get("expires_in")), TimeUnit.SECONDS);
        log.warn("baidu accesstoken :{}", accessToken);
        return accessToken;
    }

    @Override
    public String imageIdentify(String url) {
        log.warn("identify image :{}", url);
        String accesstoken = accesstoken();
        if (Fc.isBlank(accesstoken)) {
            return "图片信息识别失败";
        }
        String imageContent = baiduIdentifyApi.image(url, accesstoken);
        ImageIdentifyResponse identifyResponse = JSONObject.parseObject(imageContent, ImageIdentifyResponse.class);
        if (Fc.isNull(identifyResponse)) {
            log.error("baidu image Identify fail");
            return "图片信息识别失败";
        }
        List<ImageIdentifyResponse.Result> infos = identifyResponse.getResult();
        ImageIdentifyResponse.Result result = infos.stream().sorted(new Comparator<ImageIdentifyResponse.Result>() {
            @Override
            public int compare(ImageIdentifyResponse.Result o1, ImageIdentifyResponse.Result o2) {
                if (o1.getScore() > o2.getScore()) {
                    return -1;
                } else {
                    return 1;
                }
            }
        }).collect(Collectors.toList()).get(0);
        if (Fc.isNull(result.getBaikeInfo())) {
            log.warn("图片信息识别失败,或非植物");
            return "图片信息识别失败,或非植物";
        }
        if (Fc.isNull(result.getBaikeInfo().getDescription())) {
            log.warn(result.getName());
            return result.getName();
        }
        log.warn(result.getBaikeInfo().getDescription());
        return result.getBaikeInfo().getDescription();
    }
}
