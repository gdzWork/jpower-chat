package com.wlcb.jpower.chat.service.impl;

import com.wlcb.jpower.chat.api.SearchApi;
import com.wlcb.jpower.chat.pojo.Response;
import com.wlcb.jpower.chat.pojo.baidu.SearchResponse;
import com.wlcb.jpower.chat.service.SearchService;
import com.wlcb.jpower.module.common.utils.Fc;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author: liwenfei
 * @version: v1.0
 * @time: 2021/4/20 17:33
 * @desc: 百度搜索
 */
@Service
public class SearchServiceImpl implements SearchService {
    @Resource
    private SearchApi searchApi;

    @Override
    public String query(String content) {

        Response<List<SearchResponse>> query = searchApi.query(content);
        if (query.getCode() == 200) {
            List<SearchResponse> data = query.getData();
            if (Fc.isNull(data) || data.size() <= 0) {
                return "";
            }
            String info = "";
            for (SearchResponse datum : data) {
                if (Fc.isNotBlank(datum.getInfo())) {
                    info = datum.getInfo();
                    break;
                }
            }
            return info;
        } else {
            return "";
        }
    }
}
