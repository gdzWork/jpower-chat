package com.wlcb.jpower.chat.pojo.baidu;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.util.List;

/**
 * @author: liwenfei
 * @version: v1.0
 * @time: 2021/3/9 16:58
 * @desc:
 */
@Data
public class ImageIdentifyResponse {

    @JSONField(name = "log_id")
    private String logId;
    @JSONField(name = "result")
    private List<Result> result;

    @Data
    public static class BaikeInfo {
        @JSONField(name = "baike_url")
        private String baikeUrl;
        @JSONField(name = "description")
        private String description;
    }

    @Data
    public static class Result {
        @JSONField(name = "score")
        private Double score;
        @JSONField(name = "name")
        private String name;
        @JSONField(name = "baike_info")
        private BaikeInfo baikeInfo;
    }
}
