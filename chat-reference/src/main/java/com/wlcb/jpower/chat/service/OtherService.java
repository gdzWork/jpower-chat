package com.wlcb.jpower.chat.service;

import me.chanjar.weixin.mp.bean.message.WxMpXmlMessage;

/**
 * @author: liwenfei
 * @version: v1.0
 * @time: 2021/3/12 9:40
 * @desc:
 */
public interface OtherService {

    String request(String url, String appId, WxMpXmlMessage message);

}
