package com.wlcb.jpower.chat.pojo.baidu;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.annotation.JSONField;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @author: liwenfei
 * @version: v1.0
 * @time: 2021/3/31 14:00
 * @desc:
 */
@Getter
@Setter
public class UnitChatResponse {
    @JSONField(name = "error_code")
    private String errorCode;
    @JSONField(name = "result")
    private Result result;


    @Getter
    @Setter
    public static class Result {
        @JSONField(name = "version")
        private String version;
        @JSONField(name = "timestamp")
        private String timestamp;
        @JSONField(name = "service_id")
        private String serviceId;
        @JSONField(name = "log_id")
        private String logId;
        @JSONField(name = "session_id")
        private String sessionId;
        @JSONField(name = "interaction_id")
        private String interactionId;
        @JSONField(name = "response_list")
        private List<Response> responseList;

        @Override
        public String toString() {
            return JSON.toJSONString(Result.this);
        }
    }

    @Getter
    @Setter
    public static class Response {
        @JSONField(name = "status")
        private String status;
        @JSONField(name = "msg")
        private String msg;
        @JSONField(name = "origin")
        private String origin;
        @JSONField(name = "action_list")
        private List<Action> actionList;

        @Override
        public String toString() {
            return JSON.toJSONString(Response.this);
        }
    }

    @Getter
    @Setter
    public static class Action {
        @JSONField(name = "action_id")
        private String actionId;
        @JSONField(name = "confidence")
        private String confidence;
        @JSONField(name = "custom_reply")
        private String customReply;
        @JSONField(name = "say")
        private String say;
        @JSONField(name = "type")
        private String type;

        @Override
        public String toString() {
            return JSON.toJSONString(Action.this);
        }
    }

    @Override
    public String toString() {
        return JSON.toJSONString(UnitChatResponse.this);
    }
}
