package com.wlcb.jpower.chat.pojo.baidu;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author: liwenfei
 * @version: v1.0
 * @time: 2021/4/20 17:36
 * @desc:
 */
@Getter
@Setter
public class SearchResponse implements Serializable {
    private static final long serialVersionUID = 374419900807667567L;

    private String title;
    private String href;
    private String info;
    private Boolean isBk;
}
