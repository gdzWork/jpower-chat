package com.wlcb.jpower.chat.service;

import com.wlcb.jpower.chat.pojo.dingding.DRequest;

/**
 * @author: liwenfei
 * @version: v1.0
 * @time: 2021/3/2 15:02
 * @desc:
 */
public interface DingdingService {

    Boolean sendEx(DRequest request);
}
