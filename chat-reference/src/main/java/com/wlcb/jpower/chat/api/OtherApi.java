package com.wlcb.jpower.chat.api;

import com.dtflys.forest.annotation.Body;
import com.dtflys.forest.annotation.DataVariable;
import com.dtflys.forest.annotation.Request;
import me.chanjar.weixin.mp.bean.message.WxMpXmlMessage;

/**
 * @author mr.g
 * @version v1.1
 * @time 2021/3/12 9:36
 */
public interface OtherApi {

    @Request(
            url = "${url}?appId=${appId}",
            type = "post",
            contentType = "application/json"
    )
    String route(@DataVariable("url") String url, @DataVariable("appId") String appId, @Body WxMpXmlMessage json);
}
