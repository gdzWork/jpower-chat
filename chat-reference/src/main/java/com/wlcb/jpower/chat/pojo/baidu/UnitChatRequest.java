package com.wlcb.jpower.chat.pojo.baidu;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.annotation.JSONField;
import lombok.Getter;
import lombok.Setter;

/**
 * @author: liwenfei
 * @version: v1.0
 * @time: 2021/3/31 11:38
 * @desc: https://ai.baidu.com/ai-doc/UNIT/bkiy75fn9
 */
@Getter
@Setter
public class UnitChatRequest {

    @JSONField(name = "version")
    private String version = "2.0";
    @JSONField(name = "service_id")
    private String serviceId;
    @JSONField(name = "log_id")
    private String logId;
    @JSONField(name = "session_id")
    private String sessionId;
    @JSONField(name = "request")
    private Request request;

    @Getter
    @Setter
    public static class Request {
        @JSONField(name = "user_id")
        private String userId;
        @JSONField(name = "query")
        private String query;

        @Override
        public String toString() {
            return JSON.toJSONString(Request.this);
        }
    }

    @Override
    public String toString() {
        return JSON.toJSONString(UnitChatRequest.this);
    }

}
