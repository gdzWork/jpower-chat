package com.wlcb.jpower.chat.message.handler;

import com.wlcb.jpower.chat.builder.TextBuilder;
import com.wlcb.jpower.constant.RedisEunm;
import com.wlcb.jpower.utils.RedisHelpUtils;
import com.wlcb.jpower.utils.WxMpInstance;
import me.chanjar.weixin.common.session.WxSessionManager;
import me.chanjar.weixin.mp.api.WxMpKefuService;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.kefu.WxMpKefuMessage;
import me.chanjar.weixin.mp.bean.message.WxMpXmlMessage;
import me.chanjar.weixin.mp.bean.message.WxMpXmlOutMessage;
import org.springframework.stereotype.Component;

import java.util.Map;

import static me.chanjar.weixin.common.api.WxConsts.EventType;

/**
 * 自定义菜单事件
 */
@Component
public class MenuHandler extends AbstractHandler {

    @Override
    public WxMpXmlOutMessage handle(WxMpXmlMessage wxMessage,
                                    Map<String, Object> context, WxMpService weixinService,
                                    WxSessionManager sessionManager) {
        try {
            if (EventType.VIEW.equals(wxMessage.getEvent())) {
                return null;
            }
            if (EventType.CLICK.equals(wxMessage.getEvent())) {
                if ("UNIT".equals(wxMessage.getEventKey())) {
                    RedisHelpUtils.getRedisUtil().set(RedisEunm.BAIDU_UNIT_SESSION.getKeyPrefix() + wxMessage.getFromUser(), "", RedisEunm.BAIDU_UNIT_SESSION.getTime(), RedisEunm.BAIDU_UNIT_SESSION.getTimeUnit());
                    return new TextBuilder().build("进入UNIT", wxMessage, weixinService);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
