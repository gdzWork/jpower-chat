package com.wlcb.jpower.chat.message.handler;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.wlcb.jpower.chat.entity.wxuser.WxUserInfo;
import com.wlcb.jpower.chat.service.wxuser.WxUserInfoService;
import com.wlcb.jpower.module.common.utils.Fc;
import me.chanjar.weixin.common.session.WxSessionManager;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.message.WxMpXmlMessage;
import me.chanjar.weixin.mp.bean.message.WxMpXmlOutMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * 取消关注事件
 */
@Component
public class UnsubscribeHandler extends AbstractHandler {
    @Autowired
    private WxUserInfoService wxUserInfoService;

    @Override
    public WxMpXmlOutMessage handle(WxMpXmlMessage wxMessage,
                                    Map<String, Object> context, WxMpService wxMpService,
                                    WxSessionManager sessionManager) {
        try {
            String appId = context.get("appId").toString();
            WxUserInfo wxUserInfo = wxUserInfoService.findByOpenidAndAppId(wxMessage.getFromUser(), appId);
            if (Fc.notNull(wxUserInfo)) {
                wxUserInfoService.update(new UpdateWrapper<WxUserInfo>().lambda()
                        .set(WxUserInfo::getSubscribeTime, 0).eq(WxUserInfo::getId, wxUserInfo.getId()));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
