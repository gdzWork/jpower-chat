package com.wlcb.jpower.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @date: 2021/5/10 14:04
 * @author: fei
 * @desc:
 */
@Data
@Component
@ConfigurationProperties(prefix = "jpowerchat.baidu.unit")
public class JpowerChatBaiduUnitProperties {
    /**
     * 是否打开unit转发
     */
    private Boolean open = false;
    /**
     * 是否打开unit转发 appid
     */
    private String[] appid;
    /**
     * unit机器人id
     */
    private String serviceId;
    /**
     * unit接口调用参数
     */
    private String appKey;
    /**
     * unit接口调用参数
     */
    private String secretKey;
    /**
     * 是否转1
     */
    private boolean isReplyOne = true;
    /**
     * 是否转1 appid
     */
    private String[] isReplyOneAppid;
}
