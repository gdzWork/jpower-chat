package com.wlcb.jpower.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @date: 2021/5/10 14:09
 * @author: fei
 * @desc:
 */
@Data
@Component
@ConfigurationProperties(prefix = "jpowerchat")
public class JpowerChatProperties {

    /**
     * netty服务端口
     */
    private int serverPort = 801;
    /**
     * netty服务读超时
     */
    private int idleTimeRead = 75;
    /**
     * netty服务写超时
     */
    private int idleTimeWrite = 75;
    /**
     * netty服务最终超时
     */
    private int idleTimeAll = 95;
    /**
     * 文件保存path
     */
    private String filePath;
    /**
     * 离线回复模板链接
     */
    private String offLineUrl;
    /**
     * 离线回复模板id
     */
    private String offLineId;
    /**
     * 是否开启数据库配置回复
     */
    private Boolean isDbFixed = false;
    /**
     * 是否开启数据库配置回复appid
     */
    private String[] isDbFixedAppid;
    /**
     * 是否进入客服
     */
    private Boolean isKefu = false;
    /**
     * 是否进入客服appid
     */
    private String[] isKeFuAppid;
    /**
     * 第三方回复
     */
    private List<Other> others;
    /**
     * 是否开启百度搜索定时任务自动回复
     */
    private Boolean autoBaiduReply = false;
    /**
     * 是否开启百度搜索定时任务自动回复appid
     */
    private String[] autoBaiduReplyAppid;
    /**
     * 是否开启unit接入自动回复
     */
    private Boolean autoUnitReply = false;
    /**
     * 是否开启unit接入自动回复appid
     */
    private String[] autoUnitReplyAppid;

    /**
     * 钉钉token
     */
    private String dingDingToken = "";

    @Data
    public class Other {
        /**
         * 请求地址（只支持post，固定appid参数和body格式WxMpXmlMessage）
         */
        private String url;
        /**
         * 支持该地址的AppId
         */
        private List<String> appIds;
        /**
         * 排序
         */
        private int sort;
    }
}
