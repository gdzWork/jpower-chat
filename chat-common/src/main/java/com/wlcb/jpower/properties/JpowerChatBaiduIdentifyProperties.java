package com.wlcb.jpower.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @date: 2021/5/10 13:35
 * @author: fei
 * @desc:
 */
@Data
@Component
@ConfigurationProperties(prefix = "jpowerchat.baidu.identify")
public class JpowerChatBaiduIdentifyProperties {

    /**
     * 是否打开百度识别
     */
    private Boolean open = false;
    /**
     * 发送给百度需要识别图片的链接
     */
    private String url;
    /**
     * 图片识别接口调用参数
     */
    private String appKey;
    /**
     * 图片识别接口调用参数
     */
    private String secretKey;
    /**
     * 支持图片失败的appid
     */
    private String[] appid;
}
