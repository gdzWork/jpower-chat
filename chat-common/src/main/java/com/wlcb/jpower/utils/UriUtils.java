package com.wlcb.jpower.utils;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @time 2021/3/15 22:16
 * @author: liwenfei
 * @version:
 * @desc:
 */
public class UriUtils {

    /**
     * @param url
     * @return
     */
    public static Map<String, String> getUrlPramNameAndValue(String url) {
        String regEx = "(\\?|&+)(.+?)=([^&]*)";
        Pattern p = Pattern.compile(regEx);
        Matcher m = p.matcher(url);
        Map<String, String> paramMap = new LinkedHashMap<String, String>();
        while (m.find()) {
            String paramName = m.group(2);
            String paramVal = m.group(3);
            paramMap.put(paramName, paramVal);
        }
        return paramMap;
    }

    /**
     * 获取uri的token
     *
     * @param uri
     * @return
     */
    public static String getAuth(String uri) {
        Map<String, String> paramMap = getUrlPramNameAndValue(uri);
        return paramMap.get("jpower-auth");
    }

    /**
     * 获取uri的wxcid
     *
     * @param uri
     * @return
     */
    public static String getWxcid(String uri) {
        Map<String, String> paramMap = getUrlPramNameAndValue(uri);
        return paramMap.get("wxcid");
    }
}
