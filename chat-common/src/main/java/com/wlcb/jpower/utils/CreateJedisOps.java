package com.wlcb.jpower.utils;

import com.wlcb.jpower.module.common.utils.Fc;
import lombok.Builder;
import me.chanjar.weixin.common.redis.JedisWxRedisOps;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import redis.clients.jedis.JedisPool;

/**
 * @description: 创建wxmp需要的JedisWxRedisOps
 * @author: liwenfei
 * @time: 2021/2/23 15:53
 */
public class CreateJedisOps {

    private static JedisWxRedisOps jedisWxRedisOps = null;

    private CreateJedisOps(){}
    public static JedisWxRedisOps getInstance(Ops ops) {
        if (Fc.isNull(jedisWxRedisOps)) {
            JedisPool jedisPool = new JedisPool(ops.config, ops.host, ops.port, ops.timeout,ops.password, ops.database, ops.ssl);
            return new JedisWxRedisOps(jedisPool);
        }
        return jedisWxRedisOps;
    }

    @Builder
    public static class Ops {
        private GenericObjectPoolConfig config;
        private String host;
        private Integer port;
        private Integer timeout;
        private String password;
        private Integer database;
        private boolean ssl;
    }
}
