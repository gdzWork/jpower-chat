package com.wlcb.jpower.utils;

import com.wlcb.jpower.module.common.utils.SpringUtil;
import me.chanjar.weixin.mp.api.WxMpMessageRouter;
import me.chanjar.weixin.mp.api.WxMpService;

/**
 * @description:
 * @author: liwenfei
 * @time: 2021/2/3 11:26
 */
public class WxMpInstance {
    public static WxMpService getWxMp() {
        return SpringUtil.getBean(WxMpService.class);
    }

    public static WxMpMessageRouter getMessageRouter() {
        return SpringUtil.getBean(WxMpMessageRouter.class);
    }
}
