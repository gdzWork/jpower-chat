package com.wlcb.jpower.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.concurrent.TimeUnit;

/**
 * @author liwenfei
 * @version V1.0
 * @Package com.wlcb.jpower.chat.constant
 * @date 2020/10/25 23:23
 */
@AllArgsConstructor
@Getter
public enum RedisEunm {

    /**
     * 聊天记录缓存
     */
    CUSTOMERCHATRECORD("jpowerchat:kefu:chatrecord:", null, null),
    /**
     * 用户与会话绑定
     */
    CUSTOMERCHATDIALOG("jpowerchat:kefu:chatdialog:", null, null),

    /**
     * 用户与会话绑定
     */
    CUSTOMERACCESS("jpowerchat:kefu:chataccess:", null, null),
    /**
     * 在线用户
     */
    USER_ONLINE("jpowerchat:user:", null, null),
    /**
     * 微信token
     */
    WX_MP("jpowerchat:wx:mp", null, null),
    /**
     * 百度图像识别token
     */
    BAIDU_ACCESSTOKEN("jpowerchat:baidu:accesstoken:", null, null),
    /**
     * 语义临时id
     */
    ANSWER_USERID("jpowerchat:answer:userId:", 3L, TimeUnit.HOURS),

    /**
     * 消息排重
     */
    MSG_DUP("jpowerchat:msgdup:", 15L, TimeUnit.SECONDS),
    /**
     * 创建unit会话
     */
    BAIDU_UNIT_SESSION("jpowerchat:baidu:unit:session:", 300L, TimeUnit.SECONDS),
    /**
     * unit上次说的话
     */
    BAIDU_UNIT_SAY("jpowerchat:baidu:unit:say:", 300L, TimeUnit.SECONDS),
    /**
     * 锁
     */
    LOCK("jpowerchat:lock:", 10L, TimeUnit.SECONDS),
    /**
     * 是否订阅
     */
    SUB("jpowerchat:sub:", null, null),


    AUTO_UNIT_SWITCH("jpowerchat:baidu.unit:auto:", null, null),

    AUTOACCESS("jpowerchat:kefu:autoaccess", null, null);


    public String keyPrefix;
    public Long time;
    public TimeUnit timeUnit;
}
