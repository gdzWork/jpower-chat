package com.wlcb.jpower.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

public class ChatConstant {

    @AllArgsConstructor
    @Getter
    public enum ReplyEnum {

        SATISFIED(1, "非常满意"),

        NOSATISFIED(2, "满意"),

        ORDINARY(3, "不满意"),

        QUIT(4, "退出客服"),

        QUIT_UNIT(5, "quit"),

        QUIT_OPEN(6, "open");

        public Integer type;
        public String value;
    }

    /**
     * 消息包动作类型
     */
    @AllArgsConstructor
    @Getter
    public enum ChatActionEnum {

        /**
         * 1,心跳
         */
        HEARTBEAT(1, "心跳"),
        /**
         * 2, "聊天消息"
         */
        CUSTOMER_USER_MESSAGE(2, "消息"),
        /**
         * 3,接入用户
         */
        CUSTOMER_ACCESS_USER(3, "接入用户"),

        /**
         * 4,转接用户
         */
        CUSTOMER_TRANSFER_USER(4, "转接用户"),
        /**
         * 5,转离线
         */
        CUSTOMER_DISCONNECT_USER(5, "转离线"),

        /**
         * 6,会话完成
         */
        CUSTOMER_FINISH_USER(6, "会话完成"),
        /**
         * 7，等待列表
         */
        WAIT_USER(7, "等待列表"),
        /**
         * 8,系统推送
         */
        SYSTEM_SEND(8, "系统推送"),
        /**
         * 9，用户信息
         */
        USERINFO(9, "用户信息"),
        /**
         * 9,群聊
         */
        CUSTOMER_GROUP_CHAT(10, "群聊"),
        /**
         * 消息识别结果
         */
        CUSTOMER_USER_MESSAGE_SHIBIE(11, "图片识别结果"),
        /**
         *语音识别结果
         */
        CUSTOMER_VOICE(12, "语音识别结果"),
        /**
         * UNIT回复
         */
        CUSTOMER_UNIT(13, "UNIT回复");

        public Integer type;
        public String value;
    }

    /**
     * 消息类型
     */
    @AllArgsConstructor
    @Getter
    public enum MessageTypeEnum {

        /**
         * text,发送文本消息
         */
        TEXT("text", "文本消息"),

        /**
         * image, 发送图片消息
         */
        IMAGE("image", "图片消息"),
        /**
         * voice,发送语音消息
         */
        VOICE("voice", "语音消息"),
        /**
         * video,发送视频消息
         */
        VIDEO("video", "视频消息"),
        /**
         * music,发送音乐消息
         */
        MUSIC("music", "音乐消息"),
        /**
         * news,发送图文消息（点击跳转到外链）
         */
        NEWS("news", "图文消息"),
        /**
         * mpnews,发送图文消息（点击跳转到图文消息页面）
         */
        MPNEWS("mpnews", "图文消息"),
        /**
         * msgmenu,发送菜单消息
         */
        MSGMENU("msgmenu", "菜单消息"),
        /**
         * tmsg,模板消息
         */
        TEMPLATEMSG("tmsg", "模板消息");


        public String type;
        public String value;
    }

    @Getter
    @AllArgsConstructor
    public enum WxSubEnum {

        /**
         * 1, 未订阅
         */
        UNSUBSCRIBE(0, "unsubscribe"),

        /**
         * 2, "聊天模块"
         */
        SUBSCRIBE(1, "subscribe");

        public Integer type;
        public String value;
    }

}
