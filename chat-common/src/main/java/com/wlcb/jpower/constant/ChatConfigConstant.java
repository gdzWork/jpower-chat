package com.wlcb.jpower.constant;

public class ChatConfigConstant {

    public interface ChatAppConstant {

        String CHAT = "jpower-chat";

        String CHAT_SERVER = "jpower-chat-server";

    }

    public interface UserType {
        Integer CUSTOMER = 4;
        Integer OTHER = 9;
    }
}
