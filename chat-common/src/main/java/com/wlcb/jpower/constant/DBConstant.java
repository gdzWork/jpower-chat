package com.wlcb.jpower.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

public class DBConstant {

    @AllArgsConstructor
    @Getter
    public enum EnableEnum {

        /**
         * 0,停用
         */
        OPEN(1, "开启"),

        /**
         * 1, 启用
         */
        CLOSR(0, "关闭");

        private Integer type;
        private String value;
    }

    @Getter
    @AllArgsConstructor
    public enum CustomerMsgEnum {

        /**
         * 0,待接入
         */
        WAIT_ACCESS(1, "待接入"),
        /**
         * 1,已接入
         */
        ACCESSED(2, "已接入"),
        /**
         * 2,完成会话
         */
        FINISH(3, "完成会话"),
        /**
         * 4,未完成
         */
        NOTFINISSH(4, "未完成"),
        /**
         * 5,微信端客服接入中
         */
        OFFLINEACCESSED(5, "微信端客服接入中"),
        /**
         *5,用户主动退出
         */
        USER_QUIT(6, "用户主动退出");

        private Integer type;
        private String value;
    }
}
