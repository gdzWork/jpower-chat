package com.wlcb.jpower.wx.mp.wxuser;

import com.alibaba.fastjson.JSON;
import com.google.gson.annotations.SerializedName;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class UserInfoResponse implements Serializable {

    private static final long serialVersionUID = -4660151696611588424L;
    @SerializedName("subscribe")
    private Integer subscribe;
    @SerializedName("openid")
    private String openid;
    @SerializedName("nickname")
    private String nickname;
    @SerializedName("sex")
    private Integer sex;
    @SerializedName("language")
    private String language;
    @SerializedName("city")
    private String city;
    @SerializedName("province")
    private String province;
    @SerializedName("country")
    private String country;
    @SerializedName("headimgurl")
    private String headimgurl;
    @SerializedName("subscribe_time")
    private String subscribeTime;
    @SerializedName("subscribe")
    private String unionid;
    @SerializedName("subscribe")
    private String remark;
    @SerializedName("subscribe")
    private Integer groupid;
    @SerializedName("tagid_list")
    private List<Integer> tagidList;
    @SerializedName("subscribe_scene")
    private String subscribeScene;
    @SerializedName("qr_scene")
    private Integer qrScene;
    @SerializedName("qr_scene_str")
    private String qrSceneStr;

    private String dialogId;

    private String name;
    private String address;
    private Boolean isSpecial;

    @Override
    public String toString() {
        return JSON.toJSONString(UserInfoResponse.this);
    }
}
