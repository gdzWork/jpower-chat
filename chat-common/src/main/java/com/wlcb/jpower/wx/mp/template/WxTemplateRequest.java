package com.wlcb.jpower.wx.mp.template;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
@AllArgsConstructor
public class WxTemplateRequest implements Serializable {

    private static final long serialVersionUID = 2738598399085298805L;

    @ApiModelProperty("接收者openid列表")
    private List<String> toUsers;
    @ApiModelProperty("模板ID")
    private String templateId;
    @ApiModelProperty("跳转地址")
    private String url;
    @ApiModelProperty("配置")
    private MiniProgram miniProgram;
    @ApiModelProperty("模板配置")
    private List<WxMpTemplateData> data;
    @ApiModelProperty("公众号ID")
    private String appId;

    public WxTemplateRequest(){

    }

    public static class MiniProgram implements Serializable {

        private static final long serialVersionUID = -3502971203892204349L;
        @ApiModelProperty("公众号ID")
        private String appid;
        @ApiModelProperty("路径")
        private String pagePath;
        @ApiModelProperty("是否使用路径")
        private boolean usePath = false;

        public MiniProgram(){

        }

        public String getAppid() {
            return this.appid;
        }

        public String getPagePath() {
            return this.pagePath;
        }

        public boolean isUsePath() {
            return this.usePath;
        }

        public void setAppid(String appid) {
            this.appid = appid;
        }

        public void setPagePath(String pagePath) {
            this.pagePath = pagePath;
        }
    }

    public static class WxMpTemplateData implements Serializable {
        private static final long serialVersionUID = 5236278203350696172L;
        @ApiModelProperty("参数名称")
        private String name;
        @ApiModelProperty("参数值")
        private String value;
        @ApiModelProperty("颜色")
        private String color;

        public WxMpTemplateData() {
        }

        public WxMpTemplateData(String name, String value) {
            this.name = name;
            this.value = value;
        }

        public WxMpTemplateData(String name, String value, String color) {
            this.name = name;
            this.value = value;
            this.color = color;
        }

        public String getName() {
            return this.name;
        }

        public String getValue() {
            return this.value;
        }

        public String getColor() {
            return this.color;
        }

        public void setName(String name) {
            this.name = name;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public void setColor(String color) {
            this.color = color;
        }
    }
}
