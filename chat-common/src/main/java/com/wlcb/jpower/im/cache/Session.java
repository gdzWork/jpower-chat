package com.wlcb.jpower.im.cache;

import com.alibaba.fastjson.JSON;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.wlcb.jpower.module.common.auth.UserInfo;
import io.netty.channel.Channel;

import java.io.Serializable;

/**
 * @description: 管理用户信息和channel
 * @author: liwenfei
 * @time: 2020/11/13 9:41
 */
public class Session extends UserInfo implements Serializable {

    private static final long serialVersionUID = -829142852310384466L;

    @JsonIgnore
    private transient Channel channel;

    //登录时间
    private Long bindTime;
    //用户与服务器绑定
    @JsonIgnore
    private String position;

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public Channel getChannel() {
        return channel;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }

    public Long getBindTime() {
        return bindTime;
    }

    public void setBindTime(Long bindTime) {
        this.bindTime = bindTime;
    }

    @Override
    public String toString() {
        return JSON.toJSONString(Session.this);
    }
}
