package com.wlcb.jpower.im.cache;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.wlcb.jpower.constant.RedisEunm;
import com.wlcb.jpower.im.model.DataPacket;
import com.wlcb.jpower.module.common.utils.Fc;
import com.wlcb.jpower.utils.RedisHelpUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @description: 聊天记录缓存
 * @author: liwenfei
 * @time: 2020/12/21 23:37
 */
public class ImChatRecordCache {

    private String userId;
    private String dialogId;
    private String cid;

    public static ImChatRecordCache getInstance(String userId, String dialogId) {
        return new ImChatRecordCache(userId, dialogId);
    }

    public static ImChatRecordCache getInstance(String userId) {
        Object dialogId = RedisHelpUtils.getRedisUtil().get(RedisEunm.CUSTOMERCHATDIALOG.getKeyPrefix() + userId);
        String cid = null;
        Object obj = RedisHelpUtils.getRedisUtil().get(RedisEunm.CUSTOMERACCESS.getKeyPrefix() + userId);
        if (!Fc.isNull(obj)) {
            cid = obj.toString();
        }
        return dialogId == null ? null : new ImChatRecordCache(userId, dialogId.toString(), cid);
    }

    private ImChatRecordCache(String userId, String dialogId) {
        this.userId = userId;
        this.dialogId = dialogId;
    }

    private ImChatRecordCache(String userId, String dialogId, String cid) {
        this.userId = userId;
        this.dialogId = dialogId;
        this.cid = cid;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getDialogId() {
        return dialogId;
    }

    public void setDialogId(String dialogId) {
        this.dialogId = dialogId;
    }

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        RedisHelpUtils.getRedisUtil().set(RedisEunm.CUSTOMERACCESS.getKeyPrefix() + userId, cid);
        this.cid = cid;
    }

    /**
     * 绑定用户和会话
     */
    public ImChatRecordCache bindInit() {
        Object result = RedisHelpUtils.getRedisUtil().get(RedisEunm.CUSTOMERCHATDIALOG.getKeyPrefix() + userId);
        if (!Fc.isNull(result)) {
            this.dialogId = result.toString();
        } else {
            RedisHelpUtils.getRedisUtil().set(RedisEunm.CUSTOMERCHATDIALOG.getKeyPrefix() + userId, dialogId);
        }
        return this;
    }

    /**
     * 获取消息记录
     *
     * @return
     */
    public List<DataPacket> getRecords() {
        Object record = RedisHelpUtils.getRedisUtil().get(RedisEunm.CUSTOMERCHATRECORD.keyPrefix + dialogId);
        if (!Fc.isNull(record)) {
            return JSONObject.parseArray(record.toString(), DataPacket.class);
        }
        return Collections.emptyList();

    }

    /**
     * 设置消息记录
     * 聊天记录的实现方式有问题
     *
     * @param dataPacket
     */
    public void setRecords(DataPacket dataPacket) {
        RedisHelpUtils.RedisLock redisLock = RedisHelpUtils.getRedisLock();
        boolean tryLock = redisLock.tryLock(userId, TimeUnit.SECONDS, 10, 5);
        if(tryLock){
            try {
                List<DataPacket> records = getRecords();
                List<DataPacket> list = new ArrayList<>();
                if (records.size() > 0) {
                    list.addAll(records);
                    list.add(dataPacket);
                } else {
                    list.add(dataPacket);
                }
                RedisHelpUtils.getRedisUtil().set(RedisEunm.CUSTOMERCHATRECORD.keyPrefix + dataPacket.getDialogId(), JSON.toJSONString(list));
            }finally {
                redisLock.runlock(userId);
            }
        }


    }

    /**
     * 清除会话
     */
    public void remove() {
        String dialogId = RedisHelpUtils.getRedisUtil().get(RedisEunm.CUSTOMERCHATDIALOG.keyPrefix + userId).toString();
        RedisHelpUtils.getRedisUtil().remove(RedisEunm.CUSTOMERCHATRECORD.keyPrefix + dialogId);
        RedisHelpUtils.getRedisUtil().remove(RedisEunm.CUSTOMERCHATDIALOG.keyPrefix + userId);
        RedisHelpUtils.getRedisUtil().remove(RedisEunm.CUSTOMERACCESS.keyPrefix + userId);
    }

    public void removeCid() {
        RedisHelpUtils.getRedisUtil().remove(RedisEunm.CUSTOMERACCESS.keyPrefix + userId);
    }
}
