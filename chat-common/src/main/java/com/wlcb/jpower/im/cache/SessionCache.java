package com.wlcb.jpower.im.cache;

import com.alibaba.fastjson.JSONObject;
import com.wlcb.jpower.constant.ChatConfigConstant;
import com.wlcb.jpower.constant.RedisEunm;
import com.wlcb.jpower.im.group.ChatChannelGroup;
import com.wlcb.jpower.im.model.DataPacket;
import com.wlcb.jpower.module.common.redis.RedisUtil;
import com.wlcb.jpower.module.common.utils.Fc;
import com.wlcb.jpower.module.common.utils.HttpClient;
import com.wlcb.jpower.module.common.utils.SpringUtil;
import com.wlcb.jpower.properties.JpowerChatProperties;
import com.wlcb.jpower.utils.RedisHelpUtils;
import io.netty.channel.Channel;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import org.springframework.core.env.Environment;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

/**
 * @description: 会话缓存
 * @author: liwenfei
 * @time: 2020/12/29 10:36
 */
public class SessionCache {
    private static final Map<String, Session> SESSIONS = new ConcurrentHashMap<>();
    private static Environment env;
    private static JpowerChatProperties properties;
    final static String SESSION_REMOVE = "/open/remove";
    final static String SESSION_SEND = "/open/send";

    static {
        properties = SpringUtil.getBean(JpowerChatProperties.class);
        env = SpringUtil.getBean(Environment.class);
    }

    public static boolean addSession(Session session) {
        try {
            if (session.getUserType().intValue() != ChatConfigConstant.UserType.CUSTOMER) {
                session.getChannel().close();
            } else {
                ChatChannelGroup.add(session.getChannel());
                SESSIONS.put(session.getLoginId(), session);
                session.setPosition(InetAddress.getLocalHost().getHostAddress() + ":" + env.getProperty("server.port"));
                RedisHelpUtils.getRedisUtil().set(RedisEunm.USER_ONLINE.getKeyPrefix() + session.getLoginId(), session.toString(), (long)properties.getIdleTimeAll(), TimeUnit.SECONDS);
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * 通过登录id删除
     *
     * @param loginId
     */
    public static void remove(String loginId) {
        Session session = SESSIONS.get(loginId);
        if (Fc.isNull(session)) {
            Session redisSession = getSession(loginId);
            if (Fc.notNull(redisSession)) {
                HttpClient.doGet("http://" + redisSession.getPosition() + SESSION_REMOVE + "?loginId=" + loginId);
            }
        } else {
            SESSIONS.remove(loginId);
            RedisHelpUtils.getRedisUtil().remove(RedisEunm.USER_ONLINE.getKeyPrefix() + loginId);
            session.getChannel().close();
        }
    }

    /**
     * 通过channel 删除session
     *
     * @param ch
     */
    public static void remove(Channel ch) {
        for (Map.Entry<String, Session> entry : SESSIONS.entrySet()) {
            if (entry.getValue().getChannel().id().asLongText().equals(ch.id().asLongText())) {
                remove(entry.getKey());
                ch.close();
                break;
            }
        }
    }

    public static void removeAll() {
        Map<String, Session> sessions = getSessions();
        sessions.forEach((k, v) -> {
            remove(k);
        });
    }

    /**
     * 根据用户类型获取在线用户信息
     *
     * @return
     */
    public static Map<String, Session> getSessions() {
        List<String> keys = new ArrayList<>();
        RedisHelpUtils.scan(RedisEunm.USER_ONLINE.getKeyPrefix() + "*", key -> {
            keys.add(new String(key));
        });
        Map<String, Session> map = new HashMap<>();
        if (keys.size() > 0) {
            keys.forEach(x -> {
                Object obj = RedisHelpUtils.getRedisUtil().get(x);
                Session session = JSONObject.parseObject(obj.toString(), Session.class);
                map.put(session.getLoginId(), session);
            });
        }
        return map;
    }

    public static Session getSession(String loginId) {
        Object o = RedisHelpUtils.getRedisUtil().get(RedisEunm.USER_ONLINE.getKeyPrefix() + loginId);
        return o != null ? JSONObject.parseObject(o.toString(), Session.class) : null;
    }

    public static boolean isSession(String loginId) {
        Object o = RedisHelpUtils.getRedisUtil().get(RedisEunm.USER_ONLINE.getKeyPrefix() + loginId);
        return o != null;
    }

    /**
     * 可mq实现
     * @param receiveId
     * @param dataPacket
     */
    public static void write(String receiveId, DataPacket dataPacket) {
        if (Fc.isBlank(receiveId)) {
            return;
        }
        Session session = SESSIONS.get(receiveId);
        if (!Fc.isNull(session)) {
            session.getChannel().writeAndFlush(new TextWebSocketFrame(dataPacket.toString()));
        } else {
            Session redisSession = getSession(receiveId);
            if (Fc.notNull(redisSession)) {
                String url = "http://" + redisSession.getPosition() + SESSION_SEND + "?receiveId=" + receiveId;
                HttpClient.doPostJson(url, dataPacket.toString());
            }
        }
    }

    /**
     * 可mq实现
     * @param dataPacket
     */
    public static void write(DataPacket dataPacket) {
        if (Fc.isBlank(dataPacket.getReceiveId())) {
            return;
        }
        Session session = SESSIONS.get(dataPacket.getReceiveId());
        if (!Fc.isNull(session)) {
            session.getChannel().writeAndFlush(new TextWebSocketFrame(dataPacket.toString()));
        } else {
            Session redisSession = getSession(dataPacket.getReceiveId());
            if (Fc.notNull(redisSession)) {
                String url = "http://" + redisSession.getPosition() + SESSION_SEND + "?receiveId=" + dataPacket.getReceiveId();
                HttpClient.doPostJson(url, dataPacket.toString());
            }
        }
    }
    /**
     * 可mq实现
     * @param dataPacket
     */
    public static void writeAll(DataPacket dataPacket) {
        Map<String, Session> sessions = getSessions();
        if (sessions.size() > 0) {
            sessions.forEach((k, v) -> {
                Session session = SESSIONS.get(k);
                if (Fc.isNull(session)) {
                    Session redisSession = getSession(dataPacket.getReceiveId());
                    if (Fc.notNull(redisSession)) {
                        HttpClient.doPostJson("http://" + redisSession.getPosition() + SESSION_SEND + "?receiveId=" + k, dataPacket.toString());
                    }
                } else {
                    session.getChannel().writeAndFlush(new TextWebSocketFrame(dataPacket.toString()));
                }
            });
        }
    }

    public static Session byChannel(Channel channel) {
        for (Map.Entry<String, Session> session : SESSIONS.entrySet()) {
            if (session.getValue().getChannel().id().asLongText().equals(channel.id().asLongText())) {
                return session.getValue();
            }
        }
        return null;
    }

    public static void reUserTime(Channel channel) {
        RedisUtil redisUtil = RedisHelpUtils.getRedisUtil();
        Session session = byChannel(channel);
        redisUtil.set(RedisEunm.USER_ONLINE.getKeyPrefix() + session.getLoginId(), session.toString(), (long) properties.getIdleTimeAll(), TimeUnit.SECONDS);
    }
}
