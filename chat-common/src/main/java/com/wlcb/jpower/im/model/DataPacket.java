package com.wlcb.jpower.im.model;

import com.alibaba.fastjson.JSON;
import com.wlcb.jpower.constant.ChatConstant;
import com.wlcb.jpower.module.common.utils.Fc;
import com.wlcb.jpower.module.common.utils.constants.ConstantsEnum;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;

/**
 * 消息传递数据包
 */
public class DataPacket implements Serializable {
    private static final long serialVersionUID = 6603678508950955486L;

    @ApiModelProperty("会话ID")
    private String dialogId;
    /**
     * 消息包动作类型
     */
    @ApiModelProperty("消息包动作类型")
    private Integer action;
    /**
     * 消息类型
     */
    @ApiModelProperty("消息类型")
    private String type;
    /**
     * 应用id
     */
    @ApiModelProperty("应用id")
    private String appId;
    /**
     * 发送者类型
     */
    @ApiModelProperty("发送者类型")
    private Integer sendType;
    /**
     * 发送用户id
     */
    @ApiModelProperty("发送用户id")
    private String sendId;
    /**
     * 接收用户id
     */
    @ApiModelProperty("接收用户id")
    private String receiveId;
    /**
     * 接收者类型
     */
    @ApiModelProperty("接收者类型")
    private Integer receiveType;
    /**
     * 聊天内容
     */
    @ApiModelProperty("聊天内容")
    private String content;
    /**
     * 聊天时间
     */
    @ApiModelProperty("聊天时间")
    private Long date;

    private List<Wait> waits;

    public String getDialogId() {
        return dialogId;
    }

    public void setDialogId(String dialogId) {
        this.dialogId = dialogId;
    }

    public Integer getAction() {
        return action;
    }

    public void setAction(Integer action) {
        this.action = action;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public Integer getSendType() {
        return sendType;
    }

    public void setSendType(Integer sendType) {
        this.sendType = sendType;
    }

    public String getSendId() {
        return sendId;
    }

    public void setSendId(String sendId) {
        this.sendId = sendId;
    }

    public String getReceiveId() {
        return receiveId;
    }

    public void setReceiveId(String receiveId) {
        this.receiveId = receiveId;
    }

    public Integer getReceiveType() {
        return receiveType;
    }

    public void setReceiveType(Integer receiveType) {
        this.receiveType = receiveType;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public List<Wait> getWaits() {
        return waits;
    }

    public void setWaits(List<Wait> waits) {
        this.waits = waits;
    }

    @Override
    public String toString() {
        return JSON.toJSONString(DataPacket.this);
    }

    public static class Wait {
        @ApiModelProperty("openid")
        private String openid;
        @ApiModelProperty("appId")
        private String appId;
        @ApiModelProperty("内容")
        private String content;
        @ApiModelProperty("会话ID")
        private String dialogId;
        @ApiModelProperty("是否特殊人员")
        private Boolean isSpecial;

        public Boolean getSpecial() {
            return isSpecial;
        }

        public void setSpecial(Boolean special) {
            isSpecial = special;
        }

        public String getOpenid() {
            return openid;
        }

        public void setOpenid(String openid) {
            this.openid = openid;
        }

        public String getAppId() {
            return appId;
        }

        public void setAppId(String appId) {
            this.appId = appId;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getDialogId() {
            return dialogId;
        }

        public void setDialogId(String dialogId) {
            this.dialogId = dialogId;
        }

        @Override
        public String toString() {
            return JSON.toJSONString(Wait.this);
        }
    }

    /**
     * 服务端返回消息
     *
     * @param dialogId  会话id
     * @param receiveId 目标id
     * @param appId     公众号id
     * @param msg       消息内容
     * @return
     */
    public static DataPacket system(String dialogId, String receiveId, String appId, String msg) {
        DataPacket dataPacket = new DataPacket();
        dataPacket.setAppId(appId);
        dataPacket.setDialogId(dialogId);
        dataPacket.setAction(ChatConstant.ChatActionEnum.SYSTEM_SEND.type);
        dataPacket.setSendId(ConstantsEnum.USER_TYPE.USER_TYPE_SYSTEM.getName());
        dataPacket.setReceiveId(receiveId);
        dataPacket.setSendType(ConstantsEnum.USER_TYPE.USER_TYPE_SYSTEM.getValue());
        dataPacket.setReceiveType(ConstantsEnum.USER_TYPE.USER_TYPE_CUSTOMER.getValue());
        dataPacket.setDate(System.currentTimeMillis());
        dataPacket.setType(ChatConstant.MessageTypeEnum.TEXT.type);
        dataPacket.setContent(msg);
        return dataPacket;
    }

    /**
     * 服务端返回消息
     *
     * @param dialogId  会话id
     * @param receiveId 目标id
     * @param appId     公众号id
     * @param msg       消息内容
     * @param action    动作类型
     * @return
     */
    public static DataPacket system(String dialogId, String receiveId, String appId, String msg, Integer action) {
        DataPacket dataPacket = new DataPacket();
        dataPacket.setAppId(appId);
        dataPacket.setDialogId(dialogId);
        dataPacket.setAction(action);
        dataPacket.setSendId(ConstantsEnum.USER_TYPE.USER_TYPE_SYSTEM.getName());
        dataPacket.setReceiveId(receiveId);
        dataPacket.setSendType(ConstantsEnum.USER_TYPE.USER_TYPE_SYSTEM.getValue());
        dataPacket.setReceiveType(ConstantsEnum.USER_TYPE.USER_TYPE_CUSTOMER.getValue());
        dataPacket.setDate(System.currentTimeMillis());
        dataPacket.setType(ChatConstant.MessageTypeEnum.TEXT.type);
        dataPacket.setContent(msg);
        return dataPacket;
    }

    /**
     * 服务的推送 等待用户消息
     *
     * @param waits 等待列表
     * @return
     */
    public static DataPacket wait(List<Wait> waits, Integer receiveType) {
        DataPacket dataPacket = new DataPacket();
        dataPacket.setAction(ChatConstant.ChatActionEnum.WAIT_USER.type);
        dataPacket.setSendId(ConstantsEnum.USER_TYPE.USER_TYPE_SYSTEM.getName());
        dataPacket.setSendType(ConstantsEnum.USER_TYPE.USER_TYPE_SYSTEM.getValue());
        dataPacket.setReceiveType(receiveType);
        dataPacket.setDate(System.currentTimeMillis());
        dataPacket.setWaits(waits);
        return dataPacket;
    }


    public static String checkData(DataPacket dataPacket) {
        if (Fc.isBlank(dataPacket.getDialogId())) {
            return "dialogId";
        }
        if (Fc.isBlank(dataPacket.getSendId())) {
            return "sendId";
        }
        if (Fc.isBlank(dataPacket.getReceiveId())) {
            return "receiveId";
        }
        if (Fc.isNull(dataPacket.getSendType())) {
            return "sendType";
        }
        if (Fc.isNull(dataPacket.getReceiveType())) {
            return "receiveType";
        }
        if (Fc.isNull(dataPacket.getDate())) {
            return "date";
        }
        if (Fc.isBlank(dataPacket.getAppId())) {
            return "appId";
        }
        if (Fc.isBlank(dataPacket.getContent())) {
            return "content";
        }
        return "";
    }
}
