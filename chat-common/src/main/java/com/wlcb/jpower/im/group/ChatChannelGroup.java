package com.wlcb.jpower.im.group;

import com.wlcb.jpower.im.model.DataPacket;
import io.netty.channel.Channel;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import io.netty.util.concurrent.GlobalEventExecutor;

public class ChatChannelGroup {

    public static final ChannelGroup CHANNELGROUP = new DefaultChannelGroup("CHANNELGROUP", GlobalEventExecutor.INSTANCE);

    public static void add(Channel ch) {
        CHANNELGROUP.add(ch);
    }

    public static void remove(Channel ch) {
        CHANNELGROUP.remove(ch);
    }

    public static void broadcastTo(DataPacket dataPacket) {
        CHANNELGROUP.writeAndFlush(new TextWebSocketFrame(dataPacket.toString()));
    }

    public static void remove() {
        CHANNELGROUP.disconnect();
    }
}
