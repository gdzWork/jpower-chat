package com.wlcb.jpower.chat.reply.handler.fixed;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.wlcb.jpower.chat.entity.customer.TblCustomerMsg;
import com.wlcb.jpower.chat.reply.ReplyHandler;
import com.wlcb.jpower.chat.reply.ReplyEntity;
import com.wlcb.jpower.chat.service.customer.CustomerMsgService;
import com.wlcb.jpower.im.cache.ImChatRecordCache;
import com.wlcb.jpower.im.model.DataPacket;
import com.wlcb.jpower.module.common.utils.SpringUtil;
import lombok.extern.slf4j.Slf4j;
import me.chanjar.weixin.mp.bean.message.WxMpXmlMessage;

/**
 * 保存评价消息
 */
@Slf4j
public class SatisfactionHandler extends ReplyHandler {

    @Override
    public ReplyEntity handler(DataPacket dataPacket, WxMpXmlMessage wxMessage) {
        log.info("接收到评价消息 msgid：{},内容：{}", wxMessage.getBizMsgMenuId(), wxMessage.getContent());
        CustomerMsgService msgService = SpringUtil.getBean(CustomerMsgService.class);
        msgService.update(new UpdateWrapper<TblCustomerMsg>().lambda()
                .set(TblCustomerMsg::getScore, wxMessage.getContent())
                .eq(TblCustomerMsg::getMsgId, wxMessage.getBizMsgMenuId()));
        //评价消息不回复
        return ReplyEntity.singleTextMessage("", false);
    }

    @Override
    public ReplyEntity handler(DataPacket dataPacket, WxMpXmlMessage wxMessage, ImChatRecordCache cache) {
        return null;
    }
}
