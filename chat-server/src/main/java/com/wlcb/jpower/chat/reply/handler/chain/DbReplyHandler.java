package com.wlcb.jpower.chat.reply.handler.chain;

import com.alibaba.fastjson.JSONObject;
import com.wlcb.jpower.chat.entity.reply.TbDateReply;
import com.wlcb.jpower.chat.reply.ReplyHandler;
import com.wlcb.jpower.chat.reply.ReplyEntity;
import com.wlcb.jpower.chat.service.reply.DateReplyService;
import com.wlcb.jpower.im.cache.ImChatRecordCache;
import com.wlcb.jpower.im.model.DataPacket;
import com.wlcb.jpower.module.common.utils.Fc;
import com.wlcb.jpower.module.common.utils.SpringUtil;
import com.wlcb.jpower.properties.JpowerChatProperties;
import lombok.extern.slf4j.Slf4j;
import me.chanjar.weixin.mp.bean.message.WxMpXmlMessage;

import java.util.List;

/**
 * @description: 时间段回复
 * @author: liwenfei
 * @time: 2021/2/4 17:09
 */
@Slf4j
public class DbReplyHandler extends ReplyHandler {

    private static DateReplyService replyService;
    private static JpowerChatProperties properties;

    static {
        replyService = SpringUtil.getBean(DateReplyService.class);
        properties = SpringUtil.getBean(JpowerChatProperties.class);
    }

    @Override
    public ReplyEntity handler(DataPacket dataPacket, WxMpXmlMessage wxMessage) {
        if (!properties.getIsDbFixed()) {
            return this.getNextHandler().handler(dataPacket, wxMessage);
        }

        if (properties.getIsDbFixedAppid() != null && properties.getIsDbFixedAppid().length > 0 && !Fc.contains(properties.getIsDbFixedAppid(), dataPacket.getAppId())) {
            return this.getNextHandler().handler(dataPacket, wxMessage);
        }

        log.debug("进入固定回复");
        try {
            TbDateReply dateReply = replyService.isDate(dataPacket.getAppId());
            if (Fc.isNull(dateReply)) {
                TbDateReply tbDateReply = replyService.findByAsk(dataPacket.getContent(), dataPacket.getAppId());
                if (Fc.notNull(tbDateReply)) {
                    ReplyEntity replyEntity = new ReplyEntity().kefu();
                    replyEntity.setAppId(dataPacket.getAppId());
                    replyEntity.setToUser(wxMessage.getFromUser());
                    String replyBodys = tbDateReply.getReplyBodys();
                    List<ReplyEntity.ReplyBody> bodies = JSONObject.parseArray(replyBodys, ReplyEntity.ReplyBody.class);
                    replyEntity.setReplyBodys(bodies);
                    return replyEntity;
                }
                return this.getNextHandler().handler(dataPacket, wxMessage);
            }
            ReplyEntity replyEntity = new ReplyEntity().kefu();
            replyEntity.setAppId(dataPacket.getAppId());
            replyEntity.setToUser(wxMessage.getFromUser());
            String replyBodys = dateReply.getReplyBodys();
            List<ReplyEntity.ReplyBody> bodies = JSONObject.parseArray(replyBodys, ReplyEntity.ReplyBody.class);
            replyEntity.setReplyBodys(bodies);
            return replyEntity;
        } catch (Exception e) {
            return new ExceptionHandler(dataPacket.getAppId(), "固定回复", e).handler(dataPacket, wxMessage);
        }
    }

    @Override
    public ReplyEntity handler(DataPacket dataPacket, WxMpXmlMessage wxMessage, ImChatRecordCache cache) {
        return null;
    }
}
