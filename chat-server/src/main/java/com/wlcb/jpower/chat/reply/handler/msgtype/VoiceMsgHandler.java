package com.wlcb.jpower.chat.reply.handler.msgtype;

import com.wlcb.jpower.chat.reply.ReplyHandler;
import com.wlcb.jpower.chat.reply.ReplyEntity;
import com.wlcb.jpower.constant.ChatConstant;
import com.wlcb.jpower.im.cache.ImChatRecordCache;
import com.wlcb.jpower.im.cache.SessionCache;
import com.wlcb.jpower.im.model.DataPacket;
import com.wlcb.jpower.module.common.utils.BeanUtil;
import com.wlcb.jpower.module.common.utils.Fc;
import lombok.extern.slf4j.Slf4j;
import me.chanjar.weixin.mp.bean.message.WxMpXmlMessage;

/**
 * @date: 2021/5/11 8:43
 * @author: fei
 * @desc:
 */
@Slf4j
public class VoiceMsgHandler extends ReplyHandler {


    @Override
    public ReplyEntity handler(DataPacket dataPacket, WxMpXmlMessage wxMessage) {
        return null;
    }

    @Override
    public ReplyEntity handler(DataPacket dataPacket, WxMpXmlMessage wxMessage, ImChatRecordCache cache) {
        String recognition = wxMessage.getRecognition();
        DataPacket copy = BeanUtil.copyProperties(dataPacket, DataPacket.class);
        copy.setContent(recognition);
        copy.setType(ChatConstant.MessageTypeEnum.TEXT.type);
        copy.setAction(ChatConstant.ChatActionEnum.CUSTOMER_VOICE.type);
        if (Fc.isNotBlank(cache.getCid()) && SessionCache.isSession(cache.getCid())) {
            SessionCache.write(cache.getCid(), copy);
        }
        cache.setRecords(copy);
        return null;
    }
}
