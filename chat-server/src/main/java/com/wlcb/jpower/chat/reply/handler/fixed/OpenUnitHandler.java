package com.wlcb.jpower.chat.reply.handler.fixed;

import com.wlcb.jpower.chat.reply.ReplyEntity;
import com.wlcb.jpower.chat.reply.ReplyHandler;
import com.wlcb.jpower.chat.service.customer.ChatService;
import com.wlcb.jpower.chat.service.customer.SpecialUserService;
import com.wlcb.jpower.constant.RedisEunm;
import com.wlcb.jpower.im.cache.ImChatRecordCache;
import com.wlcb.jpower.im.model.DataPacket;
import com.wlcb.jpower.module.common.utils.ReturnJsonUtil;
import com.wlcb.jpower.module.common.utils.SpringUtil;
import com.wlcb.jpower.utils.RedisHelpUtils;
import lombok.extern.slf4j.Slf4j;
import me.chanjar.weixin.mp.bean.message.WxMpXmlMessage;

/**
 * @author: fei
 * @desc: 开启unit
 */
@Slf4j
public class OpenUnitHandler extends ReplyHandler {


    @Override
    public ReplyEntity handler(DataPacket dataPacket, WxMpXmlMessage wxMessage) {
        log.info("进入unit回复");
        SpecialUserService specialUserService = SpringUtil.getBean(SpecialUserService.class);
        if (specialUserService.findByOpenidAndAppid(dataPacket.getSendId(), dataPacket.getAppId()) == null) {
            log.warn("开启unit失败，请先添加您为该公众号的特殊人员openid={},appid={}",dataPacket.getSendId(),dataPacket.getAppId());
            return ReplyEntity.singleTextMessage("", false);
        }
        SpringUtil.getBean(ChatService.class).forcedEnd(dataPacket.getSendId());
        RedisHelpUtils.getRedisUtil().set(RedisEunm.BAIDU_UNIT_SESSION.getKeyPrefix() + wxMessage.getFromUser(), "",
                RedisEunm.BAIDU_UNIT_SESSION.getTime(), RedisEunm.BAIDU_UNIT_SESSION.timeUnit);
        ReplyEntity kefu = ReplyEntity.singleTextMessage("开启unit成功", true).kefu();
        kefu.setToUser(wxMessage.getFromUser());
        kefu.setAppId(dataPacket.getAppId());
        return kefu;
    }

    @Override
    public ReplyEntity handler(DataPacket dataPacket, WxMpXmlMessage wxMessage, ImChatRecordCache cache) {
        return null;
    }
}
