package com.wlcb.jpower.chat.reply.handler.chain;

import com.wlcb.jpower.chat.reply.ReplyEntity;
import com.wlcb.jpower.chat.reply.ReplyHandler;
import com.wlcb.jpower.im.cache.ImChatRecordCache;
import com.wlcb.jpower.im.model.DataPacket;
import lombok.extern.slf4j.Slf4j;
import me.chanjar.weixin.mp.bean.message.WxMpXmlMessage;

/**
 * @author: liwenfei
 * @version: v1.0
 * @time: 2021/4/30 15:36
 * @desc:
 */
@Slf4j
public class StartHandler extends ReplyHandler {


    @Override
    public ReplyEntity handler(DataPacket dataPacket, WxMpXmlMessage wxMessage) {
        log.debug("消息处理首节点");
        return this.getNextHandler().handler(dataPacket, wxMessage);
    }

    @Override
    public ReplyEntity handler(DataPacket dataPacket, WxMpXmlMessage wxMessage, ImChatRecordCache cache) {
        return null;
    }
}
