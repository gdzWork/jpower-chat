package com.wlcb.jpower.chat.server.handler;

import com.wlcb.jpower.im.model.DataPacket;
import io.netty.channel.ChannelHandlerContext;

public abstract class MessageHandler {


    public abstract void handler(ChannelHandlerContext ctx, DataPacket dataPacket);


}
