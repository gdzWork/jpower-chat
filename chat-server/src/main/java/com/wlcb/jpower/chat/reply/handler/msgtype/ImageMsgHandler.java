package com.wlcb.jpower.chat.reply.handler.msgtype;

import com.wlcb.jpower.chat.reply.ReplyHandler;
import com.wlcb.jpower.chat.service.BaiduIdentifyService;
import com.wlcb.jpower.chat.reply.ReplyEntity;
import com.wlcb.jpower.constant.ChatConstant;
import com.wlcb.jpower.im.cache.ImChatRecordCache;
import com.wlcb.jpower.im.cache.SessionCache;
import com.wlcb.jpower.im.model.DataPacket;
import com.wlcb.jpower.module.common.utils.BeanUtil;
import com.wlcb.jpower.module.common.utils.Fc;
import com.wlcb.jpower.module.common.utils.SpringUtil;
import com.wlcb.jpower.properties.JpowerChatBaiduIdentifyProperties;
import lombok.extern.slf4j.Slf4j;
import me.chanjar.weixin.mp.bean.message.WxMpXmlMessage;

/**
 * @date: 2021/5/11 8:42
 * @author: fei
 * @desc:
 */
@Slf4j
public class ImageMsgHandler extends ReplyHandler {

    private static JpowerChatBaiduIdentifyProperties properties;

    static {
        properties = SpringUtil.getBean(JpowerChatBaiduIdentifyProperties.class);
    }

    @Override
    public ReplyEntity handler(DataPacket dataPacket, WxMpXmlMessage wxMessage) {
        return null;
    }

    @Override
    public ReplyEntity handler(DataPacket dataPacket, WxMpXmlMessage wxMessage, ImChatRecordCache cache) {
        BaiduIdentifyService baiduIdentifyService = SpringUtil.getBean(BaiduIdentifyService.class);
        if (properties.getOpen()) {
            String url = properties.getUrl();
            String result = baiduIdentifyService.imageIdentify(url + dataPacket.getContent());
            DataPacket copy = BeanUtil.copyProperties(dataPacket, DataPacket.class);
            copy.setContent(result);
            copy.setType(ChatConstant.MessageTypeEnum.TEXT.type);
            copy.setAction(ChatConstant.ChatActionEnum.CUSTOMER_USER_MESSAGE_SHIBIE.type);
            if (Fc.isNotBlank(cache.getCid()) && SessionCache.isSession(cache.getCid())) {
                SessionCache.write(cache.getCid(), copy);
            }
            cache.setRecords(copy);
        }
        return null;
    }
}
