package com.wlcb.jpower.chat;

import com.wlcb.jpower.chat.server.ChatServerApp;
import com.wlcb.jpower.chat.server.JpowerChatRegister;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import javax.annotation.PreDestroy;

/**
 * @author mr.g
 * @date 2022-03-06 20:50
 */
@Component
@RequiredArgsConstructor
public class ChatServerApplication implements ApplicationRunner {

    private final ChatServerApp chatServerApp;
    private final JpowerChatRegister jpowerChatInstance;

    @Override
    public void run(ApplicationArguments args) {
        jpowerChatInstance.register();
        chatServerApp.startApp();
    }

    @PreDestroy
    public void destory(){
        chatServerApp.stop();
        jpowerChatInstance.stop();
    }
}
