package com.wlcb.jpower.chat.reply.handler.msgtype;

import com.alibaba.fastjson.JSONObject;
import com.wlcb.jpower.chat.reply.ReplyHandler;
import com.wlcb.jpower.chat.service.BaiduUnitService;
import com.wlcb.jpower.chat.service.customer.CustomerSettingService;
import com.wlcb.jpower.chat.reply.ReplyEntity;
import com.wlcb.jpower.constant.ChatConfigConstant;
import com.wlcb.jpower.constant.ChatConstant;
import com.wlcb.jpower.im.cache.ImChatRecordCache;
import com.wlcb.jpower.im.cache.SessionCache;
import com.wlcb.jpower.im.model.DataPacket;
import com.wlcb.jpower.module.common.utils.BeanUtil;
import com.wlcb.jpower.module.common.utils.Fc;
import com.wlcb.jpower.module.common.utils.SpringUtil;
import com.wlcb.jpower.properties.JpowerChatProperties;
import lombok.extern.slf4j.Slf4j;
import me.chanjar.weixin.mp.bean.message.WxMpXmlMessage;

import java.util.List;

/**
 * @date: 2021/5/11 8:42
 * @author: fei
 * @desc:
 */
@Slf4j
public class TextMsgHandler extends ReplyHandler {

    private static JpowerChatProperties properties;
    private static BaiduUnitService baiduUnitService;
    private static CustomerSettingService customerSettingService;

    static {
        customerSettingService = SpringUtil.getBean(CustomerSettingService.class);
        properties = SpringUtil.getBean(JpowerChatProperties.class);
        baiduUnitService = SpringUtil.getBean(BaiduUnitService.class);
    }

    @Override
    public ReplyEntity handler(DataPacket dataPacket, WxMpXmlMessage wxMessage) {
        return null;
    }

    @Override
    public ReplyEntity handler(DataPacket dataPacket, WxMpXmlMessage wxMessage, ImChatRecordCache cache) {
        try {
            if (!properties.getAutoUnitReply()) {
                return null;
            }
            if (properties.getAutoUnitReplyAppid() != null && properties.getAutoUnitReplyAppid().length > 0 && Fc.contains(properties.getAutoUnitReplyAppid(), dataPacket.getAppId())) {
                return null;
            }
            DataPacket copy = BeanUtil.copyProperties(dataPacket, DataPacket.class);
            copy.setContent(wxMessage.getContent());
            copy.setType(ChatConstant.MessageTypeEnum.TEXT.type);
            copy.setAction(ChatConstant.ChatActionEnum.CUSTOMER_VOICE.type);
            if (Fc.isNotBlank(cache.getCid()) && SessionCache.isSession(cache.getCid())) {
                SessionCache.write(cache.getCid(), copy);
            }
            cache.setRecords(copy);
            if (Fc.isNotBlank(cache.getCid())) {
                String answer = baiduUnitService.say(dataPacket.getContent(), dataPacket.getSendId());
                if (Fc.isNotBlank(answer)) {
                    Boolean reply = customerSettingService.findCidAutoReply(cache.getCid());
                    List<ReplyEntity.ReplyBody> bodyList = JSONObject.parseArray(answer, ReplyEntity.ReplyBody.class);
                    if (reply) {
                        ReplyEntity entity = new ReplyEntity().kefu();
                        entity.setToUser(dataPacket.getSendId());
                        entity.setAppId(dataPacket.getAppId());
                        entity.setReplyBodys(bodyList);
                        entity.sendReply();
                        for (ReplyEntity.ReplyBody replyBody : bodyList) {
                            DataPacket newData = toDataPacket(dataPacket, replyBody,
                                    ChatConstant.ChatActionEnum.CUSTOMER_USER_MESSAGE.type,
                                    ChatConfigConstant.UserType.CUSTOMER,
                                    ChatConfigConstant.UserType.OTHER);
                            if (Fc.isNotBlank(cache.getCid()) && SessionCache.isSession(cache.getCid())) {
                                SessionCache.write(cache.getCid(), newData);
                            }
                        }
                    } else {
                        for (ReplyEntity.ReplyBody replyBody : bodyList) {
                            DataPacket newData = toDataPacket(dataPacket, replyBody,
                                    ChatConstant.ChatActionEnum.CUSTOMER_UNIT.type,
                                    ChatConfigConstant.UserType.CUSTOMER,
                                    ChatConfigConstant.UserType.OTHER);
                            if (Fc.isNotBlank(cache.getCid()) && SessionCache.isSession(cache.getCid())) {
                                SessionCache.write(cache.getCid(), newData);
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private DataPacket toDataPacket(DataPacket dataPacket, ReplyEntity.ReplyBody replyBody, Integer action, Integer sendType, Integer receiveType) {
        DataPacket newDataPacket = new DataPacket();
        newDataPacket.setDialogId(dataPacket.getDialogId());
        newDataPacket.setAction(action);
        newDataPacket.setType(replyBody.getType());
        newDataPacket.setAppId(dataPacket.getDialogId());
        newDataPacket.setSendType(sendType);
        newDataPacket.setSendId(dataPacket.getSendId());
        newDataPacket.setReceiveId(dataPacket.getReceiveId());
        newDataPacket.setReceiveType(receiveType);
        newDataPacket.setContent(ChatConstant.MessageTypeEnum.IMAGE.type.equals(replyBody.getType()) ? replyBody.getImageId() : replyBody.getContent());
        newDataPacket.setDate(System.currentTimeMillis());
        return newDataPacket;
    }
}
