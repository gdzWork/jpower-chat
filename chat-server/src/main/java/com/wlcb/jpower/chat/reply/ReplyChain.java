package com.wlcb.jpower.chat.reply;

import com.wlcb.jpower.chat.reply.handler.chain.*;

/**
 * @author: liwenfei
 * @version: v1.0
 * @time: 2021/4/30 14:23
 * @desc: 回复链
 */
public class ReplyChain {
    private static ReplyHandler startHandler = new StartHandler();
    private static ReplyHandler fixedReplyHandler = new DbReplyHandler();
    private static ReplyHandler unitReplyHandler = new UnitReplyHandler();
    private static ReplyHandler otherHandler = new OtherReplyHandler();
    private static ReplyHandler customerReplyHandler = new CustomerReplyHandler();
    private static ReplyHandler endHandler = new EndHandler();

    static {
        startHandler.setNextHandler(unitReplyHandler);
        unitReplyHandler.setNextHandler(fixedReplyHandler);
        fixedReplyHandler.setNextHandler(otherHandler);
        otherHandler.setNextHandler(customerReplyHandler);
        customerReplyHandler.setNextHandler(endHandler);
    }

    private ReplyChain() {
    }

    public static ReplyHandler getStart() {
        return startHandler;
    }
}
