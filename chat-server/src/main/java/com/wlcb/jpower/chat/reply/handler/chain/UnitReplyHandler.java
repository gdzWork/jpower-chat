package com.wlcb.jpower.chat.reply.handler.chain;

import com.alibaba.fastjson.JSONObject;
import com.wlcb.jpower.chat.reply.ReplyHandler;
import com.wlcb.jpower.chat.reply.ReplyEntity;
import com.wlcb.jpower.chat.service.BaiduUnitService;
import com.wlcb.jpower.constant.ChatConstant;
import com.wlcb.jpower.constant.RedisEunm;
import com.wlcb.jpower.im.cache.ImChatRecordCache;
import com.wlcb.jpower.im.model.DataPacket;
import com.wlcb.jpower.module.common.redis.RedisUtil;
import com.wlcb.jpower.module.common.utils.Fc;
import com.wlcb.jpower.module.common.utils.SpringUtil;
import com.wlcb.jpower.properties.JpowerChatBaiduUnitProperties;
import com.wlcb.jpower.utils.RedisHelpUtils;
import lombok.extern.slf4j.Slf4j;
import me.chanjar.weixin.mp.bean.message.WxMpXmlMessage;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @author: liwenfei
 * @version: v1.0
 * @time: 2021/4/15 9:50
 * @desc:
 */
@Slf4j
public class UnitReplyHandler extends ReplyHandler {
    private static BaiduUnitService baiduUnitService;
    private static JpowerChatBaiduUnitProperties unitProperties;

    static {
        baiduUnitService = SpringUtil.getBean(BaiduUnitService.class);
        unitProperties = SpringUtil.getBean(JpowerChatBaiduUnitProperties.class);
    }

    @Override
    public ReplyEntity handler(DataPacket dataPacket, WxMpXmlMessage wxMessage) {
        if (!unitProperties.getOpen()) {
            return this.getNextHandler().handler(dataPacket, wxMessage);
        }
        log.debug("unit回复");
        try {
            RedisUtil redisUtil = RedisHelpUtils.getRedisUtil();
            Object object = redisUtil.get(RedisEunm.BAIDU_UNIT_SESSION.getKeyPrefix() + wxMessage.getFromUser());
            String answer = "";
            if (Fc.notNull(object)) {
                if (ChatConstant.MessageTypeEnum.TEXT.getType().equals(dataPacket.getType())) {

                    if (unitProperties.isReplyOne()) {

                        Object say = redisUtil.get(RedisEunm.BAIDU_UNIT_SAY.keyPrefix + dataPacket.getSendId());
                        answer = baiduUnitService.say("1", dataPacket.getSendId());
                        if (answer.contains("我不知道该怎么答复您。")) {
                            TimeUnit.MILLISECONDS.sleep(700);
                            answer = baiduUnitService.say(dataPacket.getContent(), dataPacket.getSendId());
                        }
                        if (Fc.notNull(say)) {
                            if (say.toString().equals(answer)) {
                                TimeUnit.MILLISECONDS.sleep(700);
                                answer = baiduUnitService.say(dataPacket.getContent(), dataPacket.getSendId());
                            }
                        }

                    }else {

                        answer = baiduUnitService.say(dataPacket.getContent(), dataPacket.getSendId());

                    }

                } else if (ChatConstant.MessageTypeEnum.VOICE.getType().equals(dataPacket.getType())) {

                    if (unitProperties.isReplyOne()) {
                        Object say = redisUtil.get(RedisEunm.BAIDU_UNIT_SAY.keyPrefix + dataPacket.getSendId());
                        answer = baiduUnitService.say("1", dataPacket.getSendId());
                        if (answer.contains("我不知道该怎么答复您。")) {
                            TimeUnit.MILLISECONDS.sleep(700);
                            answer = baiduUnitService.say(wxMessage.getRecognition(), dataPacket.getSendId());
                        }
                        if (Fc.notNull(say)) {
                            if (say.toString().equals(answer)) {
                                TimeUnit.MILLISECONDS.sleep(700);
                                answer = baiduUnitService.say(wxMessage.getRecognition(), dataPacket.getSendId());
                            }
                        }
                    }else {

                        answer = baiduUnitService.say(wxMessage.getRecognition(), dataPacket.getSendId());
                    }

                }
            }
            if (Fc.isNotBlank(answer)) {
                redisUtil.set(RedisEunm.BAIDU_UNIT_SAY.keyPrefix + dataPacket.getSendId(), answer, RedisEunm.BAIDU_UNIT_SAY.time, RedisEunm.BAIDU_UNIT_SAY.getTimeUnit());
                List<ReplyEntity.ReplyBody> bodyList = JSONObject.parseArray(answer, ReplyEntity.ReplyBody.class);
                ReplyEntity replyEntity = new ReplyEntity();
                replyEntity.setToUser(wxMessage.getFromUser());
                replyEntity.setAppId(dataPacket.getAppId());
                replyEntity.setReplyBodys(bodyList);
                return replyEntity;
            }
            return this.getNextHandler().handler(dataPacket, wxMessage);
        } catch (Exception e) {
            return new ExceptionHandler(dataPacket.getAppId(), "百度unit回复", e).handler(dataPacket, wxMessage);
        }
    }

    @Override
    public ReplyEntity handler(DataPacket dataPacket, WxMpXmlMessage wxMessage, ImChatRecordCache cache) {
        return null;
    }
}
