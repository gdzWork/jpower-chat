package com.wlcb.jpower.chat.reply.handler.chain;

import com.wlcb.jpower.chat.reply.ReplyEntity;
import com.wlcb.jpower.chat.reply.ReplyHandler;
import com.wlcb.jpower.im.cache.ImChatRecordCache;
import com.wlcb.jpower.im.model.DataPacket;
import lombok.extern.slf4j.Slf4j;
import me.chanjar.weixin.mp.bean.message.WxMpXmlMessage;

/**
 * @author: liwenfei
 * @version: v1.0
 * @time: 2021/4/30 15:42
 * @desc: 回复链异常处理
 */
@Slf4j
public class ExceptionHandler extends ReplyHandler {

    private String name;
    private Exception e;
    private String appId;


    public ExceptionHandler(String appId, String name, Exception e) {
        this.e = e;
        this.name = name;
        this.appId = appId;
    }

    @Override
    public ReplyEntity handler(DataPacket dataPacket, WxMpXmlMessage wxMessage) {
        e.printStackTrace();
        log.error("回复链异常：处理器名称：{} \n 异常信息:{} \n 接收的消息：{}", name, e, wxMessage);
        return ReplyEntity.singleTextMessage("公众号较忙，请稍后重试", true);
    }

    @Override
    public ReplyEntity handler(DataPacket dataPacket, WxMpXmlMessage wxMessage, ImChatRecordCache cache) {
        return null;
    }
}
