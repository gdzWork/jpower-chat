package com.wlcb.jpower.chat.server.handler.customer;

import com.wlcb.jpower.chat.server.handler.MessageHandler;
import com.wlcb.jpower.constant.ChatConstant;

import java.util.HashMap;
import java.util.Map;

public class CustomerMessage {
    private final static Map<Integer, MessageHandler> MAP = new HashMap<>();

    static {
        MAP.put(ChatConstant.ChatActionEnum.CUSTOMER_USER_MESSAGE.type, new CustomerUserHandler());
    }

    public static CustomerMessage getInstance() {
        return new CustomerMessage();
    }


    public MessageHandler getChatHandler(Integer action) {
        return MAP.get(action);
    }

}
