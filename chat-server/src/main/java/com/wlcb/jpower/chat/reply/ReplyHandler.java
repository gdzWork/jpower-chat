package com.wlcb.jpower.chat.reply;

import com.wlcb.jpower.im.cache.ImChatRecordCache;
import com.wlcb.jpower.im.model.DataPacket;
import me.chanjar.weixin.mp.bean.message.WxMpXmlMessage;


public abstract class ReplyHandler {

    /**
     * 下次处理的handler
     */
    private ReplyHandler nextHandler;


    public void setNextHandler(ReplyHandler nextHandler) {
        this.nextHandler = nextHandler;
    }

    public ReplyHandler getNextHandler() {
        return nextHandler;
    }

    /**
     * 未转发到客服时执行实现该方法
     */
    public abstract ReplyEntity handler(DataPacket dataPacket, WxMpXmlMessage wxMessage);
    /**
     * 转发到客服时执行实现该方法
     */
    public abstract ReplyEntity handler(DataPacket dataPacket, WxMpXmlMessage wxMessage, ImChatRecordCache cache);
}
