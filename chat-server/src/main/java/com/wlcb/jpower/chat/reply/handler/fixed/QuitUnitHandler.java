package com.wlcb.jpower.chat.reply.handler.fixed;

import com.wlcb.jpower.chat.reply.ReplyHandler;
import com.wlcb.jpower.chat.reply.ReplyEntity;
import com.wlcb.jpower.chat.service.customer.SpecialUserService;
import com.wlcb.jpower.constant.RedisEunm;
import com.wlcb.jpower.im.cache.ImChatRecordCache;
import com.wlcb.jpower.im.model.DataPacket;
import com.wlcb.jpower.module.common.utils.SpringUtil;
import com.wlcb.jpower.utils.RedisHelpUtils;
import lombok.extern.slf4j.Slf4j;
import me.chanjar.weixin.mp.bean.message.WxMpXmlMessage;

/**
 * @author: liwenfei
 * @version: v1.0
 * @time: 2021/3/31 16:27
 * @desc:
 */
@Slf4j
public class QuitUnitHandler extends ReplyHandler {
    @Override
    public ReplyEntity handler(DataPacket dataPacket, WxMpXmlMessage wxMessage) {
        log.info("进入unit退出回复");
        SpecialUserService specialUserService = SpringUtil.getBean(SpecialUserService.class);
        if (specialUserService.findByOpenidAndAppid(dataPacket.getSendId(), dataPacket.getAppId()) == null) {
            return ReplyEntity.singleTextMessage("", false);
        }

        RedisHelpUtils.getRedisUtil().remove(RedisEunm.BAIDU_UNIT_SAY.keyPrefix+dataPacket.getSendId());
        RedisHelpUtils.getRedisUtil().remove(RedisEunm.BAIDU_UNIT_SESSION.getKeyPrefix() + wxMessage.getFromUser());
        ReplyEntity kefu = ReplyEntity.singleTextMessage("退出成功", true).kefu();
        kefu.setToUser(wxMessage.getFromUser());
        kefu.setAppId(dataPacket.getAppId());
        return kefu;
    }

    @Override
    public ReplyEntity handler(DataPacket dataPacket, WxMpXmlMessage wxMessage, ImChatRecordCache cache) {
        return null;
    }
}
