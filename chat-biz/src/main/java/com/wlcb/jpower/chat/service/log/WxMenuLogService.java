package com.wlcb.jpower.chat.service.log;

import com.wlcb.jpower.chat.entity.log.TbWxMenuLog;
import com.wlcb.jpower.module.common.service.BaseService;

import java.util.List;

public interface WxMenuLogService extends BaseService<TbWxMenuLog> {

    void saveMenuLog(TbWxMenuLog log);


    List<TbWxMenuLog> findList(String appId, String createUser);
}
