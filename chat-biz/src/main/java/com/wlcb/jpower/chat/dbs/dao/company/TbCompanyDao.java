package com.wlcb.jpower.chat.dbs.dao.company;

import com.wlcb.jpower.chat.dbs.dao.company.mapper.TbCompanyMapper;
import com.wlcb.jpower.chat.entity.company.TbCompany;
import com.wlcb.jpower.module.dbs.dao.JpowerServiceImpl;
import org.springframework.stereotype.Repository;

/**
 * @description:
 * @author: liwenfei
 * @time: 2021/1/8 13:32
 */
@Repository
public class TbCompanyDao extends JpowerServiceImpl<TbCompanyMapper, TbCompany> {
}
