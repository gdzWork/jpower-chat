package com.wlcb.jpower.chat.service.customer;

import com.wlcb.jpower.chat.entity.customer.TbCustomerSettings;
import com.wlcb.jpower.module.common.service.BaseService;

import java.util.List;

/**
 * @description:
 * @author: liwenfei
 * @time: 2021/1/26 14:12
 */
public interface CustomerSettingService extends BaseService<TbCustomerSettings> {
    /**
     * 保存设置
     *
     * @param cid
     * @return
     */
    boolean save(String cid);

    /**
     * 查询客服设置
     *
     * @param cid
     * @return
     */
    TbCustomerSettings findByCid(String cid);

    /**
     * 修改设置
     *
     * @param settings
     * @return
     */
    boolean modify(TbCustomerSettings settings);

    /**
     * 查找所有自动接入的客服
     *
     * @return
     */
    List<String> findAutoAccess();

    /**
     * 查找客服接入时自动回复内容
     *
     * @param cid
     * @return
     */
    String findCidIsAutoReply(String cid);

    /**
     * 该客服是否允许转接
     *
     * @param cid
     * @return
     */
    boolean findCidAllowTransfer(String cid);

    /**
     * 是否开启自动回复
     *
     * @param cid
     * @return
     */
    Boolean findCidAutoReply(String cid);

}
