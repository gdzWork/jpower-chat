package com.wlcb.jpower.chat.service;

import com.wlcb.jpower.chat.dbs.dao.wxapp.TblWxAppDao;
import com.wlcb.jpower.chat.entity.wxapp.TblWxApp;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author: liwenfei
 * @version: v1.0
 * @time: 2021/4/22 12:27
 * @desc:
 */
@Service
public class TestService {

    @Autowired
    private TblWxAppDao wxAppDao;

    @GlobalTransactional
    public Boolean save(TblWxApp app) {
        wxAppDao.save(app);
        int a = 1 / 0;
        return true;
    }
}
