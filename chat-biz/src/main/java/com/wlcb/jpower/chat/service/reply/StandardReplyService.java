package com.wlcb.jpower.chat.service.reply;

import com.wlcb.jpower.chat.entity.reply.TbStandardReply;
import com.wlcb.jpower.module.common.service.BaseService;

import java.util.List;

public interface StandardReplyService extends BaseService<TbStandardReply> {

    List<TbStandardReply> findList(Integer status, String cid);
}
