package com.wlcb.jpower.chat.service.customer;

import com.wlcb.jpower.chat.entity.customer.TblCustomerMsg;
import com.wlcb.jpower.chat.entity.wxuser.WxUserInfo;
import com.wlcb.jpower.module.common.service.BaseService;

import java.util.Date;
import java.util.List;

public interface CustomerMsgService extends BaseService<TblCustomerMsg> {

    List<TblCustomerMsg> find(TblCustomerMsg msg, Date startTime, Date endTime);

    List<TblCustomerMsg> findList(String cid, Date startTime, Date endTime);

    boolean modifyById(TblCustomerMsg msg);

    List<WxUserInfo> findByCurrentCid(String currentCid);

    List<String> findRecept(Date startTime, Date endTime);

    long findByCid(String cid);

    String saveCustomerMsg(String openid, String appId);

    TblCustomerMsg findbyAppIdAndOpenid(String appId, String openid);
}
