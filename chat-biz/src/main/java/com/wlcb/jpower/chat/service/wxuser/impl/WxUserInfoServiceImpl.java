package com.wlcb.jpower.chat.service.wxuser.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.support.SFunction;
import com.wlcb.jpower.chat.dbs.dao.wxuser.WxUserInfoDao;
import com.wlcb.jpower.chat.dbs.dao.wxuser.mapper.WxUserInfoMapper;
import com.wlcb.jpower.chat.entity.wxuser.WxUserInfo;
import com.wlcb.jpower.chat.service.wxuser.WxUserInfoService;
import com.wlcb.jpower.module.common.service.impl.BaseServiceImpl;
import com.wlcb.jpower.module.common.utils.BeanUtil;
import com.wlcb.jpower.module.common.utils.Fc;
import com.wlcb.jpower.utils.WxMpInstance;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.result.WxMpUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @description:
 * @author: liwenfei
 * @time: 2020/11/30 14:04
 */
@Service("wxUserInfoServiceImpl")
public class WxUserInfoServiceImpl extends BaseServiceImpl<WxUserInfoMapper, WxUserInfo> implements WxUserInfoService {

    @Autowired
    private WxUserInfoDao wxUserInfoDao;

    @Override
    public boolean add(WxUserInfo wxUserInfo) {
        WxUserInfo one = wxUserInfoDao.getOne(new QueryWrapper<WxUserInfo>().lambda().eq(WxUserInfo::getOpenid, wxUserInfo.getOpenid()));
        if (Fc.isNull(one)) {
            return wxUserInfoDao.save(wxUserInfo);
        }
        return false;
    }

    @Override
    public WxUserInfo findByOpenidAndAppId(String openid, String appId) {
        return wxUserInfoDao.getOne(new QueryWrapper<WxUserInfo>().lambda().eq(WxUserInfo::getAppId, appId)
                .eq(WxUserInfo::getOpenid, openid));
    }

    @Async("threadPool")
    @Override
    public void collectUserInfo(String appId, String openid) throws WxErrorException {
        WxMpService wxMpService = WxMpInstance.getWxMp().switchoverTo(appId);
        WxMpUser wxMpUser = wxMpService.switchoverTo(appId).getUserService().userInfo(openid);
        //todo 非幂等性
        WxUserInfo wxUser = BeanUtil.copyProperties(wxMpUser, WxUserInfo.class);
        wxUser.setAppId(appId);
        wxUser.setOpenid(wxMpUser.getOpenId());
        wxUser.setSubscribe(wxMpUser.getSubscribe() ? 1 : 0);
        wxUser.setHeadimgurl(wxMpUser.getHeadImgUrl());
        wxUser.setSubscribeTime(wxMpUser.getSubscribeTime());
        wxUser.setUnionid(wxMpUser.getUnionId());
        wxUser.setRemark(wxMpUser.getRemark());
        wxUser.setGroupid(String.valueOf(wxMpUser.getGroupId()));
        wxUser.setTagidList(JSON.toJSONString(wxMpUser.getTagIds()));
        WxUserInfo one = findByOpenidAndAppId(wxMpUser.getOpenId(), appId);
        if (!Fc.isNull(one)) {
            wxUser.setId(one.getId());
            updateById(wxUser);
        } else {
            save(wxUser);
        }
    }

    @Override
    public List<WxUserInfo> findPageList(WxUserInfo wxUserInfo) {
        LambdaQueryWrapper<WxUserInfo> queryWrapper = new QueryWrapper<WxUserInfo>().lambda();
        queryWrapper.orderByDesc(WxUserInfo::getCreateTime);
        Map<SFunction<WxUserInfo, ?>, Object> map = new HashMap<>();
        map.put(WxUserInfo::getAppId, wxUserInfo.getAppId());
        map.put(WxUserInfo::getOpenid, wxUserInfo.getOpenid());
        map.put(WxUserInfo::getNickname, wxUserInfo.getNickname());
        map.put(WxUserInfo::getSex, wxUserInfo.getSex());
        map.put(WxUserInfo::getCity, wxUserInfo.getCity());
        map.put(WxUserInfo::getCountry, wxUserInfo.getCountry());
        map.put(WxUserInfo::getUnionid, wxUserInfo.getUnionid());

        queryWrapper.allEq(map, false);

        return wxUserInfoDao.list(queryWrapper);
    }
}
