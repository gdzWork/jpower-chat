package com.wlcb.jpower.chat.dbs.dao.wxuser.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wlcb.jpower.chat.entity.wxuser.WxUserInfo;
import com.wlcb.jpower.module.dbs.dao.mapper.base.JpowerBaseMapper;
import org.springframework.stereotype.Component;

/**
 * @description:
 * @author: liwenfei
 * @time: 2020/11/30 14:02
 */
@Component("wxUserInfoMapper")
public interface WxUserInfoMapper extends JpowerBaseMapper<WxUserInfo> {
}
