package com.wlcb.jpower.chat.service.wx.mp;

import com.wlcb.jpower.wx.mp.template.WxTemplateRequest;

/**
 * @author: liwenfei
 * @version: v1.0
 * @time: 2021/3/2 14:21
 * @desc:
 */
public interface TemplateMsgService {

    int templateSend(WxTemplateRequest template, boolean exSend);
}
