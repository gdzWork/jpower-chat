package com.wlcb.jpower.chat.service.wx.mp;

import me.chanjar.weixin.common.bean.menu.WxMenu;
import me.chanjar.weixin.mp.bean.menu.WxMpMenu;

/**
 * @author: liwenfei
 * @version: v1.0
 * @time: 2021/3/5 9:57
 * @desc:
 */
public interface WxMenuService {

    boolean createMenu(WxMenu wxMenu, String appId,boolean exSend);

    WxMpMenu findMenu(String appId);

    boolean deleteMenu(String appId);
}
