package com.wlcb.jpower.chat.dbs.dao.reply;

import com.wlcb.jpower.chat.dbs.dao.reply.mapper.TbDateReplyMapper;
import com.wlcb.jpower.chat.entity.reply.TbDateReply;
import com.wlcb.jpower.module.dbs.dao.JpowerServiceImpl;
import org.springframework.stereotype.Repository;

/**
 * @description:
 * @author: liwenfei
 * @time: 2021/2/4 17:01
 */
@Repository
public class TbDateReplyDao extends JpowerServiceImpl<TbDateReplyMapper, TbDateReply> {
}
