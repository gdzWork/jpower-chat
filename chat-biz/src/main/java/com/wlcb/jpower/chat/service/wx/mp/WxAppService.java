package com.wlcb.jpower.chat.service.wx.mp;


import com.wlcb.jpower.chat.entity.wxapp.TblWxApp;
import com.wlcb.jpower.module.common.service.BaseService;

import java.util.List;

public interface WxAppService extends BaseService<TblWxApp> {

    List<TblWxApp> findByStaus(Integer status);

    TblWxApp findOne(String appId);

    List<TblWxApp> findList(TblWxApp tblWxApp);
}
