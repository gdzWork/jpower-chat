package com.wlcb.jpower.chat.dbs.dao.demo;

import com.wlcb.jpower.chat.dbs.dao.demo.mapper.TbDemoRecordMapper;
import com.wlcb.jpower.chat.entity.demo.TbDemoRecord;
import com.wlcb.jpower.module.dbs.dao.JpowerServiceImpl;
import org.springframework.stereotype.Repository;

/**
 * @date: 2021/5/18 13:46
 * @author: fei
 * @desc:
 */
@Repository
public class TbDemoRecordDao extends JpowerServiceImpl<TbDemoRecordMapper, TbDemoRecord> {
}
