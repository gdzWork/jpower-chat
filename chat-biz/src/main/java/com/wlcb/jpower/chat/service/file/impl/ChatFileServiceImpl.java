package com.wlcb.jpower.chat.service.file.impl;

import com.wlcb.jpower.chat.dbs.dao.file.ChatFileDao;
import com.wlcb.jpower.chat.dbs.dao.file.mapper.ChatFileMapper;
import com.wlcb.jpower.chat.entity.file.TbChatFile;
import com.wlcb.jpower.chat.service.file.ChatFileService;
import com.wlcb.jpower.module.base.vo.ResponseData;
import com.wlcb.jpower.module.common.service.impl.BaseServiceImpl;
import com.wlcb.jpower.module.common.utils.Fc;
import com.wlcb.jpower.module.common.utils.FileUtil;
import com.wlcb.jpower.module.common.utils.ReturnJsonUtil;
import com.wlcb.jpower.properties.JpowerChatProperties;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

/**
 * @description:
 * @author: liwenfei
 * @time: 2020/12/25 11:52
 */
@Slf4j
@Service("chatFileServiceImpl")
public class ChatFileServiceImpl extends BaseServiceImpl<ChatFileMapper, TbChatFile> implements ChatFileService {

    @Autowired
    private ChatFileDao chatFileDao;
    @Autowired
    private JpowerChatProperties properties;

    @Override
    @SneakyThrows(IOException.class)
    public ResponseData fileSave(MultipartFile file) {
        String contentType = file.getContentType();
        if (Fc.isNull(contentType) || (!contentType.contains("image") && !contentType.contains("video"))) {
            return ReturnJsonUtil.fail("类型错误");
        }
        File saveFile = FileUtil.saveFile(file, "jpg,png,mp4", properties.getFilePath());
        TbChatFile chatFile = new TbChatFile();
        chatFile.setFileRealName(file.getOriginalFilename());
        chatFile.setFilePath(saveFile.getAbsolutePath());
        return ReturnJsonUtil.status(chatFileDao.save(chatFile));
    }

    @Override
    public TbChatFile findFile(String id) {
        return chatFileDao.getById(id);
    }
}
