package com.wlcb.jpower.chat.service.company;

import com.wlcb.jpower.chat.entity.company.TbCompany;
import com.wlcb.jpower.module.base.vo.ResponseData;
import com.wlcb.jpower.module.common.service.BaseService;

import java.io.File;
import java.util.List;

/**
 * @description:
 * @author: liwenfei
 * @time: 2021/1/8 13:34
 */
public interface CompanyService extends BaseService<TbCompany> {

    List<TbCompany> findList(TbCompany company);

    ResponseData importExcel(File file) throws Exception;
}
