package com.wlcb.jpower.chat.dbs.dao.customer;

import com.wlcb.jpower.chat.dbs.dao.customer.mapper.TbCustomerSettingsMapper;
import com.wlcb.jpower.chat.entity.customer.TbCustomerSettings;
import com.wlcb.jpower.module.dbs.dao.JpowerServiceImpl;
import org.springframework.stereotype.Repository;

/**
 * @description:
 * @author: liwenfei
 * @time: 2021/1/26 14:07
 */
@Repository
public class TbCustomerSettingsDao extends JpowerServiceImpl<TbCustomerSettingsMapper, TbCustomerSettings> {
}
