package com.wlcb.jpower.chat.dbs.dao.wxapp.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wlcb.jpower.chat.entity.wxapp.TblWxApp;
import com.wlcb.jpower.module.dbs.dao.mapper.base.JpowerBaseMapper;
import org.springframework.stereotype.Component;

@Component("tblWxAppMapper")
public interface TblWxAppMapper extends JpowerBaseMapper<TblWxApp> {
}
