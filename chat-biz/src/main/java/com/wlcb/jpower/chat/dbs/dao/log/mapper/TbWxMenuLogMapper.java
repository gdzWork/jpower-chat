package com.wlcb.jpower.chat.dbs.dao.log.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wlcb.jpower.chat.entity.log.TbWxMenuLog;
import com.wlcb.jpower.module.dbs.dao.mapper.base.JpowerBaseMapper;
import org.springframework.stereotype.Component;

/**
 * @author: liwenfei
 * @version: v1.0
 * @time: 2021/3/23 16:37
 * @desc:
 */
@Component("tbWxMenuLogMapper")
public interface TbWxMenuLogMapper extends JpowerBaseMapper<TbWxMenuLog> {
}
