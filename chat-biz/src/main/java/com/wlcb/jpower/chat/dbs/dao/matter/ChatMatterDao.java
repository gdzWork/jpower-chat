package com.wlcb.jpower.chat.dbs.dao.matter;

import com.wlcb.jpower.chat.dbs.dao.matter.mapper.TbChatMatterMapper;
import com.wlcb.jpower.chat.entity.matter.TbChatMatter;
import com.wlcb.jpower.module.dbs.dao.JpowerServiceImpl;
import org.springframework.stereotype.Repository;

/**
 * @description:
 * @author: liwenfei
 * @time: 2020/12/28 14:20
 */
@Repository
public class ChatMatterDao extends JpowerServiceImpl<TbChatMatterMapper, TbChatMatter> {
}
