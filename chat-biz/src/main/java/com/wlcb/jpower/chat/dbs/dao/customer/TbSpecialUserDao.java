package com.wlcb.jpower.chat.dbs.dao.customer;

import com.wlcb.jpower.chat.dbs.dao.customer.mapper.TbSpecialUserMapper;
import com.wlcb.jpower.chat.entity.customer.TbSpecialUser;
import com.wlcb.jpower.module.dbs.dao.JpowerServiceImpl;
import org.springframework.stereotype.Repository;

/**
 * @author: liwenfei
 * @version: v1.0
 * @time: 2021/4/13 9:18
 * @desc:
 */
@Repository
public class TbSpecialUserDao extends JpowerServiceImpl<TbSpecialUserMapper, TbSpecialUser> {
}
