package com.wlcb.jpower.chat.service.customer;

import com.wlcb.jpower.im.cache.ImChatRecordCache;
import com.wlcb.jpower.im.model.DataPacket;
import com.wlcb.jpower.module.base.vo.ResponseData;
import me.chanjar.weixin.mp.bean.message.WxMpXmlMessage;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Date;
import java.util.List;

public interface ChatService {

    List<DataPacket.Wait> getWaitUsers();

    ResponseData connectUser(String cid, String uid, String appId);

    ResponseData transferUser(String cid, String tcid, String uid, String appId);

    ResponseData finishUser(String cid, String uid, String appId, String type);

    ResponseData sendFile(MultipartFile media, String appId, String receiveId, String sendId) throws IOException;

    ResponseData offLine(String cid, String tcid, String uid, String openId, String appId);

    void showFile(String id) throws IOException;

    void forcedEnd(String openid);
}
