package com.wlcb.jpower.chat.service.demo;

import com.alibaba.fastjson.JSONObject;
import com.wlcb.jpower.chat.entity.demo.TbDemoRecord;
import com.wlcb.jpower.module.common.service.BaseService;

import java.util.List;
import java.util.Map;

/**
 * @date: 2021/5/18 13:48
 * @author: fei
 * @desc:
 */
public interface DemoRecordService extends BaseService<TbDemoRecord> {

    List<TbDemoRecord> findList(String groupId);
}
