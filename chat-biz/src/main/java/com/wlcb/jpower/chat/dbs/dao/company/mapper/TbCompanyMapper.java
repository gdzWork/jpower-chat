package com.wlcb.jpower.chat.dbs.dao.company.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wlcb.jpower.chat.entity.company.TbCompany;
import com.wlcb.jpower.module.dbs.dao.mapper.base.JpowerBaseMapper;
import org.springframework.stereotype.Component;

import java.util.List;

@Component("tbCompanyMapper")
public interface TbCompanyMapper extends JpowerBaseMapper<TbCompany> {

    List<TbCompany> findList(TbCompany company);
}
