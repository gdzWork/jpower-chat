package com.wlcb.jpower.chat.service.wx.mp.impl;

import com.alibaba.fastjson.JSON;
import com.wlcb.jpower.chat.entity.log.TbWxMenuLog;
import com.wlcb.jpower.chat.entity.wxapp.TblWxApp;
import com.wlcb.jpower.chat.pojo.dingding.DRequest;
import com.wlcb.jpower.chat.service.DingdingService;
import com.wlcb.jpower.chat.service.log.WxMenuLogService;
import com.wlcb.jpower.chat.service.wx.mp.WxAppService;
import com.wlcb.jpower.chat.service.wx.mp.WxMenuService;
import com.wlcb.jpower.module.base.exception.BusinessException;
import com.wlcb.jpower.module.common.utils.Fc;
import com.wlcb.jpower.utils.WxMpInstance;
import lombok.extern.slf4j.Slf4j;
import me.chanjar.weixin.common.bean.menu.WxMenu;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.menu.WxMpMenu;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author: liwenfei
 * @version: v1.0
 * @time: 2021/3/5 9:57
 * @desc:
 */
@Slf4j
@Service("wxMenuControllerImpl")
public class WxMenuServiceImpl implements WxMenuService {

    @Autowired
    private DingdingService dingdingService;
    @Autowired
    private WxAppService wxAppService;
    @Autowired
    private WxMenuLogService wxMenuLogService;

    @Override
    public boolean createMenu(WxMenu wxMenu, String appId, boolean exSend) {
        WxMpService wxMpService = WxMpInstance.getWxMp();
        boolean switchover = wxMpService.switchover(appId);
        if (!switchover) {
            throw new BusinessException("公众号切换异常，未找到微信配置数据 appId：" + appId);
        }
        try {
            String result = wxMpService.getMenuService().menuCreate(wxMenu);
            log.info("菜单创建结果：{}", result);
            TbWxMenuLog log = new TbWxMenuLog();
            log.setAppid(appId);
            log.setMenu(JSON.toJSONString(wxMenu));
            wxMenuLogService.saveMenuLog(log);
            return true;
        } catch (WxErrorException e) {
            e.printStackTrace();
            if (exSend) {
                String mpName = "";
                TblWxApp wxApp = wxAppService.findOne(appId);
                if (!Fc.isNull(wxApp)) {
                    mpName = wxApp.getMpName();
                }
                DRequest dRequest = new DRequest("微信菜单创建", appId, mpName, e.getError().getJson());
                dingdingService.sendEx(dRequest);
            }
            return false;
        }
    }

    @Override
    public WxMpMenu findMenu(String appId) {
        WxMpService wxMpService = WxMpInstance.getWxMp();
        boolean switchover = wxMpService.switchover(appId);
        if (!switchover) {
            throw new BusinessException("公众号切换异常，未找到微信配置数据 appId：" + appId);
        }
        try {
            return wxMpService.getMenuService().menuGet();
        } catch (WxErrorException e) {
            e.printStackTrace();
            log.error("菜单查询异常：{}", e.toString());
            throw new BusinessException("菜单查询异常 appId：" + appId);
        }
    }

    @Override
    public boolean deleteMenu(String appId) {
        WxMpService wxMpService = WxMpInstance.getWxMp();
        boolean switchover = wxMpService.switchover(appId);
        if (!switchover) {
            throw new BusinessException("公众号切换异常 appId：" + appId);
        }
        try {
            wxMpService.getMenuService().menuDelete();
            return true;
        } catch (WxErrorException e) {
            e.printStackTrace();
            log.error("菜单查询异常：{}", e.toString());
            return false;
        }
    }
}
