package com.wlcb.jpower.chat.dbs.dao.customer.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wlcb.jpower.chat.entity.customer.TbCustomerSettings;
import com.wlcb.jpower.module.dbs.dao.mapper.base.JpowerBaseMapper;
import org.springframework.stereotype.Component;

/**
 * @description:
 * @author: liwenfei
 * @time: 2021/1/26 14:07
 */
@Component("tbCustomerSettingsMapper")
public interface TbCustomerSettingsMapper extends JpowerBaseMapper<TbCustomerSettings> {
}
