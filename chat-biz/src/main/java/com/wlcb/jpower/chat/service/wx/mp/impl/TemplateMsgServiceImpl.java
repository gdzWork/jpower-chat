package com.wlcb.jpower.chat.service.wx.mp.impl;

import com.wlcb.jpower.chat.entity.wxapp.TblWxApp;
import com.wlcb.jpower.chat.pojo.dingding.DRequest;
import com.wlcb.jpower.chat.service.DingdingService;
import com.wlcb.jpower.chat.service.wx.mp.TemplateMsgService;
import com.wlcb.jpower.chat.service.wx.mp.WxAppService;
import com.wlcb.jpower.module.base.exception.BusinessException;
import com.wlcb.jpower.module.common.utils.Fc;
import com.wlcb.jpower.utils.WxMpInstance;
import com.wlcb.jpower.wx.mp.template.WxTemplateRequest;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.template.WxMpTemplateData;
import me.chanjar.weixin.mp.bean.template.WxMpTemplateMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author: liwenfei
 * @version: v1.0
 * @time: 2021/3/2 14:50
 * @desc:
 */
@Service("templateMsgServiceImpl")
public class TemplateMsgServiceImpl implements TemplateMsgService {

    @Autowired
    private WxAppService wxAppService;
    @Autowired
    private DingdingService dingdingService;

    /**
     * 发送模板消息
     *
     * @param template
     * @param exSend
     * @return
     */
    @Override
    public int templateSend(WxTemplateRequest template, boolean exSend) {
        int info = 0;
        String mpName = "";
        try {
            WxMpService wxMpService = WxMpInstance.getWxMp();

            if (!wxMpService.switchover(template.getAppId())) {
                throw new BusinessException("公众号切换异常 appId：" + template.getAppId());
            }

            TblWxApp tblWxApp = wxAppService.findOne(template.getAppId());
            if (!Fc.isNull(tblWxApp)) {
                mpName = tblWxApp.getMpName();
            }
            WxMpTemplateMessage wxMp = new WxMpTemplateMessage();
            wxMp.setTemplateId(template.getTemplateId());
            wxMp.setUrl(template.getUrl());
            List<WxMpTemplateData> data = new ArrayList<>();
            List<WxTemplateRequest.WxMpTemplateData> dataList = template.getData();
            dataList.forEach(x -> {
                WxMpTemplateData wxMpTemplateData = new WxMpTemplateData();
                wxMpTemplateData.setName(x.getName());
                wxMpTemplateData.setValue(x.getValue());
                wxMpTemplateData.setColor(x.getColor());
                data.add(wxMpTemplateData);
            });
            wxMp.setData(data);

            //添加小程序链接
            if (template.getMiniProgram() != null) {
                WxMpTemplateMessage.MiniProgram miniProgram = new WxMpTemplateMessage.MiniProgram();
                miniProgram.setAppid(template.getMiniProgram().getAppid());
                miniProgram.setPagePath(template.getMiniProgram().getPagePath());
                wxMp.setMiniProgram(miniProgram);
            }

            for (String toUser : template.getToUsers()) {
                wxMp.setToUser(toUser);
                try {
                    wxMpService.getTemplateMsgService().sendTemplateMsg(wxMp);
                    info++;
                } catch (WxErrorException e) {
                    if (exSend) {
                        DRequest dRequest = new DRequest("模板消息", template.getAppId(), mpName, e.getError().getJson());
                        dingdingService.sendEx(dRequest);
                    }
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            if (exSend) {
                DRequest dRequest = new DRequest("模板消息", template.getAppId(), mpName, e.toString());
                dingdingService.sendEx(dRequest);
            }
            e.printStackTrace();
        }
        return info;
    }
}
