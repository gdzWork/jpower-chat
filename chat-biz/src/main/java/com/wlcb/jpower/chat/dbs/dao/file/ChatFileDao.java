package com.wlcb.jpower.chat.dbs.dao.file;

import com.wlcb.jpower.chat.dbs.dao.file.mapper.ChatFileMapper;
import com.wlcb.jpower.chat.entity.file.TbChatFile;
import com.wlcb.jpower.module.dbs.dao.JpowerServiceImpl;
import org.springframework.stereotype.Repository;

/**
 * @description:
 * @author: liwenfei
 * @time: 2020/12/25 10:14
 */
@Repository
public class ChatFileDao extends JpowerServiceImpl<ChatFileMapper, TbChatFile> {
}
