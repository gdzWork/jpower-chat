package com.wlcb.jpower.chat.dbs.dao.wxapp;

import com.wlcb.jpower.chat.dbs.dao.wxapp.mapper.TblWxAppMapper;
import com.wlcb.jpower.chat.entity.wxapp.TblWxApp;
import com.wlcb.jpower.module.dbs.dao.JpowerServiceImpl;
import org.springframework.stereotype.Repository;

@Repository
public class TblWxAppDao extends JpowerServiceImpl<TblWxAppMapper, TblWxApp> {
}
