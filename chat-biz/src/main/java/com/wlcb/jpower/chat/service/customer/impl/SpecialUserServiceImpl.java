package com.wlcb.jpower.chat.service.customer.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.support.SFunction;
import com.wlcb.jpower.chat.dbs.dao.customer.TbSpecialUserDao;
import com.wlcb.jpower.chat.dbs.dao.customer.mapper.TbSpecialUserMapper;
import com.wlcb.jpower.chat.entity.customer.TbSpecialUser;
import com.wlcb.jpower.chat.service.customer.SpecialUserService;
import com.wlcb.jpower.module.common.service.impl.BaseServiceImpl;
import com.wlcb.jpower.module.common.utils.Fc;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author: liwenfei
 * @version: v1.0
 * @time: 2021/4/13 9:22
 * @desc:
 */
@Service
public class SpecialUserServiceImpl extends BaseServiceImpl<TbSpecialUserMapper, TbSpecialUser> implements SpecialUserService {

    @Autowired
    private TbSpecialUserDao specialUserDao;

    @Override
    public List<TbSpecialUser> findList(TbSpecialUser specialUser) {
        LambdaQueryWrapper<TbSpecialUser> queryWrapper = new QueryWrapper<TbSpecialUser>().lambda();
        queryWrapper.orderByDesc(TbSpecialUser::getCreateTime);
        Map<SFunction<TbSpecialUser, ?>, Object> map = new HashMap<>();
        map.put(TbSpecialUser::getIdcard, specialUser.getId());
        map.put(TbSpecialUser::getName, specialUser.getName());
        map.put(TbSpecialUser::getPhone, specialUser.getPhone());
        map.put(TbSpecialUser::getCompany, specialUser.getCompany());
        queryWrapper.allEq(map, false);
        return specialUserDao.list(queryWrapper);
    }

    @Override
    public TbSpecialUser match(String name, String idcard, String phone) {
        LambdaQueryWrapper<TbSpecialUser> queryWrapper = new QueryWrapper<TbSpecialUser>().lambda();
        queryWrapper.eq(TbSpecialUser::getName, name);
        if (Fc.isNotBlank(idcard)) {
            queryWrapper.eq(TbSpecialUser::getIdcard, idcard);
        }
        if (Fc.isNotBlank(phone)) {
            queryWrapper.eq(TbSpecialUser::getPhone, phone);
        }
        return specialUserDao.getOne(queryWrapper, false);
    }

    @Override
    public TbSpecialUser findByOpenidAndAppid(String openid, String appid) {
        return specialUserDao.getOne(new QueryWrapper<TbSpecialUser>().lambda()
                .eq(TbSpecialUser::getOpenid, openid).eq(TbSpecialUser::getAppid, appid));
    }
}
