package com.wlcb.jpower.chat.service.customer;

import com.wlcb.jpower.chat.entity.customer.TbSpecialUser;
import com.wlcb.jpower.module.common.service.BaseService;

import java.util.List;

public interface SpecialUserService extends BaseService<TbSpecialUser> {

    List<TbSpecialUser> findList(TbSpecialUser specialUser);

    TbSpecialUser match(String name, String idcard, String phone);

    TbSpecialUser findByOpenidAndAppid(String openid,String appid);
}
