package com.wlcb.jpower.chat.service.customer.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.wlcb.jpower.chat.dbs.dao.customer.TbCustomerSettingsDao;
import com.wlcb.jpower.chat.dbs.dao.customer.mapper.TbCustomerSettingsMapper;
import com.wlcb.jpower.chat.entity.customer.TbCustomerSettings;
import com.wlcb.jpower.chat.service.customer.CustomerSettingService;
import com.wlcb.jpower.constant.DBConstant;
import com.wlcb.jpower.module.common.service.impl.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @description:
 * @author: liwenfei
 * @time: 2021/1/26 14:13
 */
@Service("customerSettingServiceImpl")
public class CustomerSettingServiceImpl extends BaseServiceImpl<TbCustomerSettingsMapper, TbCustomerSettings> implements CustomerSettingService {

    @Autowired
    private TbCustomerSettingsDao customerSettingsDao;

    @Override
    public boolean save(String cid) {
        TbCustomerSettings customerSettings = customerSettingsDao.getOne(new QueryWrapper<TbCustomerSettings>().lambda()
                .eq(TbCustomerSettings::getCid, cid), false);
        if (customerSettings != null) {
            return false;
        }
        TbCustomerSettings settings = new TbCustomerSettings();
        settings.setCid(cid);
        return customerSettingsDao.save(settings);
    }

    @Override
    public TbCustomerSettings findByCid(String cid) {
        return customerSettingsDao.getOne(new QueryWrapper<TbCustomerSettings>().lambda()
                .eq(TbCustomerSettings::getCid, cid), false);
    }

    @Override
    public boolean modify(TbCustomerSettings settings) {
        return customerSettingsDao.updateById(settings);
    }

    /**
     * 开启自动接入的列表
     * @return
     */
    @Override
    public List<String> findAutoAccess() {
        List<TbCustomerSettings> list = customerSettingsDao.list(new QueryWrapper<TbCustomerSettings>().lambda()
                .eq(TbCustomerSettings::getAutoAccess, DBConstant.EnableEnum.OPEN.getType()));
        return list.stream().map(TbCustomerSettings::getCid).collect(Collectors.toList());
    }

    /**
     * 接入用户自动回复
     *
     * @param cid
     * @return
     */
    @Override
    public String findCidIsAutoReply(String cid) {
        TbCustomerSettings settings = customerSettingsDao.getOne(new QueryWrapper<TbCustomerSettings>()
                .lambda().eq(TbCustomerSettings::getCid, cid), false);
        if (settings != null && settings.getAutoAccessReply().intValue() == DBConstant.EnableEnum.OPEN.getType()) {
            return settings.getReplyContent();
        }
        return "";
    }

    /**
     * 是否允许转接
     *
     * @param cid
     * @return
     */
    @Override
    public boolean findCidAllowTransfer(String cid) {
        TbCustomerSettings settings = customerSettingsDao.getOne(new QueryWrapper<TbCustomerSettings>().lambda().eq(TbCustomerSettings::getCid, cid), false);
        if (settings.getAllowTransfer().intValue() == DBConstant.EnableEnum.OPEN.getType()) {
            return false;
        } else if (settings.getAllowTransfer().intValue() == DBConstant.EnableEnum.CLOSR.getType()) {
            return true;
        }
        return true;
    }

    @Override
    public Boolean findCidAutoReply(String cid) {
        TbCustomerSettings settings = findByCid(cid);
        return settings != null && settings.getAutoReply().intValue() == DBConstant.EnableEnum.OPEN.getType();
    }
}
