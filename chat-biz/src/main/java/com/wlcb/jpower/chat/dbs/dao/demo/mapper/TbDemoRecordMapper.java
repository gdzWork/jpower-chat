package com.wlcb.jpower.chat.dbs.dao.demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wlcb.jpower.chat.entity.demo.TbDemoRecord;
import com.wlcb.jpower.module.dbs.dao.mapper.base.JpowerBaseMapper;
import org.springframework.stereotype.Component;

/**
 * @date: 2021/5/18 13:47
 * @author: fei
 * @desc:
 */
@Component
public interface TbDemoRecordMapper extends JpowerBaseMapper<TbDemoRecord> {
}
