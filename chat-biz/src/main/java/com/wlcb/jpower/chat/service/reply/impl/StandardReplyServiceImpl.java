package com.wlcb.jpower.chat.service.reply.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.support.SFunction;
import com.wlcb.jpower.chat.dbs.dao.reply.TbStandardReplyDao;
import com.wlcb.jpower.chat.dbs.dao.reply.mapper.TbStandardReplyMapper;
import com.wlcb.jpower.chat.entity.reply.TbStandardReply;
import com.wlcb.jpower.chat.service.reply.StandardReplyService;
import com.wlcb.jpower.module.common.service.impl.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("standardReplyServiceImpl")
public class StandardReplyServiceImpl extends BaseServiceImpl<TbStandardReplyMapper, TbStandardReply> implements StandardReplyService {

    @Autowired
    private TbStandardReplyDao standardReplyDao;

    @Override
    public List<TbStandardReply> findList(Integer status, String cid) {
        LambdaQueryWrapper<TbStandardReply> queryWrapper = new QueryWrapper<TbStandardReply>().lambda();
        queryWrapper.orderByAsc(TbStandardReply::getCreateTime);
        queryWrapper.eq(TbStandardReply::getIsDeleted, 0);
        Map<SFunction<TbStandardReply, ?>, Object> map = new HashMap<>();
        map.put(TbStandardReply::getCid, cid);
        map.put(TbStandardReply::getStatus, status);
        queryWrapper.allEq(map, false);
        return standardReplyDao.list(queryWrapper);
    }
}
