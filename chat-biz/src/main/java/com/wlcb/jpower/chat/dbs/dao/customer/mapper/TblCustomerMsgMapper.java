package com.wlcb.jpower.chat.dbs.dao.customer.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wlcb.jpower.chat.entity.customer.TblCustomerMsg;
import com.wlcb.jpower.module.dbs.dao.mapper.base.JpowerBaseMapper;
import org.springframework.stereotype.Component;

@Component("tblCustomerMsgMapper")
public interface TblCustomerMsgMapper extends JpowerBaseMapper<TblCustomerMsg> {
}
