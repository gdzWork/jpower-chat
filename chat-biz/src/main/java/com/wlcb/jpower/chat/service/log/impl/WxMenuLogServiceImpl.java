package com.wlcb.jpower.chat.service.log.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.support.SFunction;
import com.wlcb.jpower.chat.dbs.dao.log.TbWxMenuLogDao;
import com.wlcb.jpower.chat.dbs.dao.log.mapper.TbWxMenuLogMapper;
import com.wlcb.jpower.chat.entity.log.TbWxMenuLog;
import com.wlcb.jpower.chat.service.log.WxMenuLogService;
import com.wlcb.jpower.module.common.service.impl.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author: liwenfei
 * @version: v1.0
 * @time: 2021/3/23 16:44
 * @desc: 菜单创建时 备份菜单数据
 */
@Service("wxMenuLogServiceImpl")
public class WxMenuLogServiceImpl extends BaseServiceImpl<TbWxMenuLogMapper, TbWxMenuLog> implements WxMenuLogService {

    @Autowired
    private TbWxMenuLogDao wxMenuLogDao;

    @Async("threadPool")
    @Override
    public void saveMenuLog(TbWxMenuLog log) {
        wxMenuLogDao.save(log);
    }

    @Override
    public List<TbWxMenuLog> findList(String appId, String createUser) {
        LambdaQueryWrapper<TbWxMenuLog> queryWrapper = new QueryWrapper<TbWxMenuLog>().lambda();
        queryWrapper.orderByDesc(TbWxMenuLog::getCreateTime);
        Map<SFunction<TbWxMenuLog, ?>, Object> map = new HashMap<>();
        map.put(TbWxMenuLog::getAppid, appId);
        map.put(TbWxMenuLog::getCreateUser, createUser);
        queryWrapper.allEq(map, false);
        return wxMenuLogDao.list(queryWrapper);
    }
}
