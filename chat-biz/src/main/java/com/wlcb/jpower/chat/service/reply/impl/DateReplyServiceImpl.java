package com.wlcb.jpower.chat.service.reply.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.support.SFunction;
import com.wlcb.jpower.chat.dbs.dao.reply.TbDateReplyDao;
import com.wlcb.jpower.chat.dbs.dao.reply.mapper.TbDateReplyMapper;
import com.wlcb.jpower.chat.entity.reply.TbDateReply;
import com.wlcb.jpower.chat.service.reply.DateReplyService;
import com.wlcb.jpower.constant.DBConstant;
import com.wlcb.jpower.module.common.service.impl.BaseServiceImpl;
import com.wlcb.jpower.module.common.utils.Fc;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @description:
 * @author: liwenfei
 * @time: 2021/2/4 17:03
 */
@Service("dateReplyServiceImpl")
public class DateReplyServiceImpl extends BaseServiceImpl<TbDateReplyMapper, TbDateReply> implements DateReplyService {

    @Autowired
    private TbDateReplyDao replyDao;

    @Override
    public List<TbDateReply> findPageList(TbDateReply reply) {
        LambdaQueryWrapper<TbDateReply> queryWrapper = new QueryWrapper<TbDateReply>().lambda();
        queryWrapper.orderByDesc(TbDateReply::getCreateTime);
        Map<SFunction<TbDateReply, ?>, Object> map = new HashMap<>();
        map.put(TbDateReply::getAppid, reply.getAppid());
        map.put(TbDateReply::getAsk, reply.getAsk());
        map.put(TbDateReply::getStatus, reply.getStatus());
        map.put(TbDateReply::getRemark, reply.getRemark());
        queryWrapper.allEq(map, false);
        return replyDao.list(queryWrapper);
    }

    @Override
    public List<TbDateReply> findList(String appId) {
        return replyDao.list(new QueryWrapper<TbDateReply>().lambda()
                .eq(TbDateReply::getAppid, appId)
                .eq(TbDateReply::getStatus, DBConstant.EnableEnum.OPEN.getType()));
    }


    @Override
    public TbDateReply isDate(String appId) {
        long timeMillis = System.currentTimeMillis();
        List<TbDateReply> list = findList(appId);
        if (list.size() > 0) {
            for (TbDateReply reply : list) {
                if (Fc.notNull(reply.getStartTime()) && Fc.notNull(reply.getEndTime())) {
                    long start = reply.getStartTime().getTime();
                    long end = reply.getEndTime().getTime();
                    if (timeMillis >= start && timeMillis <= end) {
                        return reply;
                    }
                }
            }
        }
        return null;
    }

    @Override
    public TbDateReply findByAsk(String ask, String appid) {
        return replyDao.getOne(new QueryWrapper<TbDateReply>().lambda().eq(TbDateReply::getAsk, ask)
                        .eq(TbDateReply::getAppid, appid)
                        .eq(TbDateReply::getStatus, DBConstant.EnableEnum.OPEN.getType())
                , false);
    }
}
