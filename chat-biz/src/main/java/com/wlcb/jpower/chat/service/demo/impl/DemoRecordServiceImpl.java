package com.wlcb.jpower.chat.service.demo.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.wlcb.jpower.chat.dbs.dao.demo.TbDemoRecordDao;
import com.wlcb.jpower.chat.dbs.dao.demo.mapper.TbDemoRecordMapper;
import com.wlcb.jpower.chat.entity.demo.TbDemoRecord;
import com.wlcb.jpower.chat.service.demo.DemoRecordService;
import com.wlcb.jpower.module.common.service.impl.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.xml.ws.ServiceMode;
import java.util.List;
import java.util.Map;

/**
 * @date: 2021/5/18 13:49
 * @author: fei
 * @desc:
 */
@Service
public class DemoRecordServiceImpl extends BaseServiceImpl<TbDemoRecordMapper, TbDemoRecord> implements DemoRecordService {

    @Autowired
    private TbDemoRecordDao demoRecordDao;

    @Override
    public List<TbDemoRecord> findList(String groupId) {
        return demoRecordDao.list(new QueryWrapper<TbDemoRecord>().lambda()
                .eq(TbDemoRecord::getGroupId, groupId).orderByDesc(TbDemoRecord::getCreateTime));
    }
}
