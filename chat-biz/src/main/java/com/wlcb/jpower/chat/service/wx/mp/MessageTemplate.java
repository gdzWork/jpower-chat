package com.wlcb.jpower.chat.service.wx.mp;

import com.wlcb.jpower.wx.mp.template.WxTemplateRequest;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName MessageTemplate
 * @Author liwenfei
 * @Date 2020/7/2 9:59
 * @Version 1.0
 */
@Component
public class MessageTemplate {

    private WxTemplateRequest.WxMpTemplateData getWxMpTemplateData() {
        return new WxTemplateRequest.WxMpTemplateData();
    }

    private WxTemplateRequest.MiniProgram getMiniProgram() {
        return new WxTemplateRequest.MiniProgram();
    }

    public WxTemplateRequest createTemplate(SendTemplateMsgEntity sendTemplateMsgEntity) {
        WxTemplateRequest wxTemplateEntity = new WxTemplateRequest();
        wxTemplateEntity.setAppId(sendTemplateMsgEntity.getAppId());
        wxTemplateEntity.setTemplateId(sendTemplateMsgEntity.getTemplateId());
        Integer count = sendTemplateMsgEntity.getParamCount();
        List<WxTemplateRequest.WxMpTemplateData> dataList = new ArrayList<>();

        WxTemplateRequest.WxMpTemplateData wxMpTemplateDataFirst = getWxMpTemplateData();
        wxMpTemplateDataFirst.setName("first");
        wxMpTemplateDataFirst.setValue(sendTemplateMsgEntity.getFirst());
        wxMpTemplateDataFirst.setColor(sendTemplateMsgEntity.getColor());
        WxTemplateRequest.WxMpTemplateData wxMpTemplateDataRemark = getWxMpTemplateData();
        wxMpTemplateDataRemark.setName("remark");
        wxMpTemplateDataRemark.setValue(sendTemplateMsgEntity.getRemark());
        wxMpTemplateDataRemark.setColor(sendTemplateMsgEntity.getColor());
        dataList.add(wxMpTemplateDataFirst);
        dataList.add(wxMpTemplateDataRemark);
        switch (count) {
            case 2: {
                if (StringUtils.isBlank(sendTemplateMsgEntity.getKeyword1())) {
                    return null;
                }
                if (StringUtils.isBlank(sendTemplateMsgEntity.getKeyword2())) {
                    return null;
                }
                WxTemplateRequest.WxMpTemplateData wxMpTemplateData1 = getWxMpTemplateData();
                wxMpTemplateData1.setName("keyword1");
                wxMpTemplateData1.setValue(sendTemplateMsgEntity.getKeyword1());
                wxMpTemplateData1.setColor(sendTemplateMsgEntity.getColor());
                WxTemplateRequest.WxMpTemplateData wxMpTemplateData2 = getWxMpTemplateData();
                wxMpTemplateData2.setName("keyword2");
                wxMpTemplateData2.setValue(sendTemplateMsgEntity.getKeyword2());
                wxMpTemplateData2.setColor(sendTemplateMsgEntity.getColor());
                dataList.add(wxMpTemplateData1);
                dataList.add(wxMpTemplateData2);
                break;
            }
            case 3: {
                if (StringUtils.isBlank(sendTemplateMsgEntity.getKeyword1())) {
                    return null;
                }
                if (StringUtils.isBlank(sendTemplateMsgEntity.getKeyword2())) {
                    return null;
                }
                if (StringUtils.isBlank(sendTemplateMsgEntity.getKeyword3())) {
                    return null;
                }
                WxTemplateRequest.WxMpTemplateData wxMpTemplateData1 = getWxMpTemplateData();
                wxMpTemplateData1.setName("keyword1");
                wxMpTemplateData1.setValue(sendTemplateMsgEntity.getKeyword1());
                wxMpTemplateData1.setColor(sendTemplateMsgEntity.getColor());
                WxTemplateRequest.WxMpTemplateData wxMpTemplateData2 = getWxMpTemplateData();
                wxMpTemplateData2.setName("keyword2");
                wxMpTemplateData2.setColor(sendTemplateMsgEntity.getColor());
                wxMpTemplateData2.setValue(sendTemplateMsgEntity.getKeyword2());
                WxTemplateRequest.WxMpTemplateData wxMpTemplateData3 = getWxMpTemplateData();
                wxMpTemplateData3.setName("keyword3");
                wxMpTemplateData3.setValue(sendTemplateMsgEntity.getKeyword3());
                wxMpTemplateData3.setColor(sendTemplateMsgEntity.getColor());
                dataList.add(wxMpTemplateData1);
                dataList.add(wxMpTemplateData2);
                dataList.add(wxMpTemplateData3);
                break;
            }
            case 4: {
                if (StringUtils.isBlank(sendTemplateMsgEntity.getKeyword1())) {
                    return null;
                }
                if (StringUtils.isBlank(sendTemplateMsgEntity.getKeyword2())) {
                    return null;
                }
                if (StringUtils.isBlank(sendTemplateMsgEntity.getKeyword3())) {
                    return null;
                }
                if (StringUtils.isBlank(sendTemplateMsgEntity.getKeyword4())) {
                    return null;
                }
                WxTemplateRequest.WxMpTemplateData wxMpTemplateData1 = getWxMpTemplateData();
                wxMpTemplateData1.setName("keyword1");
                wxMpTemplateData1.setValue(sendTemplateMsgEntity.getKeyword1());
                wxMpTemplateData1.setColor(sendTemplateMsgEntity.getColor());
                WxTemplateRequest.WxMpTemplateData wxMpTemplateData2 = getWxMpTemplateData();
                wxMpTemplateData2.setName("keyword2");
                wxMpTemplateData2.setValue(sendTemplateMsgEntity.getKeyword2());
                wxMpTemplateData2.setColor(sendTemplateMsgEntity.getColor());
                WxTemplateRequest.WxMpTemplateData wxMpTemplateData3 = getWxMpTemplateData();
                wxMpTemplateData3.setName("keyword3");
                wxMpTemplateData3.setValue(sendTemplateMsgEntity.getKeyword3());
                wxMpTemplateData3.setColor(sendTemplateMsgEntity.getColor());
                WxTemplateRequest.WxMpTemplateData wxMpTemplateData4 = getWxMpTemplateData();
                wxMpTemplateData4.setName("keyword4");
                wxMpTemplateData4.setValue(sendTemplateMsgEntity.getKeyword4());
                wxMpTemplateData4.setColor(sendTemplateMsgEntity.getColor());
                dataList.add(wxMpTemplateData1);
                dataList.add(wxMpTemplateData2);
                dataList.add(wxMpTemplateData3);
                dataList.add(wxMpTemplateData4);
                break;
            }
            case 5: {
                if (StringUtils.isBlank(sendTemplateMsgEntity.getKeyword1())) {
                    return null;
                }
                if (StringUtils.isBlank(sendTemplateMsgEntity.getKeyword2())) {
                    return null;
                }
                if (StringUtils.isBlank(sendTemplateMsgEntity.getKeyword3())) {
                    return null;
                }
                if (StringUtils.isBlank(sendTemplateMsgEntity.getKeyword4())) {
                    return null;
                }
                if (StringUtils.isBlank(sendTemplateMsgEntity.getKeyword5())) {
                    return null;
                }
                WxTemplateRequest.WxMpTemplateData wxMpTemplateData1 = getWxMpTemplateData();
                wxMpTemplateData1.setName("keyword1");
                wxMpTemplateData1.setValue(sendTemplateMsgEntity.getKeyword1());
                wxMpTemplateData1.setColor(sendTemplateMsgEntity.getColor());
                WxTemplateRequest.WxMpTemplateData wxMpTemplateData2 = getWxMpTemplateData();
                wxMpTemplateData2.setName("keyword2");
                wxMpTemplateData2.setValue(sendTemplateMsgEntity.getKeyword2());
                wxMpTemplateData2.setColor(sendTemplateMsgEntity.getColor());
                WxTemplateRequest.WxMpTemplateData wxMpTemplateData3 = getWxMpTemplateData();
                wxMpTemplateData3.setName("keyword3");
                wxMpTemplateData3.setValue(sendTemplateMsgEntity.getKeyword3());
                wxMpTemplateData3.setColor(sendTemplateMsgEntity.getColor());
                WxTemplateRequest.WxMpTemplateData wxMpTemplateData4 = getWxMpTemplateData();
                wxMpTemplateData4.setName("keyword4");
                wxMpTemplateData4.setValue(sendTemplateMsgEntity.getKeyword4());
                wxMpTemplateData4.setColor(sendTemplateMsgEntity.getColor());
                WxTemplateRequest.WxMpTemplateData wxMpTemplateData5 = getWxMpTemplateData();
                wxMpTemplateData5.setName("keyword5");
                wxMpTemplateData5.setValue(sendTemplateMsgEntity.getKeyword5());
                wxMpTemplateData5.setColor(sendTemplateMsgEntity.getColor());
                dataList.add(wxMpTemplateData1);
                dataList.add(wxMpTemplateData2);
                dataList.add(wxMpTemplateData3);
                dataList.add(wxMpTemplateData4);
                dataList.add(wxMpTemplateData5);
                break;
            }
            case 6: {
                if (StringUtils.isBlank(sendTemplateMsgEntity.getKeyword1())) {
                    return null;
                }
                if (StringUtils.isBlank(sendTemplateMsgEntity.getKeyword2())) {
                    return null;
                }
                if (StringUtils.isBlank(sendTemplateMsgEntity.getKeyword3())) {
                    return null;
                }
                if (StringUtils.isBlank(sendTemplateMsgEntity.getKeyword4())) {
                    return null;
                }
                if (StringUtils.isBlank(sendTemplateMsgEntity.getKeyword5())) {
                    return null;
                }
                if (StringUtils.isBlank(sendTemplateMsgEntity.getKeyword6())) {
                    return null;
                }
                WxTemplateRequest.WxMpTemplateData wxMpTemplateData1 = getWxMpTemplateData();
                wxMpTemplateData1.setName("keyword1");
                wxMpTemplateData1.setValue(sendTemplateMsgEntity.getKeyword1());
                wxMpTemplateData1.setColor(sendTemplateMsgEntity.getColor());
                WxTemplateRequest.WxMpTemplateData wxMpTemplateData2 = getWxMpTemplateData();
                wxMpTemplateData2.setName("keyword2");
                wxMpTemplateData2.setValue(sendTemplateMsgEntity.getKeyword2());
                wxMpTemplateData2.setColor(sendTemplateMsgEntity.getColor());
                WxTemplateRequest.WxMpTemplateData wxMpTemplateData3 = getWxMpTemplateData();
                wxMpTemplateData3.setName("keyword3");
                wxMpTemplateData3.setValue(sendTemplateMsgEntity.getKeyword3());
                wxMpTemplateData3.setColor(sendTemplateMsgEntity.getColor());
                WxTemplateRequest.WxMpTemplateData wxMpTemplateData4 = getWxMpTemplateData();
                wxMpTemplateData4.setName("keyword4");
                wxMpTemplateData4.setValue(sendTemplateMsgEntity.getKeyword4());
                wxMpTemplateData4.setColor(sendTemplateMsgEntity.getColor());
                WxTemplateRequest.WxMpTemplateData wxMpTemplateData5 = getWxMpTemplateData();
                wxMpTemplateData5.setName("keyword5");
                wxMpTemplateData5.setValue(sendTemplateMsgEntity.getKeyword5());
                wxMpTemplateData5.setColor(sendTemplateMsgEntity.getColor());
                WxTemplateRequest.WxMpTemplateData wxMpTemplateData6 = getWxMpTemplateData();
                wxMpTemplateData6.setName("keyword6");
                wxMpTemplateData6.setValue(sendTemplateMsgEntity.getKeyword6());
                wxMpTemplateData6.setColor(sendTemplateMsgEntity.getColor());
                dataList.add(wxMpTemplateData1);
                dataList.add(wxMpTemplateData2);
                dataList.add(wxMpTemplateData3);
                dataList.add(wxMpTemplateData4);
                dataList.add(wxMpTemplateData5);
                dataList.add(wxMpTemplateData6);
                break;
            }
            case 7: {
                if (StringUtils.isBlank(sendTemplateMsgEntity.getKeyword1())) {
                    return null;
                }
                if (StringUtils.isBlank(sendTemplateMsgEntity.getKeyword2())) {
                    return null;
                }
                if (StringUtils.isBlank(sendTemplateMsgEntity.getKeyword3())) {
                    return null;
                }
                if (StringUtils.isBlank(sendTemplateMsgEntity.getKeyword4())) {
                    return null;
                }
                if (StringUtils.isBlank(sendTemplateMsgEntity.getKeyword5())) {
                    return null;
                }
                if (StringUtils.isBlank(sendTemplateMsgEntity.getKeyword6())) {
                    return null;
                }
                if (StringUtils.isBlank(sendTemplateMsgEntity.getKeyword7())) {
                    return null;
                }
                WxTemplateRequest.WxMpTemplateData wxMpTemplateData1 = getWxMpTemplateData();
                wxMpTemplateData1.setName("keyword1");
                wxMpTemplateData1.setValue(sendTemplateMsgEntity.getKeyword1());
                wxMpTemplateData1.setColor(sendTemplateMsgEntity.getColor());
                WxTemplateRequest.WxMpTemplateData wxMpTemplateData2 = getWxMpTemplateData();
                wxMpTemplateData2.setName("keyword2");
                wxMpTemplateData2.setValue(sendTemplateMsgEntity.getKeyword2());
                wxMpTemplateData2.setColor(sendTemplateMsgEntity.getColor());
                WxTemplateRequest.WxMpTemplateData wxMpTemplateData3 = getWxMpTemplateData();
                wxMpTemplateData3.setName("keyword3");
                wxMpTemplateData3.setValue(sendTemplateMsgEntity.getKeyword3());
                wxMpTemplateData3.setColor(sendTemplateMsgEntity.getColor());
                WxTemplateRequest.WxMpTemplateData wxMpTemplateData4 = getWxMpTemplateData();
                wxMpTemplateData4.setName("keyword4");
                wxMpTemplateData4.setValue(sendTemplateMsgEntity.getKeyword4());
                wxMpTemplateData4.setColor(sendTemplateMsgEntity.getColor());
                WxTemplateRequest.WxMpTemplateData wxMpTemplateData5 = getWxMpTemplateData();
                wxMpTemplateData5.setName("keyword5");
                wxMpTemplateData5.setValue(sendTemplateMsgEntity.getKeyword5());
                wxMpTemplateData5.setColor(sendTemplateMsgEntity.getColor());
                WxTemplateRequest.WxMpTemplateData wxMpTemplateData6 = getWxMpTemplateData();
                wxMpTemplateData6.setName("keyword6");
                wxMpTemplateData6.setValue(sendTemplateMsgEntity.getKeyword6());
                wxMpTemplateData6.setColor(sendTemplateMsgEntity.getColor());
                WxTemplateRequest.WxMpTemplateData wxMpTemplateData7 = getWxMpTemplateData();
                wxMpTemplateData7.setName("keyword7");
                wxMpTemplateData7.setValue(sendTemplateMsgEntity.getKeyword7());
                wxMpTemplateData7.setColor(sendTemplateMsgEntity.getColor());
                dataList.add(wxMpTemplateData1);
                dataList.add(wxMpTemplateData2);
                dataList.add(wxMpTemplateData3);
                dataList.add(wxMpTemplateData4);
                dataList.add(wxMpTemplateData5);
                dataList.add(wxMpTemplateData6);
                dataList.add(wxMpTemplateData7);
                break;
            }
            default: {
                return null;
            }
        }
        wxTemplateEntity.setData(dataList);
        if (sendTemplateMsgEntity.getMinAppid() != null && !"".equals(sendTemplateMsgEntity.getMinAppid())) {
            WxTemplateRequest.MiniProgram miniProgram = getMiniProgram();
            miniProgram.setAppid(sendTemplateMsgEntity.getMinAppid());
            miniProgram.setPagePath(sendTemplateMsgEntity.getMinPagePath());
            wxTemplateEntity.setMiniProgram(miniProgram);
        } else if (sendTemplateMsgEntity.getLink() != null && !"".equals(sendTemplateMsgEntity.getLink())) {
            wxTemplateEntity.setUrl(sendTemplateMsgEntity.getLink());
        }

        return wxTemplateEntity;
    }

}
