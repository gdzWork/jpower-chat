package com.wlcb.jpower.chat.service.file;

import com.wlcb.jpower.chat.entity.file.TbChatFile;
import com.wlcb.jpower.module.base.vo.ResponseData;
import com.wlcb.jpower.module.common.service.BaseService;
import org.springframework.web.multipart.MultipartFile;

/**
 * @description:
 * @author: liwenfei
 * @time: 2020/12/25 11:52
 */
public interface ChatFileService extends BaseService<TbChatFile> {

    ResponseData fileSave(MultipartFile file);

    TbChatFile findFile(String id);
}
