package com.wlcb.jpower.chat.service.wx.mp.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.support.SFunction;
import com.wlcb.jpower.chat.dbs.dao.wxapp.TblWxAppDao;
import com.wlcb.jpower.chat.dbs.dao.wxapp.mapper.TblWxAppMapper;
import com.wlcb.jpower.chat.service.wx.mp.WxAppService;
import com.wlcb.jpower.constant.DBConstant;
import com.wlcb.jpower.module.common.service.impl.BaseServiceImpl;
import com.wlcb.jpower.chat.entity.wxapp.TblWxApp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("wxAppServiceImpl")
public class WxAppServiceImpl extends BaseServiceImpl<TblWxAppMapper, TblWxApp> implements WxAppService {

    @Autowired
    private TblWxAppDao wxAppDao;

    @Override
    public List<TblWxApp> findByStaus(Integer status) {
        List<TblWxApp> list = wxAppDao.list(new QueryWrapper<TblWxApp>().lambda()
                .eq(TblWxApp::getStatus, status)
                .eq(TblWxApp::getIsDeleted, 0)
                .eq(TblWxApp::getStatus, DBConstant.EnableEnum.OPEN.getType()));
        return list;
    }

    @Override
    public TblWxApp findOne(String appId) {
        return wxAppDao.getOne(new QueryWrapper<TblWxApp>().lambda()
                .eq(TblWxApp::getAppId, appId)
                .eq(TblWxApp::getIsDeleted, 0)
                .eq(TblWxApp::getStatus, DBConstant.EnableEnum.OPEN.getType()));
    }

    @Override
    public List<TblWxApp> findList(TblWxApp tblWxApp) {
        LambdaQueryWrapper<TblWxApp> queryWrapper = new QueryWrapper<TblWxApp>().lambda();
        queryWrapper.orderByDesc(TblWxApp::getCreateTime);
        queryWrapper.eq(TblWxApp::getIsDeleted, 0);
        Map<SFunction<TblWxApp, ?>, Object> map = new HashMap<>();
        map.put(TblWxApp::getAppId, tblWxApp.getAppId());
        map.put(TblWxApp::getMpName, tblWxApp.getMpName());
        map.put(TblWxApp::getCreateUser, tblWxApp.getCreateUser());
        map.put(TblWxApp::getId, tblWxApp.getId());
        map.put(TblWxApp::getStatus, tblWxApp.getStatus());
        queryWrapper.allEq(map, false);
        return wxAppDao.list(queryWrapper);
    }
}
