package com.wlcb.jpower.chat.dbs.dao.matter.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wlcb.jpower.chat.entity.matter.TbChatMatter;
import com.wlcb.jpower.module.dbs.dao.mapper.base.JpowerBaseMapper;
import org.springframework.stereotype.Component;

@Component("tbChatMatterMapper")
public interface TbChatMatterMapper extends JpowerBaseMapper<TbChatMatter> {
}
