package com.wlcb.jpower.chat.service.reply;

import com.wlcb.jpower.chat.entity.reply.TbDateReply;
import com.wlcb.jpower.module.common.service.BaseService;

import java.util.List;

public interface DateReplyService extends BaseService<TbDateReply> {

    List<TbDateReply> findPageList(TbDateReply reply);

    List<TbDateReply> findList(String appId);

    TbDateReply isDate(String appId);

    TbDateReply findByAsk(String ask,String appid);
}
