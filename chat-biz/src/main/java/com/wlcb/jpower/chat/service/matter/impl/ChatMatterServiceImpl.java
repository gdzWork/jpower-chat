package com.wlcb.jpower.chat.service.matter.impl;

import com.wlcb.jpower.chat.dbs.dao.matter.mapper.TbChatMatterMapper;
import com.wlcb.jpower.chat.entity.matter.TbChatMatter;
import com.wlcb.jpower.chat.service.matter.ChatMatterService;
import com.wlcb.jpower.module.common.service.impl.BaseServiceImpl;
import org.springframework.stereotype.Service;

/**
 * @description:
 * @author: liwenfei
 * @time: 2020/12/28 14:22
 */
@Service("chatMatterServiceImpl")
public class ChatMatterServiceImpl extends BaseServiceImpl<TbChatMatterMapper, TbChatMatter> implements ChatMatterService {
}
