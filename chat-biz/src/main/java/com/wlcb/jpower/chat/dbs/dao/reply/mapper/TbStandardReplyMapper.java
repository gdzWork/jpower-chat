package com.wlcb.jpower.chat.dbs.dao.reply.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wlcb.jpower.chat.entity.reply.TbStandardReply;
import com.wlcb.jpower.module.dbs.dao.mapper.base.JpowerBaseMapper;
import org.springframework.stereotype.Component;

@Component("tbStandardReplyMapper")
public interface TbStandardReplyMapper extends JpowerBaseMapper<TbStandardReply> {
}
