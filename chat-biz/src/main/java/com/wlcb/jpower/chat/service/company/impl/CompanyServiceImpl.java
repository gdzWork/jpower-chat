package com.wlcb.jpower.chat.service.company.impl;

import com.wlcb.jpower.chat.dbs.dao.company.TbCompanyDao;
import com.wlcb.jpower.chat.dbs.dao.company.mapper.TbCompanyMapper;
import com.wlcb.jpower.chat.entity.company.TbCompany;
import com.wlcb.jpower.chat.service.company.CompanyService;
import com.wlcb.jpower.module.base.vo.ResponseData;
import com.wlcb.jpower.module.common.service.impl.BaseServiceImpl;
import com.wlcb.jpower.module.common.support.BeanExcelUtil;
import com.wlcb.jpower.module.common.utils.FileUtil;
import com.wlcb.jpower.module.common.utils.ReturnJsonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.List;

/**
 * @description:
 * @author: liwenfei
 * @time: 2021/1/8 13:34
 */
@Service("companyServiceImpl")
public class CompanyServiceImpl extends BaseServiceImpl<TbCompanyMapper, TbCompany> implements CompanyService {

    @Autowired
    private TbCompanyDao companyDao;

    @Override
    public List<TbCompany> findList(TbCompany company) {
        return companyDao.getBaseMapper().findList(company);
    }

    @Override
    public ResponseData importExcel(File file) throws Exception {
        BeanExcelUtil<TbCompany> beanExcelUtil = new BeanExcelUtil<>(TbCompany.class);
        List<TbCompany> companyList = beanExcelUtil.importExcel(file);
        FileUtil.deleteFile(file);
        return ReturnJsonUtil.status(companyDao.saveBatch(companyList));
    }
}
