package com.wlcb.jpower.chat.dbs.dao.customer;

import com.wlcb.jpower.chat.dbs.dao.customer.mapper.TblCustomerMsgMapper;
import com.wlcb.jpower.chat.entity.customer.TblCustomerMsg;
import com.wlcb.jpower.module.dbs.dao.JpowerServiceImpl;
import org.springframework.stereotype.Repository;

@Repository
public class TblCustomerMsgDao extends JpowerServiceImpl<TblCustomerMsgMapper, TblCustomerMsg> {
}
