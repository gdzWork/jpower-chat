package com.wlcb.jpower.chat.dbs.dao.file.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wlcb.jpower.chat.entity.file.TbChatFile;
import com.wlcb.jpower.module.dbs.dao.mapper.base.JpowerBaseMapper;
import org.springframework.stereotype.Component;

@Component("chatFileMapper")
public interface ChatFileMapper extends JpowerBaseMapper<TbChatFile> {

}
