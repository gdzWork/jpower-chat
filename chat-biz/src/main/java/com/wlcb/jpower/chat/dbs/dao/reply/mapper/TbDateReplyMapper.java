package com.wlcb.jpower.chat.dbs.dao.reply.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wlcb.jpower.chat.entity.reply.TbDateReply;
import com.wlcb.jpower.module.dbs.dao.mapper.base.JpowerBaseMapper;
import org.springframework.stereotype.Component;

/**
 * @description:
 * @author: liwenfei
 * @time: 2021/2/4 17:01
 */
@Component("tbDateReplyMapper")
public interface TbDateReplyMapper extends JpowerBaseMapper<TbDateReply> {
}
