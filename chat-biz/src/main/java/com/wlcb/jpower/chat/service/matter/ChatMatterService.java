package com.wlcb.jpower.chat.service.matter;

import com.wlcb.jpower.chat.entity.matter.TbChatMatter;
import com.wlcb.jpower.module.common.service.BaseService;

public interface ChatMatterService extends BaseService<TbChatMatter> {
}
