package com.wlcb.jpower.chat.dbs.dao.log;

import com.wlcb.jpower.chat.dbs.dao.log.mapper.TbWxMenuLogMapper;
import com.wlcb.jpower.chat.entity.log.TbWxMenuLog;
import com.wlcb.jpower.module.dbs.dao.JpowerServiceImpl;
import org.springframework.stereotype.Repository;

/**
 * @author: liwenfei
 * @version: v1.0
 * @time: 2021/3/23 16:36
 * @desc:
 */
@Repository
public class TbWxMenuLogDao extends JpowerServiceImpl<TbWxMenuLogMapper, TbWxMenuLog> {
}
