package com.wlcb.jpower.chat.dbs.dao.reply;

import com.wlcb.jpower.chat.dbs.dao.reply.mapper.TbStandardReplyMapper;
import com.wlcb.jpower.chat.entity.reply.TbStandardReply;
import com.wlcb.jpower.module.dbs.dao.JpowerServiceImpl;
import org.springframework.stereotype.Repository;

@Repository
public class TbStandardReplyDao extends JpowerServiceImpl<TbStandardReplyMapper, TbStandardReply> {
}
