package com.wlcb.jpower.chat.service.wxuser;

import com.wlcb.jpower.chat.entity.wxuser.WxUserInfo;
import com.wlcb.jpower.module.common.service.BaseService;
import me.chanjar.weixin.common.error.WxErrorException;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @description:
 * @author: liwenfei
 * @time: 2020/11/30 14:04
 */
@Service("wxUserInfoService")
public interface WxUserInfoService extends BaseService<WxUserInfo> {

    boolean add(WxUserInfo wxUserInfo);

    WxUserInfo findByOpenidAndAppId(String openid,String appId);

    void collectUserInfo(String appId,String openid) throws WxErrorException;

    List<WxUserInfo> findPageList(WxUserInfo wxUserInfo);

}
