package com.wlcb.jpower.chat.service.wx.mp;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @Author fei
 * @Date 2020/7/1 11:13
 * @Version 1.0
 */
@Data
public class SendTemplateMsgEntity implements Serializable {

    @ApiModelProperty("公众号ID")
    private String appId;
    @ApiModelProperty("标签ID")
    private Long tagId;
    @ApiModelProperty("标签中的第一个拉取的OPENID")
    private String nextOpenid;
    @ApiModelProperty("用户openid，多个逗号分隔")
    private String sendUser;
    @ApiModelProperty("跳转地址")
    private String link;
    @ApiModelProperty("第一个参数值")
    private String first;
    @ApiModelProperty("参数值")
    private String keyword1;
    @ApiModelProperty("参数值")
    private String keyword2;
    @ApiModelProperty("参数值")
    private String keyword3;
    @ApiModelProperty("参数值")
    private String keyword4;
    @ApiModelProperty("参数值")
    private String keyword5;
    @ApiModelProperty("参数值")
    private String keyword6;
    @ApiModelProperty("参数值")
    private String keyword7;
    @ApiModelProperty("备注")
    private String remark;
    @ApiModelProperty("参数数量")
    private Integer paramCount;
    @ApiModelProperty("颜色")
    private String color;
    @ApiModelProperty("公众号ID")
    private String minAppid;
    @ApiModelProperty("公众号路径")
    private String minPagePath;
    @ApiModelProperty("模板ID")
    private String templateId;
}
