package com.wlcb.jpower.chat.dbs.dao.wxuser;

import com.wlcb.jpower.chat.dbs.dao.wxuser.mapper.WxUserInfoMapper;
import com.wlcb.jpower.module.dbs.dao.JpowerServiceImpl;
import com.wlcb.jpower.chat.entity.wxuser.WxUserInfo;
import org.springframework.stereotype.Repository;

/**
 * @description:
 * @author: liwenfei
 * @time: 2020/11/30 14:01
 */
@Repository
public class WxUserInfoDao extends JpowerServiceImpl<WxUserInfoMapper, WxUserInfo> {
}
