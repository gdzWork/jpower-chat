package com.wlcb.jpower.chat.entity.matter;

import com.baomidou.mybatisplus.annotation.TableName;
import com.wlcb.jpower.module.tenant.entity.TenantEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @description:
 * @author: liwenfei
 * @time: 2020/12/28 14:18
 */
@Data
@TableName("tb_chat_matter")
public class TbChatMatter extends TenantEntity implements Serializable {
    private static final long serialVersionUID = 5948806155018682202L;

    @ApiModelProperty("事项名称")
    private String name;

    @ApiModelProperty("父级事项ID")
    private String parentId;
}
