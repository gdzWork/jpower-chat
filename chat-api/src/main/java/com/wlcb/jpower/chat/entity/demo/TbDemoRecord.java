package com.wlcb.jpower.chat.entity.demo;

import com.baomidou.mybatisplus.annotation.TableName;
import com.wlcb.jpower.module.dbs.entity.base.BaseEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @date: 2021/5/18 13:43
 * @author: fei
 * @desc:
 */
@Data
@TableName("tb_demo_record")
public class TbDemoRecord extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 5671261885380034685L;

    @ApiModelProperty("用户ID")
    private String userId;
    @ApiModelProperty("组ID")
    private String groupId;
    @ApiModelProperty("内容")
    private String content;
    @ApiModelProperty("颜色")
    private String color;
}
