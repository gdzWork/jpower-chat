package com.wlcb.jpower.chat.entity.customer;

import com.baomidou.mybatisplus.annotation.TableName;
import com.wlcb.jpower.module.base.annotation.Dict;
import com.wlcb.jpower.module.dbs.entity.base.BaseEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @description: 客服系统设置
 * @author: liwenfei
 * @time: 2021/1/26 11:34
 */
@Data
@TableName("tb_customer_settings")
public class TbCustomerSettings extends BaseEntity implements Serializable {

    private static final long serialVersionUID = -7854382906665369486L;
    /**
    客服账号
     */
    @ApiModelProperty("客服账号")
    private String cid;
    /**
     * 0 不启用自动接入  1 启用自动接入
     */
    @Dict(name = "YN01",attributes = "autoAccessStr")
    @ApiModelProperty("是否自动接入")
    private Integer autoAccess;
    private String autoAccessStr;
    /**
     * 接入自动回复
     * 0 不启用自动回复 1 启用自动回复
     */
    @Dict(name = "YN01",attributes = "autoAccessReplyStr")
    @ApiModelProperty("是否自动回复")
    private Integer autoAccessReply;
    private String autoAccessReplyStr;
    /**
     * 自动回复内容
     */
    @ApiModelProperty("自动回复内容")
    private String replyContent;
    /**
     * 是否开启 转接限制功能 0 关闭 1 开启
     */
    @Dict(name = "YN01",attributes = "allowTransferStr")
    @ApiModelProperty("是否开启转接限制功能")
    private Integer allowTransfer;
    private String allowTransferStr;
    /**
     * 自动回复 0 关闭 1 开启
     */
    @Dict(name = "YN01",attributes = "autoReplyStr")
    @ApiModelProperty("是否自动回复")
    private Integer autoReply;
    private String autoReplyStr;

}
