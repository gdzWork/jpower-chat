package com.wlcb.jpower.chat.entity.wxapp;

import com.baomidou.mybatisplus.annotation.TableName;
import com.wlcb.jpower.module.tenant.entity.TenantEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
@TableName("tb_wx_app")
public class TblWxApp extends TenantEntity implements Serializable {
    private static final long serialVersionUID = 5865834189146890078L;

    @ApiModelProperty("平台名称")
    private String mpName;
    @ApiModelProperty("公众号ID")
    private String appId;
    @ApiModelProperty("公众号密钥")
    private String appSecret;
    @ApiModelProperty("公众号盐值")
    private String token;
    @ApiModelProperty("微信公众号的EncodingAESKey")
    private String aesKey;
    @ApiModelProperty("公众号原始ID")
    private String wxOriginalId;
}
