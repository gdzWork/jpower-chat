package com.wlcb.jpower.chat.entity.reply;

import cn.hutool.core.date.DatePattern;
import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.wlcb.jpower.module.tenant.entity.TenantEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @description:
 * @author: liwenfei
 * @time: 2021/2/4 16:59
 */
@Data
@TableName("tb_fixed_reply")
public class TbDateReply extends TenantEntity implements Serializable {
    private static final long serialVersionUID = 8320211832394964004L;

    @ApiModelProperty("公众号ID")
    private String appid;
    @ApiModelProperty("用户问题")
    private String ask;
    @JSONField(format= DatePattern.NORM_DATETIME_PATTERN)
    @ApiModelProperty("开始时间")
    private Date startTime;
    @JSONField(format=DatePattern.NORM_DATETIME_PATTERN)
    @ApiModelProperty("结束时间")
    private Date endTime;
    @ApiModelProperty("备注")
    private String remark;
    @ApiModelProperty("回复内容 json数组格式")
    private String replyBodys;

}
