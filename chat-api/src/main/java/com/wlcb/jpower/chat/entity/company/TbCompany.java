package com.wlcb.jpower.chat.entity.company;

import com.baomidou.mybatisplus.annotation.TableName;
import com.wlcb.jpower.module.base.annotation.Excel;
import com.wlcb.jpower.module.tenant.entity.TenantEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @description 客服单位表
 * @author liwenfei
 * @time 2021/1/8 13:30
 */
@Data
@TableName("tb_company")
public class TbCompany extends TenantEntity implements Serializable {
    private static final long serialVersionUID = 415831478041065025L;

    @Excel(name = "归属区域")
    @ApiModelProperty("归属区域")
    private String area;
    @Excel(name = "单位")
    @ApiModelProperty("单位")
    private String company;
    @Excel(name = "客服号")
    @ApiModelProperty("客服号")
    private String cid;
    @Excel(name = "负责人")
    @ApiModelProperty("负责人")
    private String person;
    @Excel(name = "电话")
    @ApiModelProperty("电话")
    private String phone;
    @Excel(name = "关键字")
    @ApiModelProperty("关键字")
    private String remark;
}
