package com.wlcb.jpower.chat.entity.wxuser;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.wlcb.jpower.module.tenant.entity.TenantEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @description:
 * @author: liwenfei
 * @time: 2020/11/30 13:57
 */
@Data
@TableName("tb_wx_userinfo")
public class WxUserInfo extends TenantEntity implements Serializable {

    @ApiModelProperty("用户openId")
    private String openid;
    @ApiModelProperty("用户是否订阅该公众号标识，值为0时，代表此用户没有关注该公众号，拉取不到其余信息。")
    private Integer subscribe;
    @ApiModelProperty("昵称")
    private String nickname;
    @ApiModelProperty("性别")
    private Integer sex;
    @ApiModelProperty("城市")
    private String city;
    @ApiModelProperty("国家")
    private String country;
    @ApiModelProperty("省份")
    private String province;
    @ApiModelProperty("头像")
    private String headimgurl;
    @ApiModelProperty("关注时间")
    private Long subscribeTime;
    @ApiModelProperty("公众平台ID")
    private String unionid;
    @ApiModelProperty("备注")
    private String remark;
    @ApiModelProperty("组id")
    private String groupid;
    @ApiModelProperty("标签")
    private String tagidList;
    @ApiModelProperty("用户关注的渠道来源")
    private String subscribeScene;
    @ApiModelProperty("二维码扫码场景")
    private String qrScene;
    @ApiModelProperty("二维码扫码场景描述")
    private String qrSceneStr;
    @ApiModelProperty("公众号ID")
    private String appId;
    @ApiModelProperty("地理位置")
    private String location;

    @TableField(exist = false)
    @ApiModelProperty("会话ID")
    private String dialogId;
    @TableField(exist = false)
    @ApiModelProperty("是否特殊人员")
    private Boolean isSpecial;

}
