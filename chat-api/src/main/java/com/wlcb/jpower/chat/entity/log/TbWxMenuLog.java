package com.wlcb.jpower.chat.entity.log;

import com.baomidou.mybatisplus.annotation.TableName;
import com.wlcb.jpower.module.dbs.entity.base.BaseEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author: liwenfei
 * @version: v1.0
 * @time: 2021/3/23 16:37
 * @desc:
 */
@Data
@TableName("tb_wx_menu_log")
public class TbWxMenuLog extends BaseEntity {
    @ApiModelProperty("公众号id")
    private String appid;
    @ApiModelProperty("菜单字符串")
    private String menu;
}
