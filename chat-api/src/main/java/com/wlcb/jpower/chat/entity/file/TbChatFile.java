package com.wlcb.jpower.chat.entity.file;

import com.baomidou.mybatisplus.annotation.TableName;
import com.wlcb.jpower.module.tenant.entity.TenantEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @description:
 * @author: liwenfei
 * @time: 2020/12/25 10:11
 */
@Data
@TableName("tb_chat_file")
public class TbChatFile extends TenantEntity implements Serializable {
    private static final long serialVersionUID = 280520645299649417L;

    @ApiModelProperty("文件原名称")
    private String fileRealName;
    @ApiModelProperty("文件全路径")
    private String filePath;
}
