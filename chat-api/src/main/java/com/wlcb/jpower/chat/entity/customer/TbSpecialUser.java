package com.wlcb.jpower.chat.entity.customer;

import com.baomidou.mybatisplus.annotation.TableName;
import com.wlcb.jpower.module.dbs.entity.base.BaseEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author: liwenfei
 * @version: v1.0
 * @time: 2021/4/13 9:17
 * @desc:
 */
@Data
@TableName("tb_special_user")
public class TbSpecialUser extends BaseEntity implements Serializable {
    private static final long serialVersionUID = -6807039504188501270L;

    @ApiModelProperty("姓名")
    private String name;
    @ApiModelProperty("身份证")
    private String idcard;
    @ApiModelProperty("手机号")
    private String phone;
    @ApiModelProperty("备注")
    private String remark;
    @ApiModelProperty("公司")
    private String company;
    @ApiModelProperty("openid")
    private String openid;
    @ApiModelProperty("appid")
    private String appid;
}

