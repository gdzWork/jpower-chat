package com.wlcb.jpower.chat.entity.customer;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.wlcb.jpower.module.tenant.entity.TenantEntity;
import com.wlcb.jpower.chat.entity.wxuser.WxUserInfo;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
@TableName("tb_customer_msg")
public class TblCustomerMsg extends TenantEntity implements Serializable {
    private static final long serialVersionUID = -7532097417796418438L;

    /**
     * 用户id
     */
    @ApiModelProperty("用户openid")
    private String openid;
    /**
     * msg id
     */
    @ApiModelProperty("会话ID")
    private String msgId;
    /**
     * 客服id
     */
    @ApiModelProperty("初始客服账号")
    private String firstCid;
    /**
     * 当前接入人
     */
    @ApiModelProperty("当前客服账号")
    private String currentCid;
    /**
     * 最后完成会话客服id
     */
    @ApiModelProperty("最后完成会话客服账号")
    private String completeCid;
    /**
     * 公众号id
     */
    @ApiModelProperty("公众号id")
    private String appid;
    /**
     * 会员记录
     */
    @ApiModelProperty("会员记录")
    private String data;
    /**
     * 评分
     */
    @ApiModelProperty("评分")
    private String score;

    /**
     * 咨询事项
     */
    @ApiModelProperty("咨询事项")
    private String type;
    /**
     * 用户姓名
     */
    @ApiModelProperty("用户姓名")
    private String name;
    /**
     * 地址
     */
    @ApiModelProperty("地址")
    private String address;
    /**
     * 城市
     */
    @ApiModelProperty("城市")
    private String city;

    @ApiModelProperty("用户信息")
    @TableField(exist = false)
    private WxUserInfo userInfo;

    /**
     * 咨询事项
     */
    @ApiModelProperty("咨询事项")
    @TableField(exist = false)
    private String typeName;
    /**
     * 是否特殊人员
     */
    @ApiModelProperty("是否特殊人员 字典:YN01")
    private Integer isSpecial;

}
