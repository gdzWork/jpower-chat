package com.wlcb.jpower.chat.entity.reply;

import com.baomidou.mybatisplus.annotation.TableName;
import com.wlcb.jpower.module.tenant.entity.TenantEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
@TableName("tb_standard_reply")
public class TbStandardReply extends TenantEntity implements Serializable {
    private static final long serialVersionUID = 3947342573832661149L;

    @ApiModelProperty("回复内容")
    private String content;

    @ApiModelProperty("客服账号")
    private String cid;
}
